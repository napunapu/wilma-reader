Santulla huomenna ti lääkärikäynti
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Hellsten Saila (HEL), Kultanen Tiia (KUL), Lehmusvuori Teija (LEH)
2021-03-29 19.59

<p>Hei!</p>
<p>Santulla on huomenna lääkärikäynti Lastensairaalalla, joten hän on poissa ruotsin tunnin ja luultavimmin tulee myöhässä käsityötunnille.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
