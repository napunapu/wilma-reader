Julkaisin Santun tukipaperin teille huoltajille näkymään
Lehmusvuori Teija (LEH)
Piilotettu
2020-09-25 10.53

<p>Hei,</p>
<p>ja kiitos vielä mukavasta tapaamisesta.<br>
Toivottavasti Santtu voi paremmin.<br>
Julkaisin oppimissuunitelman teille huoltajille näkymään.<br>
Jos siinä on jotain kommentoitavaa, palaattehan asiaan ja muokataan sitä.</p>
<p>Hyvää viikonloppua!<br>
Yst. terv. Teija LEH</p>
