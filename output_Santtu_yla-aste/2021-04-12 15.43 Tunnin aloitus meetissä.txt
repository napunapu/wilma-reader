Tunnin aloitus meetissä
Forss Tommi (FOR)
Piilotettu
2021-04-12 15.43

<p>Moi,</p>
<p>tänään ollaan Espoossa siirrytty osittain etä-/lähiopetukseen, jolloin yksi luokka-aste on kerrallaan viikon koululla. Tämän vuoksi esim liikunnassa tunnin aloitus saattaa myöhästyä hieman, kuten tänään. Yseillä oli tunti kentällä, josta pääsin takaisin koululle aloittamaan tunnin&nbsp;vasta 14.47. Joten odotelkaa rauhassa opettajaa, jos ei ihan minuutilleen kuulu.&nbsp;</p>
<p>T. Tommi</p>
