LIIKUNTAILTAPÄIVÄ pe 12.3. 11.45-14.30 välisenä aikana, OHJEET
Lehmusvuori Teija (LEH)
Piilotettu
2021-03-10 13.03

<p>Hei 7D ja huoltajat!<br>
Tästä puhuimme siis LV-vartissa tänään:<br>
Ensi perjantaina on iltapäiväksi erityisohjelmaa. Alla ohjeet oppilaille&nbsp;(ja huoltajille tiedoksi) siitä, miten toimitaan.<br>
Yst. terv. Teija LEH</p>
<p>___________________________________</p>
<p>Teema: MIKÄ LUOKKA LIIKKUU PISIMMÄLLE?<br>
<br>
Perjantain 12.3. aamupäivän oppitunnit pidetään lukujärjestyksen mukaan. Ruokailun jälkeen klo 11.45 aloitetaan liikuntapäivän ohjelma.<br>
<br>
1. Valitse liikuntalaji, esim. kävely, pyöräily, hiihto, juoksu tai muu omalla lihasvoimalla etenevä laji.<br>
<br>
2. Mittaa liikkumasi matka jollain puhelimen liikuntasovelluksella (esim. Sports Tracker, Runkeeper) tai äly- tai urheilukellolla.<br>
<br>
3. Ota tuloksesta kuvakaappaus ja palauta kuva ENGLANNIN Classroomiin. Huolehdi, että kuvassa näkyy suoritusaika (päivämäärä ja kellonaika), matka ja mahdollisesti myös laji.<br>
<br>
Matkan voi myös mitata Google Mapsissa sunnittelemalla reitin, jos puhelimella tai kellolla mittaaminen ei ole mahdollista.<br>
<br>
4. Palauta kuvasi ENGLANNIN CLASSROOMISSA OLEVAAN TEHTÄVÄÄN pe 12.30 klo 14.30 mennessä.<br>
<br>
Luokanvalvoja laskee tuloksen yhteen ja jakaa luokan oppilasmäärällä. Pisimmälle liikkuneelle luokalle on luvassa palkinto. (Opettajat muodostavat oman joukkueensa.)</p>
