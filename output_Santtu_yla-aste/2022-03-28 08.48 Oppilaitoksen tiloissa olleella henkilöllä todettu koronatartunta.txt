Oppilaitoksen tiloissa olleella henkilöllä todettu koronatartunta
Korhonen Joonas (KOR)
Piilotettu
2022-03-28 08.48

<p>Hei!</p>
<p>Oppilaitoksen tiloissa viime viikolla olleella henkilöllä on todettu koronavirustartunta. Kaikki lapset ja henkilökunta eivät ole altistuneet.</p>
<p>Lapsesi voi jatkaa normaalia arkea, jos on oireeton.</p>
<p>Mikäli ilmenee koronavirustautiin sopivia oireita (kuume, yskä, hengenahdistus, lihaskivut, kurkkukipu, väsymys, haju-/makuaistin menetys, ripuli), suositellaan tekemään kotitesti sairaanhoitopiirin ohjeiden mukaan tai tarvittaessa hakeutumaan terveydenhuollon koronavirustestiin. Terveydenhuollon näytteenoton voi varata osoitteessa koronabotti.hus.fi. Voit myös soittaa kotikuntasi koronaneuvontaan (Espoon koronaneuvonta p. 09 816 34600 ma-pe klo 7–18 ja la-su 9–15).</p>
<p>yst.</p>
<p>Joonas Korhonen</p>
