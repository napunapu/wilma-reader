Kiitos!
Lehmusvuori Teija (LEH)
Piilotettu
2021-06-05 08.19

<p>Hyvät 7D:n oppilaiden huoltajat!</p>
<p>Kiitos teille kaikille ystävällisistä viesteistä ja hyvästä yhteistyöstä kuluneen kouluvuoden aikana. Oppilaat ovat tehneet hyvin töitä vaikka vuosi ei ole ollut heille helppo. Uusi ryhmä, uudet opettajat, vaativammat oppiainekokonaisuudet ja kokeet, poikkeusajan tuomia haasteita lukuun ottamatta. Vuosi on tuonut uusia ystäviä, vanhat ystävyydet ovat saattaneet muuttua ja kadota, sosiaaliset suhteet ja oma itse muuttuvat.&nbsp;</p>
<p>Olen ylpeä oppilaistani, he ovat kasvaneet&nbsp;vuoden aikana yläkoululaisiksi. Tästä on hyvä jatkaa oppimista yhdessä.</p>
<p>Hyvää kesää kaikille!</p>
<p>Ystävällisin terveisin,</p>
<p>Teija LEH</p>
<p>&nbsp;</p>
