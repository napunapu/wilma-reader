Yhteinen vanhempainilta ja muutamia muita ajatuksia / Parent's event and some other thoughts
Lehmusvuori Teija (LEH)
Piilotettu
2022-03-04 10.58

<p>Hei 8D:n oppilaiden huoltajat! / Dear guardians of the 8D pupils</p>
<p>Rehtori Joonas Korhonen on lähettänyt kaikille kutsun ensi keskiviikon 9.3. vanhempainiltaan,<br>
joka järjestetään etätilaisuutena. En järjestä erikseen mitään 8D-luokan vanhempainiltaa,<br>
mutta olen mielelläni tarvittaessa kahdenkeskisessä yhteydessä perheisiin, jotka sitä toivovat.<br>
Ns. yhteisiä asioita on varsin vähän.&nbsp;<br>
<br>
Lukuvuosi on sujunut kaikista ulkoisista haasteista riippumatta luokalla kohtuullisen hyvin.<br>
Toki se on varmasti ollut melko haastava ja oppilaiden jaksaminen on ollut koetuksella.<br>
Poissaoloja on enemmän (koronasta ja sen vaatimasta varovaisuudesta johtuen) kuin aiemmin<br>
ja myöhästelyä ja aamutuntipoissaoloja on osalla oppilaista runsaasti ja osa oppilaista reagoi haastaen<br>
koulun järjestyssääntöjä säännöllisesti. Erityisesti pakollinen ulkovälitunti ruokailun yhteydessä on<br>
jatkuvasti osalle suuri "haaste". Toivottavasti kevät tuo helpotusta koulun sääntöihin ja niiden noudattamisen<br>
vaikeuteen.<br>
<br>
Kahdeksannen luokan merkitystä ei useinkaan hahmoteta, sen onnistumisen päälle jatketaan<br>
työtä ensi vuonna, jolloin oppilaat päättävät jatkosuunnitelmistaan. Pieni "loppuspurtti" voisi olla<br>
monella paikallaan: välineet mukana, läksyt huolella tehtyinä ja pidemmällä ajanjaksolla kokeisiin<br>
kerraten olisi hyvä tulla kouluun, ja näissä asioissa&nbsp;te huoltajat voitte olla apuna ja tukena.<br>
Varmasti jokaiselle oppilaalle on selvää - mutta muistutus tästä teille huoltajille - että päättötodistukseen<br>
tästä lukuvuodesta jää jo historian arvosana, koska ensi vuonna alkaa yhteiskuntaopin opiskelu.<br>
<br>
Jos mielessänne on nuorenne asioita, joista haluaisitte keskustella, olettehan yhteydessä.<br>
Voimme sitten tarvittaessa järjestää&nbsp;tapaamisen. Kiitos teille hyvästä tiedonkulusta tähän asti.<br>
Toivottavasti jaksatte pitää minut ajan tasalla nuorenne asioista&nbsp;jatkossakin ja seurata wilmaa.<br>
Tapaan oppilaitani valitettavan harvoin - kerran viikossa oppitunnilla ja kerran viikossa lyhyessä LV-vartissa -<br>
enkä voi väittää olevani aina "ajan tasalla".</p>
<p>Huolimatta ikävistä uutisista maailmanpolitiikassa,<br>
toivon teille oikein hyvää kevään jatkoa!</p>
<p>Ystävällisin terveisin,<br>
Teija LEH</p>
<p>//</p>
<p>Our principal&nbsp;Joonas Korhonen has sent an invitation to a joint parental (distance) meeting on the 9th of March.<br>
I will not arrange any special 8D parent's event but I'm more than happy to arrange a private meeting with<br>
you if you see fit. There are very few things we should talk about all together.</p>
<p>This term has gone relatively well despite all the challenges the pandemic has placed on us all.<br>
I'm sure it has been especially hard on the students. The students have been absent more (because of covid and<br>
the extra carefullness it has required) and some students are very often late for class or oversleep regularly,<br>
and some have difficulties to follow the school rules. Especially the mandatory outdoor break around lunch is<br>
a huge challenge for some of my&nbsp;students. I hope this spring we will see lighter rules and regulations<br>
and hopefully it also makes it easier for the students to follow them.<br>
<br>
The importance of the 8th grade is not always understood. Its results make&nbsp;the basis the students build their<br>
next year's studies and future studying plans on. It's always better to come to school with the right books and<br>
other necessary materials packed in the backpack and all the homework carefully done. Spending a few more evenings or hours<br>
on studying to the tests would also be a good idea. There is room for improvement there and some extra effort on this might be a good idea.<br>
This is where you parents and guardians can be of help. All my students are very aware of this, but a quick reminder that this year's grade<br>
in history will also appear on&nbsp;the student's final report card next year, since they will study social studies next year.</p>
<p>If there are things that you feel we should talk with me,&nbsp;please send a message.<br>
We can always arrange a meeting for a more detailed chat. Thank you for having kept me&nbsp;well informed,<br>
please continue with that and follow wilma regularly. Unfortunately I only meet my students twice a week;<br>
once when we have our lesson together and once briefly in the weekly info, and I'm afraid I can't say I'm always "up-to-date".</p>
<p>Despite the gloomy and distressing global news, I wish all families a very good spring.</p>
<p>Best regards,<br>
Teija LEH</p>
<p>&nbsp;</p>
