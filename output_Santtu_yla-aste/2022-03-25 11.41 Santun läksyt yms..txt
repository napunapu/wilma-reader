Santun läksyt yms.
Lehmusvuori Teija (LEH)
Piilotettu
2022-03-25 11.41

<p>Hei taas!</p>
<p>Johanna sanoikin viestitelleensä teidän vanhempien kanssa Santun mahdollisuudesta käyttää konetta koulussa.<br>
Minua on mietityttänyt asia, joka näkyy ainakin englannissa. Nyt kun Santtu ei pysty kirjoittamaan kynällä,<br>
olisiko mahdollista tehdä sellaisia läksyjä, joissa täytetään tehtäviä vaikka niin, että joku kirjoittaa Santun sanomat vastaukset tehtäväkirjaan?<br>
Nyt ei ole lainkaan selvää, onko Santtu edesä katsonut läksyksi tulleita tehtäviä ja ne jäävät tyhjiksi, mikä ei auta tunnilla työskentelyä yhtään.<br>
<br>
Onko Whatsup-muistiinpanoista ollut apua? Nyt Santtu kirjoittelee niitä ahkerasti (ainakin englannin tunnille), ja tuntuu joskus unohtuvan siihen puhelinmaailmaan. Niiden kirjoittaminenhan on enemmän kuin OK, mutta mietin, pitäisikö minun seurailla sitä tarkemmin?</p>
<p>Sovimme varmaan kohtapuolin sitten sen sovitun "huhtikuun alkupuolen" palaverin, voisiko terapiataho olla siinä aloitteellinen vai toivotteko, että me täältä etsimme yhteistä aikaa?</p>
<p>Hyvää viikonloppua koko perheelle!</p>
<p>Yst. terv. Teija LEH</p>
