RYHMÄYTYSPÄIVÄ TORSTAINA 20.8.2020
Lehmusvuori Teija (LEH)
Piilotettu
2020-08-17 14.02

<p>Hei!</p>
<p>Seiskaluokkalaisten ryhmäytyspäivä järjestetään torstaina 20.8.2020. Koulupäivä alkaa klo 8.30 kotiluokasta ja päättyy noin klo 13.15.</p>
<p>Ryhmäytyspäivä pidetään Kauklahden alueella. Päivän aikana oppilaat kiertävät luokanvalvojan kanssa rasteja, joilla tutustutaan oman luokan oppilaisiin. Suurin osa päivästä tullaan viettämään ulkona liikkuen, joten oppilaan tulee pukeutua sään mukaisesti.&nbsp;</p>
<p>Ruokailu tapahtuu normaalisti koulun ruokalassa.&nbsp;</p>
<p>Mukaan kannattaa varata vesipullo.</p>
<p>Olkaa yhteyksissä, mikäli tulee mitä tahansa kysyttävää!</p>
<p>Yt. 7. luokkien luokanvalvojat</p>
