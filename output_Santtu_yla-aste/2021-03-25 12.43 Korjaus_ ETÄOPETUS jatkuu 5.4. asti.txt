Korjaus: ETÄOPETUS jatkuu 5.4. asti
Korhonen Joonas (KOR)
Piilotettu
2021-03-25 12.43

<p>Tänään lähettämässäni&nbsp;viestissä, jossa kerroin etäopetuksen jatkuvan, oli otsikossa virhe. <strong>Etäopetus</strong> siis jatkuu 5.4. asti. Muuten viestin sisältö oli oikein.&nbsp;Pahoittelut sekaannuksesta. Etäkoulu vaatii näköjään veronsa!</p>
<p>Yst.</p>
<p>Joonas</p>
<p>&nbsp;</p>
<p>Vuosiluokkien 7–10 oppilaat ja toiseen asteen opiskelijat jatkavat etäopetuksessa 5.4.2021 asti pääkaupunkiseudun koronakoordinaatioryhmän linjauksen mukaisesti.</p>
<p>Etäopetuksessa olevat oppilaat ja opiskelijat saavat koululta lounaspaketin, joka sisältää koululounaat viikoksi. Lounaspaketti ajalle 29.3.–1.4.2021 jaetaan tiistaina 30.3. kello 14-16 välisenä aikana. Lounaspaketteja on varattu niille oppilaille, jotka ovat aiemmin ilmoittautuneet jakeluun. Voit myös kysellä koulusihteeri Tuire Isotalolta ylimääräisistä ruokapaketeista tai ilmoittautua varuilta pääsiäisen jälkeiseen hakuun perjantai-iltapäivään mennessä, jos etäopetus sattuisi jatkumaan.</p>
<p>Välttämättömän lähiopetuksen tarpeet turvataan edelleen. Kouluilla voidaan järjestää lähiopetusta esimerkiksi erityisen tuen, pidennetyn oppivelvollisuuden ja valmistavan opetuksen oppilaille.</p>
<p>Vuosiluokat 1–6 jatkavat lähiopetuksessa.</p>
<p>&nbsp;</p>
<p>Pääkaupunkiseudun koronakoordinaatioryhmän tiedote 24.3.2021:&nbsp;<a href="https://www.espoo.fi/fi-FI/Paakaupunkiseudulla_voimassa_olevia_rajo%28192154%29">https://www.espoo.fi/fi-FI/Paakaupunkiseudulla_voimassa_olevia_rajo(192154)</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Eleverna i årskurserna 7–10 samt studerande på andra stadiet fortsätter i distansundervisning fram till 5.4.2021 enligt huvudstadsregionens&nbsp;coronasamordningsgrupps&nbsp;riktlinjer.&nbsp;</p>
<p>&nbsp;</p>
<p>Eleverna och studerandena i distansundervisning får lunchpaket från skolan. Lunchpaket för tiden 29.3.-1.4.2021 delas ut på tisdag 30.3, 14-16. Lunchpaket har reserverats till de elever som tidigare anmält sig till lunchutdelningen.</p>
<p>Behoven av nödvändig närundervisning tryggas fortsättningsvis. Skolorna kan ordna närundervisning till exempel för elever som har fått beslut om särskilt stöd, elever som omfattas av förlängd läroplikt och elever som deltar i undervisning som förbereder för den grundläggande utbildningen.</p>
<p>Elever i årskurserna 1–6 fortsätter i närundervisning.</p>
<p>&nbsp;</p>
<p>Huvudstadsregionens coronasamordningsgrupps meddelande 24.3.2021:&nbsp;<a href="https://www.espoo.fi/fi-FI/Paakaupunkiseudulla_voimassa_olevia_rajo%28192154%29">https://www.espoo.fi/fi-FI/Paakaupunkiseudulla_voimassa_olevia_rajo(192154)</a></p>
<p>__________________________________________________________________________</p>
<p>&nbsp;</p>
<p>In line with the guidelines issued by the coronavirus coordination group for the Helsinki metropolitan area, distance learning will continue for pupils in grades 7–10 and upper secondary school students until 5 April 2021.</p>
<p>Pupils and students studying from home will receive a lunch package from school containing lunches for a week. Lunch packages for the period from 29 March to 1 April 2021 will be available on Tuesday 30, 14-16. We have reserved lunch packages for all pupils who have previously signed up. For extra packages, please contact Tuire Isotalo as soon as possible.</p>
<p>Essential contact teaching needs will be met. Schools can provide contact teaching for pupils who have received a special support decision, pupils attending extended compulsory education or instruction preparing for basic education.</p>
<p>Pupils in grades 1-6 will continue in contact teaching.</p>
<p>&nbsp;</p>
<p>Press release of the coronavirus coordination group, 24 March 2021:&nbsp;<a href="https://www.espoo.fi/fi-FI/Paakaupunkiseudulla_voimassa_olevia_rajo%28192154%29">https://www.espoo.fi/fi-FI/Paakaupunkiseudulla_voimassa_olevia_rajo(192154)</a></p>
