5. jakson liikunta 9 bd
Forss Tommi (FOR)
Piilotettu
2023-04-10 19.35

<p><strong>9B/D LAJI PAIKKA MUUTA</strong></p>
<p><strong>ke 12.4. telinevoimistelu liikuntasali sisäliikuntavarustus</strong></p>
<p><strong>to 13.4. uinti, paikalla 14.00, päättyy 15,30 Keski-Espoon uh uikkarit, pyyhe ja uimalasit</strong></p>
<p><strong>ke 19.4. kpkp koulun sali sisävarustus</strong></p>
<p><strong>to 20.4. Vapaa edellisen viikon kaksoistunnista</strong></p>
<p><strong>ke 26.4. lentopallo + lenkkeily liikuntasali sisäliikuntavarustus</strong></p>
<p><strong>to 27.4. jalkapallo Hansavalkama futiskengät</strong></p>
<p><strong>ke 3.5. ulkokuntosali Hansavalkama ulkovarustus </strong></p>
<p><strong>to 4.5. maastojuoksu Palolampi juoksuvarusteet</strong></p>
<p><strong>ke 10.5. toiveliikuntatunti koulu ulkovarustus</strong></p>
<p><strong>to 11.5. 2000m juoksu Mankin kenttä juoksuvarusteet</strong></p>
<p><strong>ke 17.5. pesäpallo + kävely t + p koulun kenttä ulkovarustus</strong></p>
<p><strong>to 18.5. helatorstai</strong></p>
<p><strong>ke 24.5. pesäpallo t + p koulun kenttä</strong></p>
<p><strong>to 25.5. pesäpallo koulun kenttä</strong></p>
