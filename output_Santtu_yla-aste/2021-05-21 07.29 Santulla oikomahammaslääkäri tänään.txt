Santulla oikomahammaslääkäri tänään
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Kultanen Tiia (KUL), Lehmusvuori Teija (LEH), Niemenpää Seija (NIE)
2021-05-21 07.29

<p>Huomenta!</p>
<p>Santulla on tänään oikomahammaslääkäri Leppävaarassa klo 13.20, joten joutuu lähtemään ruotsin tunnilta klo 12.45 ja tulee historian tunnille myöhässä.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
