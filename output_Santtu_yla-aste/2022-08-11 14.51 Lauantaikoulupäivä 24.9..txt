Lauantaikoulupäivä 24.9.
Korhonen Joonas (KOR)
Piilotettu
2022-08-11 14.51

<p>Hyvät oppilaat ja huoltajat!</p>
<p>Kauklahti-seura ja Kauklahden koulu järjestävät yhteistyössä Kauklahti-päivän koulullamme lauantaina 24.9. Tilaisuudessa juhlistetaan 50-vuotiasta Espoon kaupunkia. Tänä syksynä tulee kuluneeksi myös 60 vuotta koulumme perustamisesta.</p>
<p>Lauantai 24.9. on oppilaille koulupäivä, joka korvaa maanantain 5.12. koulupäivän. Näin oppilaat saavat hieman pidemmän itsenäisyyspäiväloman.</p>
<p>Tilaisuuden tarkempi aikataulu ja ohjelma ilmoitetaan myöhemmin, mutta tilaisuudessa on myyntikojuja, kahvio ja mahdollisesti esiintyjiä. Koulu pitää myös&nbsp;avoimia ovia, jolloin kaikki voivat tulla tutustumaan Kauklahden koulun oppitunteihin.</p>
<p>Vanhemmat ja muut läheiset ovat erittäin lämpimästi tervetulleita tilaisuuteen!</p>
<p>Ystävällisin terveisin,</p>
<p>Joonas, rehtori</p>
