Espoon harrastuspolun harrastusryhmät alkavat viikolla 36/Recreational groups of the Espoo Hobby Path to start in week 36
Korhonen Joonas (KOR)
Piilotettu
2022-09-05 08.05

<p>Espoon harrastuspolku tarjoaa <strong>maksuttomia</strong> <strong>harrastuksia</strong> 3.–9.-luokkalaisille lapsille ja nuorille koulupäivien yhteyteen omalla koululla tai sen läheisyydessä. Joitakin harrastuksia järjestetään myös etäyhteydellä. Harrastuspolulla pääsee innostumaan ja oppimaan uuden harrastuksen äärellä ja saattaa jopa löytää polun tavoitteellisemman harrastuksen pariin.</p>
<p>Harrastusta voi tulla kokeilemaan kavereiden kanssa tai yksin – harrastamisen yhteydestä saattaa löytää myös uusia kavereita. Harrastuksiin voi osallistua osaamistasosta riippumatta.</p>
<p>Lasten ja nuorten toiveiden mukaisesti tarjolla on muun muassa kulttuuria ja taidetta, käsillä tekemistä sekä pelaamista ja koodausta. Oman koulun harrastusryhmän lisäksi tarjolla on <a href="https://www.espoo.fi/fi/harrastusryhmat-espoon-harrastuspolulla#section-41485">maksuttomia harrastusryhmiä muissa tiloissa ja etäyhteydellä</a>. Lisäksi Harrastuspolku täydentää hyvin jo tarjolla olevia <a href="https://www.espoo.fi/fi/liikunta-ja-luonto/liikuntaryhmat-ja-kurssit/lasten-ja-nuorten-liikunta">maksuttomia liikuntaharrastuksia</a>.</p>
<p>Espoon harrastuspolku toteuttaa harrastamisen Suomen mallia Espoossa. <a href="https://harrastamisensuomenmalli.fi/">Voit lukea lisää harrastamisen Suomen mallista sen verkkosivuilta.</a></p>
<p>Lihaskunto-, voima- ja kuntosaliharjoittelussa keskityt oikeaoppisten ja turvallisten tekniikoiden opetteluun sekä kestovoiman ja liikehallinnan parantamiseen leikkimielisyyttä unohtamatta. Pääset lisäämään maltillisesti voimaa ja lihasmassaa kehittävän harjoittelun osuutta, kun tekniikkasi kehittyvät. Et tarvitse aiempaa kokemusta osallistumista varten, vaan huomioimme jokaisen yksilöllisen lähtötilanteen ja toiveet.<br>
&nbsp;</p>
<p>Aika: Keskiviikkoisin klo 14.30 ja 15.30, kesto 60 minuuttia/ryhmä.<br>
Paikka: Kauklahden koulu, liikuntasali/Kuntosali<br>
Ilmoittautuminen:<a href="https://jalkipoltto.com/kuntosalikoulu-espoo"> Ilmoittaudu tästä.</a><br>
Järjestäjä: Jälkipoltto/Espoon harrastuspolku</p>
<p>Espoon harrastuspolun osallistujat ovat vakuutettuja harrastuksen keston ajan. Ryhmiin ilmoittaudutaan suoraan harrastuksen järjestäjälle, ellei toisin mainita. Harrastusryhmän järjestäjä on yhteydessä huoltajiin harrastuksen käytännön asioista. Harrastuksen järjestäjältä saat tiedon ryhmien ilmoittautumistilanteesta. Olethan yhteydessä harrastuksen järjestäjään, mikäli haluat perua lapsen osallistumisen tai sinulla on kysyttävää harrastuksesta.</p>
<p><strong>Lisätietoja: </strong></p>
<p>Espoon harrastuspolku: <a href="https://www.espoo.fi/harrastuspolku">espoo.fi/harrastuspolku</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Hobbystigen i Esbo erbjuder <strong>gratis hobbyer</strong> för tredje- till niondeklassare barn och unga i samband med skoldagarna i egen skola eller dess närhet. Vissa hobbyer ordnas också på distans. På hobbystigen kan man bli intresserad och lära sig vid en ny hobby och till och med hitta en stig till en mer målmedveten hobby.</p>
<p>Man kan prova på hobbyn med vänner eller ensam – i samband med hobbyverksamhet kan man hitta också nya vänner. Man kan delta i hobbyer oberoende av kompetensnivån.</p>
<p>Utbud av Hobbystigen i Esbo ordnas enlighet med barnens och ungdomarnas önskemål. Det erbjudas bland annat kultur och konst, hantverk samt spel och kodning. Utöver din skolans hobbygrupp finns det gratis <a href="https://www.espoo.fi/sv/hobbygrupper-pa-hobbystigen-i-esbo#section-41485">hobbygrupper i andra lokaler och på distans</a>. Hobbystigen kompletterar väl <a href="https://www.espoo.fi/sv/idrott-motion-och-natur/ledd-motion/idrott-barn-och-unga">de avgiftsfria idrottshobbyerna som redan erbjuds</a>.</p>
<p>Hobbystigen i Esbo genomför den nationella Finlandsmodellen för hobbyverksamhet i Esbo. Du kan läsa mer om Finlandsmodell för hobbyverksamhet på webbplats: https://harrastamisensuomenmalli.fi/sv/.</p>
<p>I muskelkonditions-, styrke- och gymträningen koncentrerar du dig på att lära dig rätta och säkra tekniker samt förbättra muskeluthålligheten och behärskningen av rörelser, utan att glömma lekfullheten. Du får måttligt öka andelen träning som utvecklar styrkan och muskelmassan när dina tekniker utvecklas. Du behöver ingen tidigare erfarenhet för att delta, utan vi beaktar allas individuella utgångslägen och önskemål.</p>
<p>&nbsp;</p>
<p>Evenemangets tidpunkt: onsdagar kl. 14.30 och 15.30, längd 60 minuter/grupp<br>
Plats: Skolan Kauklahden koulu, gymnastiksal<br>
Anmälan: <a href="https://jalkipoltto.com/kuntosalikoulu-espoo">Anmäl dig till en hobby (på finska)</a><br>
Arrangör: Jälkipoltto/ Hobbystigen i Esbo</p>
<p>Deltagare av hobbystigen i Esbo är försäkrade under den tid hobbyn pågår. Man anmäler sig till grupperna direkt till den som ordnar hobbyn. När gruppen har bildats kontaktar den som ordnar hobbygruppen vårdnadshavarna. Vänligen kontakta den som ordnar hobbyn om du vill avboka barnets deltagande.</p>
<p><strong>Ytterligare information: </strong></p>
<p>Hobbystigen i Esbo: <a href="https://www.espoo.fi/hobbystigen-i-esbo">https://www.espoo.fi/hobbystigen</a></p>
<p>The Espoo Hobby Path offers <strong>free</strong> <strong>recreational activities</strong> for 3<sup>rd</sup>-9<sup>th</sup> graders during school days at their own school or in its vicinity. Some activities are also arranged remotely. On the Hobby Path, you can have fun and learn in a new hobby and may even find your way to a more goal-oriented hobby.</p>
<p>You can try out hobbies with friends or alone, and you may also find new friends in connection with hobbies. You can participate in hobbies regardless of your level of expertise.</p>
<p>Espoo Hobby Paths activities are arranged in accordance with the wishes of children and young people, there is, for example, culture and art, hands-on activities, gaming, and coding. In addition to your own school's hobby group, there are free <a href="https://www.espoo.fi/en/recreational-activities-on-espoo-hobby-path#section-41485">hobby groups available in other facilities and by remote connection</a>. The Hobby Path is a good complement to <a href="https://www.espoo.fi/en/sports-and-nature/guided-exercise/exercise-children-and-young-people">the free sports activities already available</a>.</p>
<p>The Espoo Hobby Path implements the Ministry of Education and Culture’s Finnish Model for Leisure Activities in Espoo. Additional information on the Finnish Model for Leisure Activities is available on the website: <a href="https://harrastamisensuomenmalli.fi/en/">https://harrastamisensuomenmalli.fi/en/.</a></p>
<p>In muscle tone, strength and gym training, you will focus on learning correct and safe techniques as well as improving endurance and movement management, not forgetting playfulness. As your techniques evolve, you will be able to moderately increase the share of strength and muscle mass training. No previous experience is needed to participate. We take into account each participant’s individual starting level and wishes.</p>
<p>&nbsp;</p>
<p><strong>Event date/time:</strong> Wednesdays at 14.30 and 15.30, duration 60 minutes/group</p>
<p><strong>Venue:</strong> Kauklahden koulu, school’s gymnasium</p>
<p><strong>Registration:</strong> <a href="https://jalkipoltto.com/kuntosalikoulu-espoo">Register for the hobby here (in Finnish)</a></p>
<p><strong>Organiser</strong>: Jälkipoltto / Espoo Hobby Path</p>
<p>The main language of these hobbies is Finnish.<br>
<br>
The participants of the Espoo Hobby Path are insured for the duration of the hobby activities. Enrolment for hobby groups is directly to the organiser of the hobby. Once a group has been formed, the organiser of the hobby group contacts the parents/guardians. Please contact the organiser of the hobby if you want to cancel your child’s participation.</p>
<p><strong>For more information: </strong></p>
<p>Espoo Hobby Path: <a href="https://www.espoo.fi/en/culture-and-leisure/espoo-hobby-path">espoo.fi/en/hobbypath</a></p>
<p>&nbsp;</p>
