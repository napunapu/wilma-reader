LIIKUNTAPÄIVÄ 28.9
Paloniemi Jasmin (PAL)
Piilotettu
2020-09-25 08.27

<p>Hei oppilaat ja uoltajat!</p>
<p>Ensi <strong>maanantaina 28.9 </strong>pidämme&nbsp;koulussamme liikuntapäivän, jolloin kaikilla on poikkeuksellinen aikataulu. Alla on&nbsp;laitettu aikataulu päivästä luokka-asteittain.</p>
<p>&nbsp;</p>
<p>7. LUOKAT</p>
<p>8. LUOKAT</p>
<p>9. LUOKAT</p>
<p>&nbsp;</p>
<p>Liikunnallisin terveisin</p>
<p>Jasmin ja Tommi</p>
