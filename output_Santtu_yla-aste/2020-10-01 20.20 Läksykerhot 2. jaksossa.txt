Läksykerhot 2. jaksossa
Maasara Elsa (PEK)
Piilotettu
2020-10-01 20.20

<p>Hei!</p>
<p>Läksykerhot jatkuvat 2. jaksossa samoin kuin 1. jaksossa: matemaattisten aineiden läksykerho pidetään torstaisin klo 14.30 KE-luokassa ja reaaliaineiden&amp;kielten läksykerho&nbsp;perjantaisin klo 14.30 luokassa 105.</p>
<p><strong>Läksykerhoon pitää ilmoittautua etukäteen!&nbsp;</strong>Läksykerhoissa on rajallisesti paikkoja, jotta turvavälit on mahdollista säilyttää ja jotta läksykerhoa pitävällä opettajalla on mahdollista auttaa jokaista läksykerhoon tulijaa.&nbsp;</p>
<p>Yt. Elsa Pekkarinen ja Johanna Fleming</p>
