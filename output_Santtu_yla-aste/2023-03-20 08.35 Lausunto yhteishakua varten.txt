Lausunto yhteishakua varten
Tarvainen Outi (Santtu Tarvainen, 9D)
Korpela Panu (Santtu Tarvainen, 9D), Fleming Johanna (FLE)
2023-03-20 08.35

<p>Hei, lausunnot pitää toimittaa eteenpäin viimeistään 28.3.
</p>
<p>Tässä tarkka sanamuoto:<br>
"Palauta seuraavat liitteet suoraan oppilaitoksiin, joihin haet: - Erityisen tuen suunnitelma, kuten HOJKS tai HOKS - Terveydentilaa tai toimintakykyä kuvaava asiantuntijalausunto, mikäli tuen tarve opinnoissa johtuu terveydellisistä syistä tai muusta toimintarajoitteesta - Aikaisemmat koulu- ja opiskelutodistusjäljennökset."</p>
<p>Meillä on lääkärin- ja neuropsykologin lausunnot ja HOJKS, mutta lausunto Santun koulunkäynnistä ei varmaan ole haitaksi. </p>
<p>t. Outi</p>
<p></p>
