Joulukirkko ja valonjuhla peruttu
Korhonen Joonas (KOR)
Piilotettu
2021-12-17 12.21

<p>Hyvät oppilaat ja huoltajat,</p>
<p>joudumme valitettavasti perumaan joulukirkon ja valonjuhlan. Pääkaupunkiseudun koronatilanne on muuttunut nopeasti huonommaksi, ja nyt myös muutamalla koulussamme olleella henkilöllä on todettu koronatartunta.&nbsp;</p>
<p>Ensi maanantain ohjelma on siis normaalin lukujärjestyksen mukainen.</p>
<p>Mikäli oppilaalla on koronaan viittavia oireita, on hänen hyvä jäädä kotiin ja mennä koronatestiin.</p>
<p>yst.</p>
<p>Joonas</p>
<p>&nbsp;</p>
