Santtu väsynyt / kipeä
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Hakkarainen Jari (HAK), Lehmusvuori Teija (LEH)
2021-10-06 13.20

<p>Hei,</p>
<p>Santtu oli niin väsynyt että ei jaksanut osallistua musiikin tunnille eikä edes kävellä kotiin. Oletan, että on tulossa kipeäksi.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
