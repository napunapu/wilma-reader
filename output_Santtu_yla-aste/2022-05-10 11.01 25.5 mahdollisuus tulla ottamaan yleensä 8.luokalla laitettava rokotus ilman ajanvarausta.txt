25.5 mahdollisuus tulla ottamaan yleensä 8.luokalla laitettava rokotus ilman ajanvarausta
Mikkonen Heta (H.M)
Piilotettu
2022-05-10 11.01

<p>Hei,</p>
<p>Lukuvuosi lähenee loppuaan. Mikäli sinulle ei ole ehditty tänä vuonna tehdä&nbsp;terveystarkastusta, tervetuloa tarkastamaan rokotustilanteesi 25.5 klo 8.30-14.30 välillä. Samalla on mahdollista halutessasi tarkastaa esim. kasvu. Mikäli haluaisit keskustella tarkemmin terveyteesi liittyvistä asioista, olethan minuun yhteydessä ajan varaamiseksi. Olet myös tervetullut käymään avoajallani, joka on pääsääntöisesti ma-pe klo 11.20-11.45.</p>
<p>Ystävällisin terveisin, Terveydenhoitaja Heta Mikkonen</p>
