Re: Santulla fysioterapeutti tänään
Lehmusvuori Teija (LEH)
Korpela Panu (Santtu Tarvainen, 9D)
2022-02-17 11.47

<p>Hei Panu,</p>
<p>kiitos tiedosta.<br>
Huomasin eilen englannin kokeessa,<br>
että Santulla oli haasteita tehdä luetun ymmärtämistä,<br>
hän ei osannut seurata tekstiä ja vastata kysymyksiin yhtä aikaa<br>
eikä muisti tietenkään riittänyt (ei riittäisi itsellänikään) vastaamaan<br>
yhden lukemisen jälkeen. Aiemmin tätä haastetta en ole huomannut.<br>
On ehkä hyvä, että nyt nähdään, mitkä asiat ovat hänelle uusia haasteita<br>
ja jatkokokeissa pystytään (ehkä) järjestämään tarvittavaa tukea.<br>
Laitan hänelle nyt kaksi arvosanaa, luetun ymmärtäminen mukana ja ilman koko<br>
luetun ymmärtämisen tehtävää.</p>
<p>Ystävällisin terveisin,<br>
Teija LEH</p>
