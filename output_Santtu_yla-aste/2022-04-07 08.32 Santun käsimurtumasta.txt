Santun käsimurtumasta
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Forss Tommi (FOR), Lehmusvuori Teija (LEH)
2022-04-07 08.32

<p>Huomenta!</p>
<p>Santulla oli eilen murtuman kontrollikäynti ja käsi on parantunut hyvin. Huhtikuun ajan tulisi vielä välttää lajeja, jossa käsi voi joutua kontaktiin (esim. pallopelit) tai missä on kaatuma-alttius.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
