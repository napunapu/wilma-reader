Santulla kasvaimen jatkohoitoja
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Fleming Johanna (FLE), Lehmusvuori Teija (LEH)
2021-08-10 11.55

<p>Hei!</p>
<p>Santulla alkoi loppukeväästä väsymys lisääntyä, ja ennen juhannusta ilmaantui ongelmia lähimuistissa. Syyksi osoittautuivat jonkin verran suurentuneet kasvaimen pinnassa olevat nesterakkulat. Näitä saatiin pienennettyä juhannuksen aikaan tehdyssä tähystysleikkauksessa, mutta jonkin verran oireita on edelleen jäljellä. Syöpäsoluja ei näkynyt koepaloissa, mikä on toki hyvä uutinen.</p>
<p>Santulla oli eilen MRI ja kuulemme perjantaina, kuinka jäljellä olevien oireiden kanssa edetään.</p>
<p>Santtu on ollut sitä mieltä, että jaksaa koulussa täydet päivät, mutta opettajien on hyvä olla tietoisia muistiongelmista. Ongelmia on sekä välittömän lähimuistin kanssa (ei välttämättä muista, mitä on keskusteltu pari minuuttia aiemmin), että keskipitkässä muistissa (ei muistanut viime viikonlopun ilmailunäytöstä seuraavana päivänä, että olimme olleet katsomassa sitä).</p>
<p>Lääkärinlausunto aiheesta: https://1drv.ms/b/s!Ao_mjRTNIOtpg5gMwO4Jn5IOw-mvEw?e=40pypQ&nbsp; Laitan paperiversion Santun mukaan.</p>
<p>Santun ja vanhempien luvalla tästä saa kertoa Santun opettajille, erityisopettajalle, rehtorille ja kouluhoitajalle, kuinka koettekaan tarpeelliseksi.</p>
<p>Terveisin, Panu ja Outi</p>
