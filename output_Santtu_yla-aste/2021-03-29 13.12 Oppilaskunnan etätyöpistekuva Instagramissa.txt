Oppilaskunnan etätyöpistekuva Instagramissa
Voutilainen Mikko (VOU)
Piilotettu
2021-03-29 13.12

<p>Hei!&nbsp;<br>
<br>
Koulumme oppilaskunta on ideoinnut kuvakisan etätyöpisteistä&nbsp;koulumme Instagramiin. Koulun Instagram löytyy käyttäjänimellä @kauklahdenkoulu.</p>
<p>Osallistu kuvakisaan lähettämällä kuvasi omasta etätyöpisteestä oppilaskunnan ohjaavalle opettajalle Elsa Pekkariselle sähköpostilla&nbsp;osoitteeseen elsa.pekkarinen@eduespoo.fi. Kirjoita sähköpostiviestiin kuvan lisäksi koko nimesi&nbsp;ja luokkasi. Lähettämällä kuvan sitoudut siihen, että lähettämäsi kuva voidaan julkaista koulun Instagram-tilillä.</p>
<p>Arvovaltainen raati&nbsp;valitsee&nbsp;kuvista&nbsp;viisi parasta jokaiselta luokkatasolta (7lk, 8lk ja 9&amp;10lk) ja henkilökunnan keskuudesta. Parhaat kuvat julkaistaan Instagramissa, joista koulun tilin&nbsp;seuraajat pääsette äänestämään&nbsp;oman suosikkinne. Luokkatason eniten ääniä saanut työpiste etenee ETÄTYÖPISTEFINAALIIN, jonka&nbsp;voittajan valitsette te Instagramin seuraajat äänestämällä omaa suosikkianne.&nbsp;Voittajan työpiste palkitaan Kauklahden koulun hienoimman etätyöpisteen tittelillä ja pienellä palkinnolla.&nbsp;</p>
<p>Lähetä työpisteesi kuva, nimesi ja luokkasi osoitteeseen elsa.pekkarinen@eduespoo.fi&nbsp;viimeistään tiistaihin klo 18.00 mennessä,&nbsp;niin pääset osallistumaan huikeiden&nbsp;palkintojen ja suuren kunnian tuovaan kisaan!</p>
<p>Luokkatasojen parhaat äänestetään&nbsp;keskiviikkona Instagramissa&nbsp;ja viikko huipentuu&nbsp;suureen ETÄTYÖPISTEFINAALIIN torstaina!</p>
<p>Oppilaskunnan puolesta Mikko Voutilainen, oppilaskunnan ohjaava opettaja</p>
