KOULUILLA ANNETUT 12-15-VUOTAIDEN KORONAROKOTUKSET
Mikkonen Heta (H.M)
Piilotettu
2021-08-18 13.31

<p><strong>KOULUILLA &nbsp;ANNETUT 12-15-VUOTAIDEN KORONAROKOTUKSET</strong></p>
<p>Oppilailla oli mahdollisuus saada koronarokotuksia Kauklahden&nbsp;koulussa 16.8-18.8&nbsp; välisenä aikana. Mikäli haluatte syksyn aikana vielä myöhemmin &nbsp;rokotuksen, rokotusaika tulee varata jatkossa kaupungin rokotuspisteeltä. Sähköinen ajanvaraus <a href="https://eur03.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.koronarokotusaika.fi%2F&amp;data=04%7C01%7C%7C6a94b0bcd4154b43fc2408d9621c2869%7C6bb04228cfa542139f39172454d82584%7C1%7C0%7C637648695350962690%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&amp;sdata=aeTn%2FgP853LuXHZQXckX23MqHgKO95iqYZK9bTWy474%3D&amp;reserved=0">koronarokotusaika.fi</a>.&nbsp;on  mahdollista&nbsp;myös&nbsp;12–15-vuotiaille. &nbsp; Ilman ajanvarausta ensimmäinen rokoteannos on saatavilla &nbsp;myös Iso Omenan&nbsp;walk-in-rokotuspisteellä. Kaupungin muihin&nbsp;rokotuspisteisiin tulee varata aika.&nbsp;&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Tehosteannos&nbsp;</strong></p>
<p>Jos nuori on saanut ensimmäisen rokotuksen koulussa, myös&nbsp;tehosterokote&nbsp;annetaan koulussa.  Tehosteannoksen aikataulusta tiedotetaan koulukohtaisesti Wilmassa elokuun aikana. Vastaavasti, jos nuori on saanut ensimmäisen rokotuksen rokotuspisteellä, myös tehosterokote&nbsp;annetaan rokotuspisteellä.</p>
<p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Lisätietoa&nbsp;</strong></p>
<p>Espoon koronaneuvonta- ja ajanvaraus p. 09 816 34600 (ma-pe klo 7–18 ja la-su 9–15).&nbsp;</p>
<p>Oma kouluterveydenhoitaja&nbsp;&nbsp;</p>
<p><u><a href="https://eur03.safelinks.protection.outlook.com/?url=https%3A%2F%2Fwww.espoo.fi%2Ffi-FI%2FEspoon_kaupunki%2FAjankohtaista%2FKoronavirus%2FSosiaali_ja_terveyspalvelut%2FKoronarokotus&amp;data=04%7C01%7C%7C6a94b0bcd4154b43fc2408d9621c2869%7C6bb04228cfa542139f39172454d82584%7C1%7C0%7C637648695350962690%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&amp;sdata=7zbLsaairoV7RT86L%2BJAeCitkXZ0mWDuUEZ5iG20rEs%3D&amp;reserved=0">https://www.espoo.fi/fi-FI/Espoon_kaupunki/Ajankohtaista/Koronavirus/Sosiaali_ja_terveyspalvelut/Koronarokotus</a></u>&nbsp;</p>
