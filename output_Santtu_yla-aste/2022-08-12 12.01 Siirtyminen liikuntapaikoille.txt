Siirtyminen liikuntapaikoille
Paloniemi Jasmin (PAL)
Piilotettu
2022-08-12 12.01

<p><strong>Hyvä huoltaja!</strong></p>
<p>Espoon suomenkielinen perusopetus on ohjeistanut kaikkia kouluja tarkentamaan koulupäivän aikana tapahtuvia siirtymisiä koulun ulkopuolisiin paikkoihin. Sen vuoksi&nbsp;<strong>kysymme huoltajilta lupaa itsenäiseen siirtymiseen liikuntapaikoille.&nbsp;</strong>Jos lapsenne saa siirtyä itsenäisesti liikuntapaikoille, ei viestiin tarvitse vastata. Mikäli ette anna lupaa, pyydämme teitä ystävällisesti vastaamaan tähän viestiin.</p>
<p>Aamutunneille oppilaat tulevat suoraan kotoa ja koulupäivän viimeiseltä tunnilta lähtevät suoraan kotiin. Kesken koulupäivän oppilaat siirtyvät liikuntapaikalle ja takaisin koululle opettajan ohjeiden mukaan liikennesääntöjä noudattaen. Liikuntapaikoille voi siirtyä joko kävellen tai pyörällä.&nbsp;<strong>Pyöräilijöille tiedoksi</strong>: Pyöräilykypärän käyttö on&nbsp;<strong>pakollista</strong>. Jos oppilas ei kuitenkaan tästä huolimatta käytä kypärää,&nbsp;<u>hän ei kuulu matkalla koulun vakuutuksen piiriin.</u></p>
<p>Jokaisen jakson alussa lähetämme Wilma-viestin, josta selviää sen jakson liikuntapaikat. Jotta liikuntatunnit sujuisivat jouhevasti ja liikuntatarjonta olisi monipuolista, toivomme, että mahdollisimman moni oppilas saa luvan itsenäiseen siirtymiseen.</p>
<p>7-luokkalaisten kanssa käymme turvalliset reitit liikutatunneille läpi yhdessä ensimmäisillä tunneilla.</p>
<p>&nbsp;</p>
<p>Ystävällisin terveisin liikunnanopettajat</p>
<p>Jasmin Paloniemi ja Tommi Forss</p>
<p>&nbsp;</p>
