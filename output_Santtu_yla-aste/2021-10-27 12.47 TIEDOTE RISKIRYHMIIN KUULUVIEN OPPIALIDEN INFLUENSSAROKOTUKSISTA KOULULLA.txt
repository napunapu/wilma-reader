TIEDOTE RISKIRYHMIIN KUULUVIEN OPPIALIDEN INFLUENSSAROKOTUKSISTA KOULULLA
Mikkonen Heta (H.M)
Piilotettu
2021-10-27 12.47

<p><strong>KOULU- JA OPISKELUTERVEYDENHUOLTO </strong></p>
<p>TIEDOTE RISKIRYHMÄÄN KUULUVIEN OPPILAIDEN INFLUENSSAROKOTUKSET KOULUILLA</p>
<p>&nbsp;</p>
<p><strong>AJANVARAUKSELLA</strong></p>
<p>Huomioittehan, että jos varaatte sähköisesti ajan useammalle henkilölle, jokaiselle tulee varata erillinen aika. Influenssarokotteita on rajallinen määrä koululla. Varattu ajanvarausaika on ohjeellinen.</p>
<p>Aikoja ei anneta Wilman kautta</p>
<p>&nbsp;</p>
<p><strong>ILMAN AJANVARAUSTA</strong> opiskelija voi tulla <strong>oman oppilaitoksensa</strong> terveydenhoitajalle</p>
<p><strong>Päivä:&nbsp; 1.11&nbsp; &nbsp;8.30-10.30 7.luokkalaiset</strong></p>
<p><strong>Päivä:&nbsp;</strong> <strong>1.11&nbsp; &nbsp;12-14 8.luokkalaiset</strong></p>
<p><strong>Päivä: 15.11 klo 8.30-10.30 9. ja 10. luokkalaiset</strong></p>
<p>Opiskelijan on tultava vastaanotolle joko huoltajan kanssa tai itsenäisesti. Opiskelijaa ei haeta luokasta.</p>
<p>Rokotuksen voi käydä myös ottamassa Espoon <strong>muissa toimipaikoissa </strong>järjestettävissä influenssarokotuspäivissä, joiden ajankohdista ja paikoista saatte tietoa <a href="https://www.espoo.fi/fi/influenssarokotukset-espoossa">https://www.espoo.fi/fi/influenssarokotukset-espoossa</a></p>
<p><strong>Resepti influenssarokotukseen</strong></p>
<p>&nbsp;</p>
<p>Koululääkärit kirjoittavat tarvittaessa influenssarokotereseptejä opiskelijoille, jotka eivät kuulu riskiryhmiin tai reseptejä saa Espoon terveysasemilta <a href="https://www.espoo.fi/fi/terveys/terveysasemat">https://www.espoo.fi/fi/terveys/terveysasemat</a></p>
