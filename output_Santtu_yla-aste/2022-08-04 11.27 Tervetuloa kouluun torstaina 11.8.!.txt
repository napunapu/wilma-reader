Tervetuloa kouluun torstaina 11.8.!
Korhonen Joonas (KOR)
Piilotettu
2022-08-04 11.27

<p>Hei!</p>
<p>Lukuvuosi 2022-23 alkaa torstaina 11.8. Uusien seiskojen koulu alkaa yhdeksältä, jolloin kokoonnutaan pihalle koulun eteen. 8. ja 9. luokkalaiset puolestaan saapuvat kotiluokkiinsa kello 10.</p>
<p>Sydämellisesti tervetuloa!</p>
<p>&nbsp;</p>
