Santulla laboratorio tänään
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Hakkarainen Jari (HAK), Lehmusvuori Teija (LEH)
2023-03-21 08.42

<p>Hei!</p>
<p>Santtu on tänään suurimman osan musiikin tuntia poissa laboratoriokäynin vuoksi.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
