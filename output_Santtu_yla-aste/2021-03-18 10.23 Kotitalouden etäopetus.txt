Kotitalouden etäopetus
Littunen Petra (LIT)
Piilotettu
2021-03-18 10.23

<p>Hei,</p>
<p>Koska etäopetuksessa osa oppilaista on jättänyt tehtävät tekemättä lähetän vanhemmille infoa;</p>
<p>Kotitalouden etäopetuksessa oppitunnit koostuvat sekä teoriasta (yhteisestä meet- opetuksesta) että kotona tehtävistä työtehtävistä (toiminnasta). Luokan classroom alustalla on oppituntikohtaiset opetusmateriaalit ja tehtävät tarkkoine ohjeistuksineen.</p>
<p>Viime viikon 1. etäopetustunnilla oli tarkoituksena pestä pyykkiä vanhempien apulaisena. Osa oppilaista suoritti tämän hienosti, mutta osalta tehtävä puuttuu vielä, wilma merkinnät laitettu niistä.</p>
<p>Tämän viikon 2. etäopetuksessa tehtäväksi tuli omavalintaisen kääretortun leipominen ja (vanhemmille) kauppalistan kirjoittaminen puuttuvista raaka-aineista. Koulu ei pysty tarjoamaan raaka-aineita, joten tehtävä on mahdollista suorittaa myös kirjallisena (opettajaan silloin yhteys wilma-viestillä).</p>
<p>Seuraavan viikon, 3. etäopetuskerran aikana harjoittelemme siivouksen teoriaa ja toisella oppitunnilla oppilaat siivoovat oman huoneensa tai kodin olohuoneen.</p>
<p>Toiminta eli toinen oppitunti sijoittuu joko toiselle kotitalouden oppitunnille klo. 9.45-11 tai oppilas voi halutessaan suorittaa tehtävät myös vanhempien kotona ollessa esimerkiksi illalla tai viikonloppuna. Viikkokohtaisen tehtävän palautus on sovittu oppilaiden kanssa yhdessä, suorittamiseen on aikaa useampi päivä.</p>
<p>Toivoisin, että oppilaat ryhtyisivät hommiin oma-aloitteisesti.&nbsp;Asiat on harjoiteltu yhdessä oppitunnilla&nbsp;ja niiden suorittaminen kotona pitäisi hallita,&nbsp;apuna saa&nbsp;vanhemmilta ja oppikirjasta olla mutta oppilaan puolesta ei saa tehdä. :)</p>
<p>&nbsp;</p>
<p>Ystävällisin terveisin,</p>
<p>Petra kotitalousopettaja</p>
