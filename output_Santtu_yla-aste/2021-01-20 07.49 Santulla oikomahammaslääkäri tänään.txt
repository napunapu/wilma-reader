Santulla oikomahammaslääkäri tänään
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Forss Tommi (FOR), Kultanen Tiia (KUL)
2021-01-20 07.49

<p>Hei,</p>
<p>Santulla on tänään oikomahammaslääkäri Leppävaarassa klo 13.20 - 13.50. Hänen tulisi lähteä ruotsin tunnilta klo 12.45. En usko että ehdimme järkevästi takaisin liikuntatunnille.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
