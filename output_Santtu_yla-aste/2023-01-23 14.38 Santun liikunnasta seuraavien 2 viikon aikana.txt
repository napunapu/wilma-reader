Santun liikunnasta seuraavien 2 viikon aikana
Fleming Johanna (FLE)
Tarvainen Santtu, 9D, Tarvainen Outi (Santtu Tarvainen, 9D), Korpela Panu (Santtu Tarvainen, 9D), Forss Tommi (FOR)
2023-01-23 14.38

<p>Hei,</p>
<p>&nbsp;</p>
<p>Siis juttelin Tommi-open kanssa ja hänelle sopi hyvin että Santtu korvaa liikuntatunnit luistelun ajan fysioterapialla, futis ja maalipallolla sekä hiihdolla. Laitatteko vaikka isä ja Santtu näistä kuvaa ja tietoa Tommille sähköpostitse: tommi.forss@espoo.opetus.fi</p>
<p>&nbsp;</p>
<p>Tänään Santtu teki liikkatunnilla luonani matikan kokeen puoliksi ja jatkaa nyt pe klo 14.30 läksykerhossa loppuun. Liikka tunnit ovat aina viimeisiä tunteja niin Santtu saa silloin lähteä sitten jo kotiin.</p>
<p>&nbsp;</p>
<p>: ) Johanna</p>
