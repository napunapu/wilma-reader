"Syysloma on lukuloma"
Korhonen Joonas (KOR)
Piilotettu
2022-10-14 09.24

<p>Hyvät oppilaat ja huoltajat!</p>
<p>Ensi viikolla on syysloma. Olkoon se kaikille rentouttava ja mukava! Yksi tehokkaimmista rentoutumisen muodoista&nbsp;lienee&nbsp;lukeminen, joten sysloma voi olla lukulomakin.&nbsp;Ohessa opetushallituksen tiedote asiasta.</p>
<p><em><u>Syysloma on Lukuloma – askel askeleelta kirjamatkalle!</u></em></p>
<p><br>
<em>Koulujen syyslomalla perheitä kannustetaan viettämään lukulomaa lukemalla yhdessä. Kampanja toteutetaan nyt neljättä kertaa.</em></p>
<p><br>
<em>Lukeminen on innostava ja kaikille sopiva harrastus iästä ja taustasta riippumatta. Lukijaksi voi alkaa lapsena tai aikuisiällä, ja kirjastojen ja kirjakauppojen ammattilaiset auttavat löytämään kiinnostuksen kohteisiin ja taitotasoon sopivaa luettavaa.</em></p>
<p><br>
<em>Lukuloma-kampanja innostaa lukemaan pienin askelin. Kampanjan verkkosivuille lukuloma.fi on koottu kirjavinkkejä eri-ikäisille sekä lukemaan kannustavia värityskuvia. Sivuilta löytyy myös kouluikäisille suunnattu tarinallinen lukuhaaste, jossa on uusi tehtävä loman jokaiseen päivään. Sosiaalisessa mediassa kampanja näkyy tunnisteilla <strong>#lukuloma</strong> ja <strong>#läslov.</strong></em></p>
<p><br>
<em>Kampanjan taustalla on Suomen kirja-alan järjestöjen ja lukutaitotoimijoiden huoli lukuinnon ja lukemisharrastuksen vähenemisestä. Lukemisen väheneminen näkyy myös lukutaitotutkimuksissa, joiden mukaan erot erityisesti nuorten lukutaidossa ovat kasvaneet 2000-luvulla selvästi. Lukuinto voi kuitenkin puhjeta milloin vain ja lukemisharrastus voi alkaa missä vaiheessa elämää tahansa. Sähkö- ja äänikirjojen myötä kirjaan on yhä helpompi tarttua missä vain, ja lukeminen ja kirjallisuudesta nauttiminen saa yhä uusia muotoja.</em></p>
<p><br>
<em>Myös tiede- ja kulttuuriministeri <strong>Petri Honkonen</strong> on mukana tukemassa kampanjaa. Ministeri Honkonen muistuttaa lukemisen merkityksestä yksilölle ja yhteiskunnalle:</em></p>
<p><br>
<em>- Lukemista ja lukutaitoa kutsutaan supervoimaksi, eikä tämä ole liioittelua. Lukemisella on tunnustettuja terveysvaikutuksia, kuten rauhoittuminen, rentoutuminen ja muistin harjoittaminen. Lukeminen on myös rauhantyötä: se edistää empatiakykyä ja auttaa meitä ymmärtämään muita ihmisiä ja muita kulttuureita. Asettumaan toisen asemaan. Näistä taidoista on apua maailmassa, joka muuttuu nopeasti ja tuo eteemme jatkuvasti uusia haasteita, ministeri Honkonen sanoo.</em></p>
<p><br>
<em>Syyslomaa vietetään kouluissa lokakuussa viikoilla 42-43, tarkat päivät vaihtelevat kunnittain ja kouluittain.<br>
Kampanjan toteuttavat yhteistyössä Finlands svenska författareförening, Kirjakauppaliitto, Kirjasampo, Kirjastokaista, Lastenkirjainstituutti, Lukuliike - Opetushallitus, Lukukeskus, Sivupiiri, Suomen Kirjailijaliitto, Suomen Kirjastoseura, Suomen Kirjasäätiö, Suomen Kustannusyhdistys, Suomen tietokirjailijat ja Äidinkielen opettajain liitto.</em></p>
<p><em>Ystävällisin terveisin</em></p>
<p><em>Lukuliike - Opetushallitus</em></p>
<p><em><a href="http://www.lukuliike.fi">www.lukuliike.fi</a><br>
<a href="http://www.lukuloma.fi">www.lukuloma.fi</a></em></p>
