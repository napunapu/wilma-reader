Santulla iltapäivällä terveydellinen poissaolo
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Lehmusvuori Teija (LEH), Forss Tommi (FOR)
2021-10-08 11.55

<p>Hei!</p>
<p>Santtu on tänään poissa iltapäivän tunnilta voimakkaan väsymyksen vuoksi.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
