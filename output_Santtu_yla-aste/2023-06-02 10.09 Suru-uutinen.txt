Suru-uutinen
Korhonen Joonas (KOR)
Piilotettu
2023-06-02 10.09

<p>Hyvä huoltaja,&nbsp;<br>
&nbsp;</p>
<p>valitettavasti joudun kertomaan, että rakas opettajamme&nbsp;Eero Lahtinen menehtyi eilen sairaskohtaukseen. Olemme kertoneet asiasta tänään koulun oppilaille, ja koululla on tarjottu keskusteluapua kaikille sitä tarvitseville oppilaille ja henkilökunnan jäsenille.&nbsp;</p>
<p>Kaikilla oppilailla on halutessaan mahdollisuus keskustella aiheesta oppilashuollon tai muiden koulun aikuisten kanssa. Kodeissakin voi olla hyvä keskustella aiheesta, jos aihe mietityttää lapsia ja nuoria.&nbsp;</p>
<p>Järjestämme koululla tänään hiljaisen hetken ja suruliputuksen.</p>
<p>ystävällisin terveisin,</p>
<p>Joonas Korhonen, rehtori</p>
