1. jakson liikuntaohjelma(9.lk perusliikunta)
Forss Tommi (FOR)
Piilotettu
2022-08-12 14.43

<p>Moi!</p>
<p>Muistathan hommata ajoissa liikuntaan sopivan varustuksen, erityisesti juoksulenkkarit.</p>
<p><strong>9A/C&nbsp; &nbsp;LAJI&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; PAIKKA&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;MUUTA</strong></p>
<p><strong>Ma 15.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Pe 19.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 22.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Pe 26.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 29.8 </strong>yleisurheilu(2000m juoksu) Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Pe 2.9</strong> jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Ma 5.9 </strong>jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Pe 9.9 </strong>jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Ma 12.9 </strong>ulkokuntosali Hansavalkama ulkoliikuntavarustus + vesipullo</p>
<p><strong>Pe 16.9 </strong>LIIKUNTAPÄIVÄ</p>
<p><strong>Ma 19.9 </strong>pesäpallo koulun kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Pe 23.9</strong> pesäpallo/lenkki koulun kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 26.9 </strong>toiveliikuntatunti aloitus koululta</p>
<p><strong>Pe 30.9</strong> maastojuoksu Palolampi juoksutossut + vesipullo</p>
<p>&nbsp;</p>
<p><strong>9B/D&nbsp; &nbsp;LAJI&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;PAIKKA&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;MUUTA</strong></p>
<p><strong>Ma 15.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 22.8</strong> yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 29.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 5.9 </strong>yleisurheilu(2000m juoksu) Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ma 12.9 </strong>jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Ma 19.9 </strong>jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Ma 26.9 </strong>maastojuoksu Palolampi juoksutossut + vesipullo</p>
<p>&nbsp;</p>
<p><strong>9E/F/G&nbsp; &nbsp;LAJI&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;PAIKKA&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; MUUTA</strong></p>
<p><strong>Ke 17.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>To 18.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ke 24.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>To 25.8 </strong>yleisurheilu Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ke 31.8 </strong>yleisurheilu(2000m juoksu) Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>To 1.9 </strong>jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Ke 7.9 </strong>jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>To 8.9</strong> jalkapallo Hansavalkama futiskengät + vesipullo</p>
<p><strong>Ke 14.9 </strong>ulkokuntosali Hansavalkama ulkoliikuntavarustus + vesipullo</p>
<p><strong>To 15.9 </strong>katukoris Mankin kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ke 21.9 </strong>pelit/leikit Hansavalkama ulkoliikuntavarustus + vesipullo</p>
<p><strong>To 22.9 </strong>pesäpallo koulun kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>Ke 28.9 </strong>pesäpallo koulun kenttä ulkoliikuntavarustus + vesipullo</p>
<p><strong>To 29.9 </strong>maastojuoksu Palolampi juoksutossut + vesipullo</p>
<p>&nbsp;</p>
