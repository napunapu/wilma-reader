9. luokan ruotsin opiskelu
Mattila Riitta (MAT)
Piilotettu
2022-09-05 19.34

<p>Hei 9.lk ruotsin&nbsp;oppilaani ja huoltajat!</p>
<p>Pieni katsaus 9. luokan ruotsin opiskeluun ja arviointiin:</p>
<p>Opiskelussa yläkoulussa korostuu sinnikäs, jatkuva työskentely ja vastuun ottaminen omasta opiskelusta. Sanaston opiskelu on kielen opiskelussa ensiarvoisen tärkeää ja sanakokeita pidetään, vaikka ei välttämättä &nbsp;jokaisesta kappaleesta.&nbsp;Sanojen tai muun opiskellun aineksen suullisesti osaaminen kyseltäessä&nbsp;on ihan yhtä tärkeää kuin kirjalliset sanakokeet!&nbsp; Ruotsin kirjasarjaan Megafon voi ladata kännykkään maksuttoman Otso-sovelluksen. Sitä kautta oppilas saa kirjasarjan digitaaliset materiaalit käyttöönsä, esim. äänitteet. Lisäkuuntelu kotona auttaa sinua sanojen ääntämisen ja kuullunymmärtämisen vahvistamisessa, otathan sen aktiiviseen käyttöön! Harjoittelemme ja kertaamme myös kielioppiasioita ja tutustumme eri Pohjoismaihin ja niiden kulttuuriin.</p>
<p>Ruotsin&nbsp;arviointi perustuu siihen, kuinka hyvin ja missä määrin oppilas on saavuttanut opetussuunnitelmassa ruotsille&nbsp;asetetut tavoitteet. Tavoitteet on jaettu neljään osaan: (1) kasvu kulttuuriseen moninaisuuteen ja kielitietoisuuteen, (2) kielen opiskelutaidot (3) taito toimia vuorovaikutuksessa ja (4) taito tulkita ja tuottaa tekstejä sekä puhetta. Käytännössä tämä kaikki voi olla mm. ääntämisen harjoittelua, sanakokeita, suullisia pariharjoitteita, ääneen lukemisen äänittämistä, laajempia kirjallisia kokeita, pieniä suullisia esityksiä, perustuntityöskentelyä, pelejä, kirjoitelmia, suullisen osaamisen videointia, kielen rakenteiden opiskelua, kuullun ja luetun ymmärtämisen testejä, ajankohtaisia uutisaiheita jne. Arviointiin sisältyy myös itse- ja vertaisarviointi sekä isona osana työskentelyn arviointi.&nbsp;</p>
<p>Koulussamme on tässä ensimmäisessä jaksossa perjantaisin erityisopettaja Johanna Flemingin pitämä läksykerho klo 14.30 alkaen ja tarvittaessa etsimme yhteistä tukiopetusaikaa. Läksykerhoon pitää ilmoittautua etukäteen joko suoraan Johannalle tai opettajan kautta. Tunneilla käymme kuitenkin kaikki asiat läpi, joten keskittyminen ja aktiivisuus tunneilla ja säännöllinen läksyjen teko&nbsp;useimmiten riittävät.</p>
<p>Muistathan, että sanakokeista, verbikokeista tai muista pienistä testeistä tulee tieto vain wilman läksyosioon, ei erilliseen koevarauslistaan.</p>
<p>Jos ruotsin&nbsp;opiskelusta on kysyttävää, olettehan yhteydessä!</p>
<p>Ystävällisin terveisin,</p>
<p>ru-opettaja Riitta Mattila</p>
<p>&nbsp;</p>
