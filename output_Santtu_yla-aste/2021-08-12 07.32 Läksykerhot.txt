Läksykerhot
Maasara Elsa (PEK)
Piilotettu
2021-08-12 07.32

<p>Hei!</p>
<p>Läksykerhot aloittavat jälleen ensi viikolla toimintansa. Läksykerhoja on kaksi: torstaisin reaaliaineissa ja kielissä sekä perjantaisin&nbsp;matematiikassa, fysiikassa ja kemiassa. Reaaliaineiden ja kielten&nbsp;läksykerho pidetään torstaisin klo 14.30 luokassa 218 ja matemaattisten aineiden&nbsp;läksykerho&nbsp;perjantaisin klo 14.30 KE-luokassa.</p>
<p>Syksyn ensimmäisten viikkojen aikana läksykerhot toimivat&nbsp;hieman poikkeuksellisesti:</p>
<p>• Viikolla 33 läksykerho on vain matemaattisissa aineissa perjantaina 20.8.</p>
<p>• Viikolla 34 läksykerho on vain reaaliaineissa ja kielissä torstaina 26.8.</p>
<p>Eli viikosta 35 eli 30.8. eteenpäin läksykerhot toimivat sekä torstaisin että perjantaisin.</p>
<p>Läksykerhoon voi tulla pyytämään apua kotitehtäviin, koekertaukseen ym.&nbsp;Matemaattisten aineiden läksykerhoa pitää Elsa Pekkarinen ja reaaliaineiden&amp;kielten läksykerhoa pitää erityisopettaja Johanna Fleming.</p>
<p>Läksykerhoihin pitää&nbsp;<strong>ilmoittautua etukäteen</strong>, jotta turvavälit olisi mahdollista säilyttää. Ilmoittautumisen voi hoitaa oppilas itse, oppilaan huoltaja, luokanvalvoja tai aineenopettaja. Ilmoittautumisen voi hoitaa kasvotusten tai esimerkiksi wilmaviestillä.</p>
<p>Nähdään läksykerhoissa!</p>
<p>Yt. Elsa Pekkarinen ja Johanna Fleming</p>
