Tiistaina vietetään koulussamme seurapaitapäivää!
Huhtala Tiina (T.H)
Piilotettu
2022-09-26 14.14

<p>&nbsp;</p>
<p>Hei kaikki Kauklahden koulun oppilaat, huoltajat ja henkilökunnan jäsenet!</p>
<p>Huomenna tiistaina 27.9. vietetään valtakunnallista "Seurapaitapäivää" kouluissa ja työpaikoilla - niin myös meillä! <a href="https://www.olympiakomitea.fi/seuratoiminta/kampanjat/seurapaitapaiva/">Lue täältä lisää.</a></p>
<p>Kannustan kaikkia kaivelemaan kaappiensa perukoilta edustamasi tai kannattamasi seuran asuja. Voit aivan vapaasti pukea päällesi myös muun kuin urheiluseuran asuja. Jos olet osa valtakunnallista runolausujien salaseuraa ja teillä on oma T-paita, niin tottakai laitat sen päälle.&nbsp;<img height="23" src="/shared/scripts/ckeditor/plugins/smiley/images/regular_smile.png" width="23">&nbsp; Koulupäivä on muuten ihan normaali lukujärjestyksen mukainen.</p>
<p>Somessa &nbsp;#seurapaitapäivä #seurasydän #kauklahdenkoulu</p>
<p>&nbsp;</p>
<p>Iloa alkavaan viikkoosi!</p>
<p>Terveisin,</p>
<p>Tiina Huhtala</p>
<p>Yhteisöohjaaja</p>
<p>&nbsp;</p>
