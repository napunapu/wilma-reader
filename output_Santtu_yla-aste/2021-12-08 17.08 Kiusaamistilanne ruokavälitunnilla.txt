Kiusaamistilanne ruokavälitunnilla
Tuominen Eeva-Riitta (TUO)
Piilotettu
2021-12-08 17.08

<p>Hei!</p>
<p>Tänään ruokavälitunnilla ollessani valvomassa aulaa huomasin neljän seitsemännen luokan oppilaan oppilaan piirittäneen Santun portaikon luona. Santtu tuntui vastailevan reippaana hymyilevään tyyliinsä poikaryhmän&nbsp;juttuihin, mutta ruumiinkielestä näin, ettei kaikki ole ihan kunnossa ja menin paikalle. Pojat olivat huijanneet Santun sanomaan englanniksi "polvi kasvaa" ja syyttivät Santtua hyvin pahan, rasistisen sanan käytöstä. Sanoin pojille heidän käytöksensä olevan kiusaamista ja kerroin asiasta poikien luokanvalvojalle. Hän lupasi puhutella luokkansa pojat heidän ikävästä käytöksestään. Kysyin Santulta, tuntuiko tilanne hänestä ahdistavalta ja hän kertoi tilanteen olleen kurja.&nbsp;On ahdistavaa ja epäreilua, kun syytetään rasisitisesta kielenkäytöstä syyttä ja vielä joukkovoimalla. Osa ryhmän pojista on varsin kookkaita ja itsevarmoja. Sanoin Santulle, että jos tällaista tapahtuu uudelleen, asiasta kannattaa kertoa joko luokanvalvojalle, minulle tai valvovalle opettajalle.&nbsp;</p>
<p>Minulla oli tilanteen jälkeen kahden kiusaajajoukkoon kuuluneen oppilaan tunti&nbsp;ja puhuin heille vakavasti asiasta. Molemmat lupasivat pyytää Santulta anteeksi.&nbsp;Toivottavasti muistavat pyytää! Lähetin Santun luokanvalvojalle tiedon tapahtuneesta.</p>
<p>Terveiset Santulle. Hän toimi kurjassa&nbsp;tilanteessa reippaasti ja rakentavasti.</p>
<p>Yst. terv. Eeva-Riitta Tuominen, kuvataiteen lehtori</p>
