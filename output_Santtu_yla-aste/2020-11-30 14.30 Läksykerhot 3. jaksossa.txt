Läksykerhot 3. jaksossa
Maasara Elsa (PEK)
Piilotettu
2020-11-30 14.30

<p>Hei!</p>
<p>Läksykerhot jatkuvat 3. jaksossa eli tästä viikosta alkaen seuraavasti: reaaliaineiden&amp;kielten läksykerho&nbsp;torstaisin klo 14.30 luokassa 105 ja&nbsp;matemaattisten aineiden läksykerho perjantaisin klo 14.30 KE-luokassa.</p>
<p><strong>LÄKSYKERHOON PITÄÄ ILMOITTAUTUA ETUKÄTEEN JA ILMOITTAUTUMINEN ON SITOVA!&nbsp;</strong>Läksykerhoissa on rajallisesti paikkoja, jotta turvavälit on mahdollista säilyttää ja jotta läksykerhoa pitävällä opettajalla on mahdollista auttaa jokaista läksykerhoon tulijaa. Läksykerhossa ei voi tehdä rästiin jääneitä kokeita.&nbsp;</p>
<p>Yt. Elsa Pekkarinen ja Johanna Fleming</p>
