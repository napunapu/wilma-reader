Välipalamyynti 14.11. alkaen
Korhonen Joonas (KOR)
Piilotettu
2022-10-31 12.54

<p>Hei!</p>
<p>välipalanmyynti jatkuu koulussa 14.11. lähtien. Yhden välitunnilla maanantaisin, tiistaisin ja torstaisin oppilaat voivat ostaa käteisellä välipalaa koulun ruokalasta.</p>
<p>yst.</p>
<p>Joonas</p>
