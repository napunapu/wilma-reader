Välkommen!
Åberg Suvi (ÅBE)
Piilotettu
2020-08-14 11.09

<p>Hyvät oppilaat ja kotiväki,</p>
<p>Minulla on ilo aloittaa tämän ryhmän kanssa 7. luokan&nbsp;ruotsin opinnot.&nbsp;Toivotan paljon tsemppiä ja onnistumisen riemua opiskeluun! Olettehan yhteydessä Wilman kautta ja seuraatte Wilman tuntimerkintöjä - kiitos yhteistyöstä.&nbsp;Luonnollisesti&nbsp;toivomme kotiväen tukea läksyjen muistuttamisessa jne.</p>
<p>Ruotsin opiskelussa korostuvat parityö ja yhdessä oppiminen. Itsenäistäkin otetta tarvitaan etenkin näinä aikoina, kun yhteistoiminnallisuus on jätettävä hieman vähemmälle. Toisen kotimaisen kielen käyttö&nbsp;voi olla joillekin nuorille välillä arjesta etäällä, mutta onneksi uuden oppiminen voi olla hauskaa ja on aina&nbsp;kehittävää!&nbsp;Ruotsi kuten muukin kielitaito on&nbsp;arvaamattoman arvokasta&nbsp;ja avaa ovia ties minne.&nbsp;Tässä vielä tiedoksi opetussuunnitelma ja arviointikriteerit:&nbsp;<a href="https://eperusteet.opintopolku.fi/#/fi/perusopetus/419550/vuosiluokkakokonaisuus/428782/oppiaine/605637">https://eperusteet.opintopolku.fi/#/fi/perusopetus/419550/vuosiluokkakokonaisuus/428782/oppiaine/605637</a></p>
<p>Vi ses snart!</p>
<p>Suvi Åberg</p>
