Luistelua liikuntatunneilla
Forss Tommi (FOR)
Piilotettu
2021-01-18 10.46

<p>Moi,</p>
<p>viimein saatiin koulun omalle kentälle jää ja päästään luistelemaan.<strong> Luistelut alkavat tällä vkolla</strong>, joten ottakaa mukaan lämmin ulkoliikuntavaatetus(EI FARKKUJA), käsineet,&nbsp;luistimet sekä kypärä(myös pyöräily-/laskettelukypärä käy). Ne, jotka haluavat pelata jääkiekkoa, mukaan myös maila.<strong> Kypärän käyttö on pakollista, </strong>muuten joutuu seuraamaan kentän laidalta<strong>!</strong></p>
<p>Koululla on jonkin verran luistimia, joita voi ensi hätään lainata. Terien kunto ei vain ole ihan priimaa, joten omien varusteiden käyttö on suotavaa.</p>
<p>Tommi</p>
