Kauklahden nuorisotilat
Korhonen Joonas (KOR)
Piilotettu
2021-02-04 10.23

<p>*välitetty*</p>
<p>Tervehdys Kauklahden nuorisotilalta!</p>
<p>Koronatilanteesta johtuen on erittäin tärkeää huomioida nämä asiat:</p>
<p>1. Saavut nuorisotilalle vain täysin terveenä!</p>
<p>2. Mikäli lapsesi on yli 12-vuotias, tulee nuorisotilalla käyttää maskia.</p>
<p>3. Pienryhmiin vaaditaan ilmoittautuminen. Perjantaina ja lauantaina järjestetään yläkoululaisten (ja kutosluokkalaisten) pienryhmiä erilaisin teemoin. Aiemmasta poiketen Puuhakerhoon ja Girls Can ryhmään ilmoittaudutaan myös ennakkoon. Ryhmiin otetaan max. 8 nuorta kerrallaan. Avointa toimintaa emme voi järjestää. Yllä mainitut kriteerit ovat voimassa ainakin helmikuun ajan lukuunottamatta hiihtolomaviikkoa. Osa pienryhmistä pääsee alkoi jo keskiviikkona 3.2., tarkemmat päivämäärät löydät ilmoittautumislomakkeesta. Aiemmat kevään 2021 kerhoilmoittautumiset on otettu huomioon.</p>
<p>Jos jokin mietityttää, laita sähköpostia: <a href="#OnlyHTTPAndHTTPSAllowed">kauklahti.nuoriso@espoo.fi</a></p>
<p>Voit ilmoittaa lapsesi tällä linkillä: <a href="https://eur03.safelinks.protection.outlook.com/?url=https%3A%2F%2Fbit.ly%2F2KCbxX9&amp;data=04%7C01%7C%7C7739b519f56a401570fb08d8c8d8a753%7C6bb04228cfa542139f39172454d82584%7C1%7C0%7C637480180137509813%7CUnknown%7CTWFpbGZsb3d8eyJWIjoiMC4wLjAwMDAiLCJQIjoiV2luMzIiLCJBTiI6Ik1haWwiLCJXVCI6Mn0%3D%7C1000&amp;sdata=kdswRtSW9UDzwxrSefKUn7DjEJ7GoS%2FzxhCqUn8jHSc%3D&amp;reserved=0">https://bit.ly/2KCbxX9</a> tai huoltaja voi myös ilmoittaa lapsensa ennakolta paikan päällä.</p>
<p>Nähdään pian!</p>
<p>Terveisin nuorisonohjaajat Sari, Miira ja Tiina.</p>
