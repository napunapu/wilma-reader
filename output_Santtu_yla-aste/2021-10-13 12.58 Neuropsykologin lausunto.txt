Neuropsykologin lausunto
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Lehmusvuori Teija (LEH)
2021-10-13 12.58

<p>Hei!</p>
<p>Lausunnon skannattu versio löytyy alla olevasta linkistä salasanalla<br>
j6J5$sEjH2De%Ng9Ay!!</p>
<p>https://kodum-my.sharepoint.com/:b:/g/personal/panu_korpela_kodum_fi/EboufUeXfMNOomFwcRQ-RRcB7FPHVig-7HvhRmAgRBthVg?e=QI9aZ9</p>
<p>Santtu sekä vanhemmat antavat luvan dokumentin ja sen tietojen välittämiseen tarpeen mukaan.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
