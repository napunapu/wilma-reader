Ensi viikko on hyvinvointiviikko - Tervetuloa maanantaina aamupalalle!
Särkkä Hanna (SÄR)
Piilotettu
2021-12-10 09.02

<p>Hei oppilaat ja huoltajat (tiedoksi koulun väki)!</p>
<p>Kauklahden koulussa vietetään ensi viikolla&nbsp;hyvinvointiviikkoa.</p>
<p>Viikon teemoihin kuuluu:&nbsp;</p>
<p>Koulupäivien aikana järjestämme teemoihin liittyvää ohjelmaa jokaisena päivänä.&nbsp;Saamme vierailijoita Setasta, Espoon sirkus- ja teatterikoulu Eskosta&nbsp;sekä nuorisotoimesta.&nbsp;<br>
Koululla tarjotaan <strong>aamupalaa maanantaina ja&nbsp;&nbsp;torstaina klo 7.50-8.30</strong>.&nbsp; Aamupalaa tarjoillaan kaikille halukkaille oppilaille&nbsp;ja&nbsp;aamupalalle osallistuminen on vapaaehtoista.<br>
Aamupalan meille tarjoavat ravintola Koivunoksa, K-supermarket Lasihytti, HOK- Elanto sekä Kauklahden kappeli. Lisäksi saamme kisailuihin palkintoja Kauppamäen karamellilta.</p>
<p>Viikon kaikessa toiminnassa huomioidaan koronaturvallisuus.</p>
<p>Terveisin, Hyvinvointiviikon tiimi</p>
