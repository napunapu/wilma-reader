Aamupala torstaina 8.00-8.30
Huhtala Tiina (T.H)
Piilotettu
2023-02-07 15.28

<p>Tervehdys!</p>
<p>Torstaina 9.2.2023 klo 8.00-8.30 on koulussamme tarjolla aamupalaa.</p>
<p>Tervetuloa aloittamaan neljättä jaksoa yhteisellä aamupalalla.</p>
<p>Hyvää helmikuun alkua jokaiselle!</p>
<p>&nbsp;</p>
<p>Terveisin,</p>
<p>TiiNa, yhteisöohjaaja</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
