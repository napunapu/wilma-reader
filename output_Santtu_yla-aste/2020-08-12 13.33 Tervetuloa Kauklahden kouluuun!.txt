Tervetuloa Kauklahden kouluuun!
Lehmusvuori Teija (LEH)
Piilotettu
2020-08-12 13.33

<p>Hyvä seiskaluokkalaisen huoltaja!<br>
Välitäthän seuraavat tiedot uudelle Kaukalahden koulun seiskaluokkalaiselle:<br>
<br>
Tervetuloa Kauklahden kouluuun!</p>
<p>Kokoonnumme torstaina 13.8.2020 klo 9.00 koulun sisäpihalle pääovien luokse. Koulupäivä päättyy noin 12.30. Mahdollinen eduespoo-tunnus kannattaa ottaa mukaan viimeistään perjantaina.</p>
<p>Perjantaina koulupäivä alkaa 8.20 (tule suoraan kotiluokkaan) ja päättyy noin 12.30. Molempina päivinä on ruokailu klo 11.00.</p>
<p>Noudatamme turvaväleistä ja hygieniasta annettuja ohjeita.</p>
<p>Ystävällisin terveisin,</p>
<p>7.-luokkien luokanvalvojat</p>
