Taksvärkki 27.5.
Maasara Elsa (PEK)
Piilotettu
2022-04-22 09.08

<p>Hyvät oppilaat, vanhemmat ja Kauklahden koulun henkilökunta,&nbsp;  &nbsp;&nbsp;</p>
<p>Päivätyökeräys eli taksvärkki järjestetään Kauklahden koulussa  perjantaina 27.5.2022. Keräyksellä saatavat varat lahjoitetaan Unicefin kautta Ukrainan sodan uhrien auttamiseen. Seuraavassa tärkeimmät käytännön tiedot päivätyökeräyksestä: &nbsp;</p>
<p>Päivätyö suoritetaan perjantaina 27.5, jolloin päivätyöhön osallistuva oppilas on vapautettu koulutyöstä. &nbsp;</p>
<p>Päivätyön voi suorittaa joko kotona tai ulkopuolisen palveluksessa. Oppilas hankkii itse työpaikkansa. &nbsp;</p>
<p>Keräyksen järjestää Kauklahden koulu. Keräykseen osallistuvat oppilaat ovat vakuutetut normaalilla koululaisvakuutuksella, eikä palkasta tarvitse maksaa ennakonpidätystä eikä sosiaaliturvamaksua. &nbsp;</p>
<p>Oppilaan on mahdollisuus tulla koululle syömään, vaikka hän osallistuisikin päivätyökeräykseen.</p>
<p>Päivätyökeräykseen osallistuvalle oppilaalle jaetaan työlomake, joka toimii palkkakuittina. Päivätyökeräyksestä saatu palkka, vähintään 10 €, maksetaan tilisiirtona Unicefin tilille (suositeltava maksutapa) tai käteisenä koululle. Tilille maksettaessa on käytettävä koulun omaa viitenumeroa.</p>
<p>Oppilas tuo maanantaina 30.5.2022 luokanvalvojalle täytetyn työlomakkeen sekä tositteen tilisiirrosta tai palkan käteisenä.  &nbsp;</p>
<p>Niille oppilaille, jotka eivät osallistu päivätyökeräykseen, järjestetään perjantaina 27.5. koulupäivä klo 9-12.</p>
<p><strong>Ilmoitathan 30.4. mennessä alla näkyvästä linkistä, osallistuuko oppilas päivätyökeräykseen ja ruokaileeko hän&nbsp;koululla.</strong></p>
<p><a href="https://forms.gle/QMHWK5RimTmwtQYA9">https://forms.gle/QMHWK5RimTmwtQYA9</a></p>
<p>Kauklahden koulun keräystoimikunta toivoo runsasta osanottoa ja kiittää jo etukäteen osallistujia heidän antamastaan arvokkaasta tuesta. </p>
