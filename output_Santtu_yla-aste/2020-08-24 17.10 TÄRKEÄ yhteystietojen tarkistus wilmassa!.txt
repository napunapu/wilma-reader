TÄRKEÄ yhteystietojen tarkistus wilmassa!
Lehmusvuori Teija (LEH)
Piilotettu
2020-08-24 17.10

<p>Hyvät oppilaitteni huoltajat,</p>
<p>laitan varmuuden vuoksi vielä erillisenä viestinä saman asian, joka on tiedotteenakin wilmassa.</p>
<p>Huoltaja, tarkista ja korjaa oppilaan ja huoltajien yhteystiedot sekä päivitä uudistuneet oppilaan julkaisuluvat Wilman lomakkeella perjantaihin 28.8.2020 mennessä. Huomioi, että Wilman lomakkeet eivät näy mobiililaitteisiin ladattavissa sovelluksissa eli lomakkeen täyttämiseen täytyy käyttää Wilman selainversiota.</p>
<p>Ystävällisin terveisin,<br>
Teija Lehmusvuori (7D LV)</p>
<p>------</p>
<p>Dear parents,</p>
<p>kindly please check that all the contact information we have in wilma is correct when it comes to the student and the guardians.<br>
You will have to use the net version to do this - it can not be done using the mobile application.</p>
<p>Best regards,<br>
Terija Lehmusvuori (7D form teacher)</p>
