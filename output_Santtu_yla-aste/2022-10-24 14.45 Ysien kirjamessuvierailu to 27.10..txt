Ysien kirjamessuvierailu to 27.10.
Salonen Niina (SAL)
Piilotettu
2022-10-24 14.45

<p>Hei ysiluokkalainen ja tiedoksi myös kotiväki,</p>
<p>tämän viikon torstaina 27.10. retkeilemme Helsingin kirjamessuille. Retkelle osallistuvat&nbsp;kaikki koulumme yhdeksäsluokkalaiset sekä retkeä valvovat opettajat. Torstain aikataulu on seuraava:</p>
<p>Aamun tunnit pidetään normaalisti lukujärjestyksen mukaan, mutta ysien ruokailu alkaa&nbsp;torstaina poikkeuksellisesti&nbsp;jo klo 10:30. Tämän jälkeen kokoonnumme&nbsp;klo 10:50 ala-aulassa, jossa ilmoittaudutaan omalle vastuuopettajalle. <strong>Tarkistathan oman luokkasi vastuuopettajan alta.</strong> Tästä siirrymme&nbsp;hyvässä järjestyksessä Kauklahden juna-asemalle,&nbsp;matkaamme&nbsp;klo 11:05 lähtevällä E-junalla&nbsp;Pasilaan ja jatkamme kävellen Messukeskukseen. Perillä kohteessa olemme&nbsp;noin klo 11:40. Messualueella kierrellään&nbsp;omatoimisesti ja täytetään sähköinen&nbsp;<em>Suunnista kirjaan</em> -lomake.</p>
<p>Messualueen ulkopuolella ilmoittaudutaan omalle vastuuopelle klo 13:20. Palaamme takaisin koululle Pasilasta klo 13:46 lähtevällä U-junalla. Koululle saavumme noin klo 14:15, ja koulupäivä päättyy tähän.&nbsp;</p>
<p>Mukavaa alkanutta&nbsp;viikkoa kaikille ja reipasta retkimieltä torstaihin!</p>
<p><strong>Luokkien vastuuopettajat kirjamessuilla:</strong></p>
<p>9A: Anna Päärni</p>
<p>9B: Asta Lerkki</p>
<p>9C: Niina Salonen</p>
<p>9D: Teija Lehmusvuori</p>
<p>9E: Essi Mala</p>
<p>9F: Miikka Autti</p>
<p>9G: Ariadna Pyulze</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
