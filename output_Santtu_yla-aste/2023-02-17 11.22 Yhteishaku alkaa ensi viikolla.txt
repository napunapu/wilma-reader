Yhteishaku alkaa ensi viikolla
Räsänen Mari (RÄS)
Piilotettu
2023-02-17 11.22

<p><span style="color:#008080;"><span style="font-size:14px;"><strong><em>Hei ysit ja huoltajat!</em></strong></span></span></p>
<p>&nbsp;</p>
<p><strong>YHTEISHAKU ALKAA ENSI VIIKON TIISTAINA 21.2. JA PÄÄTTYY 21.3.2023</strong></p>
<p>Tässä vielä&nbsp;<a href="https://www.youtube.com/watch?v=XgKOHCfVX-8">ohjevideo</a>&nbsp;yhteishaun tekemisestä. Pääpaino videossa on Omniaan ammatilliseen koulutukseen hakemisessa, mutta samalla lomakkeella haetaan toki kaikkiin toisen asteen koulutuksiin. Katsothan sen :-)&nbsp;</p>
<p>&nbsp;</p>
<p><span style="font-size:14px;">Yhteishakulomake sijaitsee Opintopolussa:&nbsp;<a href="https://opintopolku.fi/konfo/fi/">https://opintopolku.fi/konfo/fi/</a></span></p>
<p>&nbsp;</p>
<p><br>
Kivaa hiihtolomaa kaikille lomalaisille! 😊&nbsp; Lomailen itsekin ensi viikon.</p>
<p>&nbsp;</p>
<p>Terveisin, Mari-opo</p>
