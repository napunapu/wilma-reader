Kouluterveydenhuollon toiminta kevät 2022
Mikkonen Heta (H.M)
Piilotettu
2022-01-11 14.41

<p>KOULUTERVEYDENHUOLLON TOIMINTA &nbsp;<br>
KEVÄÄLLÄ 2022</p>
<p>Espoon kouluterveydenhuolto on osallistunut koronasta johtuviin kriittisiin terveydenhoitotehtäviin kuten Covid 19- rokotuksiin ja tartunnanjäljitykseen. Tämän vuoksi palveluissamme on nyt ruuhkaa ja paljon hoitovelkaa. Vastaanottoajat voivat valitettavasti mennä useamman viikon päähän.</p>
<p>Poikkeusolojen järjestelyt vaikuttavat edelleen kevään 2022 toimintaamme.</p>
<p>Kevätlukukaudella 2022 tarjoamme palveluita erityisesti oppilaille/perheille, joilla on huolta ja tuen tarvetta.</p>
<p>Huoltajilla on mahdollisuus osallistua lapsen/ nuoren terveydenhoitajan tapaamiseen myös etävastaanoton kautta.&nbsp;Etävastaanottoon ohjeet saatte omalta terveydenhoitajalta.</p>
<p>Huomioittehan, että terveystarkastuksiin ja muihin käynteihin ei kevään aikana lähtökohtaisesti tule erillistä kutsua, mutta jos teillä on huoli lapsenne/nuorenne kasvun, kehityksen ja hyvinvoinnin suhteen, olkaa yhteydessä omaan terveydenhoitajaan.</p>
<p>Lapset ja nuoret voivat tarvittaessa, minkä tahansa asian tiimoilta, tulla tapaamaan terveydenhoitajaa koulupäivän aikana avovastaanottoajalla, jonka ajankohdasta on tiedote oman koulun Wilmassa/nettisivulla.</p>
<p>&nbsp;</p>
<p><strong>Ajanvaraus oman koulun terveydenhoitajalle</strong></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Äkilliset ja pitkäaikaissairaudet sekä reseptien uusinnat ja matkailijoiden rokotukset hoidetaan terveysasemilla.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Uusi kouluterveydenhuollon keskitetty ohjaus ja neuvonta puhelinpalvelu (ei ajanvarausta) </strong></p>
<p>Tarjoamme ohjausta, neuvontaa ja tarvittaessa etävastaanottoa kouluterveydenhuollon asioissa nyt myös keskitetysti. Voitte olla yhteydessä ammattilaiseen (terveydenhoitaja) kouluterveydenhuollon keskitetyn puhelinpalvelun kautta.</p>
<p>Yhteydenotot maanantaisin ja keskiviikkoisin klo 9 - 13 p.&nbsp;<a href="#OnlyHTTPAndHTTPSAllowed">09 8166 1234</a>&nbsp;.<br>
Käytössämme on takaisinsoittojärjestelmä. Pyrimme soittamaan takaisin mahdollisimman pian.</p>
<p>&nbsp;</p>
<p>Terveisin,<br>
Espoon kouluterveydenhuolto</p>
<p>&nbsp;</p>
<p>Lisätietoa: <a href="https://www.espoo.fi/fi/terveys/koululaisten-ja-opiskelijoiden-terveydenhuolto/kouluterveydenhuolto-0">https://www.espoo.fi/fi/terveys/koululaisten-ja-opiskelijoiden-terveydenhuolto/kouluterveydenhuolto-0</a></p>
