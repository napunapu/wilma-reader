Yhteydenottopyyntö / neuropsykologi
Korpela Panu (Santtu Tarvainen, 9D)
Tarvainen Outi (Santtu Tarvainen, 9D), Fleming Johanna (FLE), Lehmusvuori Teija (LEH)
2022-01-27 16.31

<p>Hei!</p>
<p>Santun neuropsykologi Riia Lindblom-Ikonen pyysi teitä olemaan yhteydessä häneen palaverin sopimiseksi. Yhteystiedot ovat:</p>
<p>Ystävällisin terveisin,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
