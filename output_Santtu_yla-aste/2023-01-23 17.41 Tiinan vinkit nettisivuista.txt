Tiinan vinkit nettisivuista
Lehmusvuori Teija (LEH)
Piilotettu
2023-01-23 17.41

<p>Hei oppilaani ja huoltajat!</p>
<p>Olen yhteisöohjaajamme Tiinan kanssa keskustellut luokassa nyt kahden pidennetyn&nbsp;LV-vartin aikana oppilaiden kanssa jaksamisesta, stressistä ja asioista, joilla voi stressiä ja jännitystä lieventää. Olemme toki päässeet vasta "raapaisemaan pintaa" mutta viime kerralla Tiinalla oli vinkata muutamia nettiosoitteita, joista voisi olla apua, jos olo tuntuu ylivoimaiselta. Muistutan myös, että aina kannattaa jutella ao. opettajille, eri oppiaineisiin liittyvissä kysymyksissä ja opinto-ohjaajien&nbsp;kanssa yhteishakuasioissa.</p>
<p>NuortenNetti https://www.nuortennetti.fi/</p>
<p>Nuorten Mielenterveystalo https://www.mielenterveystalo.fi/nuoret/Pages/default.aspx</p>
<p>Sekaisin-chat https://www.sekasin.fi/</p>
<p>CHILLAA-sovellus puhelimesi sovelluskaupasta</p>
<p>Ystävällisin terveisin,</p>
<p>LV Teija</p>
<p>&nbsp;</p>
