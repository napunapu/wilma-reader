Keskiviikkona 9.2. ja maanantaina 14.2. oppilaitoksen tiloissa olleilla henkilöillä on todettu koronavirustartunta
Korhonen Joonas (KOR)
Piilotettu
2022-02-15 12.14

<p>Hei!</p>
<p>Keskiviikkona 9.2. ja maanantaina 14.2. oppilaitoksen tiloissa olleilla henkilöillä on todettu koronavirustartunta. Kaikki lapset ja henkilökunta eivät ole altistuneet.</p>
<p>Lapsesi voi jatkaa normaalia arkea, jos on oireeton.</p>
<p>Mikäli ilmenee koronavirustautiin sopivia oireita (kuume, yskä, hengenahdistus, lihaskivut, kurkkukipu, väsymys, haju-/makuaistin menetys, ripuli), suositellaan tekemään kotitesti sairaanhoitopiirin ohjeiden mukaan tai tarvittaessa hakeutumaan terveydenhuollon koronavirustestiin. Terveydenhuollon näytteenoton voi varata osoitteessa koronabotti.hus.fi. Voit myös soittaa kotikuntasi koronaneuvontaan (Espoon koronaneuvonta p. 09 816 34600 ma-pe klo 7–18 ja la-su 9–15).</p>
<p>yst.</p>
<p>Joonas Korhonen</p>
