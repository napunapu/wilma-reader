Lounaan jakelu etäoppilaille/Lunch package distribution
Korhonen Joonas (KOR)
Piilotettu
2021-03-02 13.25

<p>Hyvät huoltajat,</p>
<p>luokkia 7G, 8H/9G ja 9F sekä erityisen tuen oppilaita lukuun&nbsp;ottamatta koulumme oppilaat ovat etäopetuksessa&nbsp;8.3.2021-28.3.2021. Etäopetuksessa oleville oppilaille tarjoamme lounaspaketin, joka sisältää lounaat koulupäiville. Lounaspaketti jaetaan kerran viikossa.</p>
<p>Tämän viikon perjantaina (5.3.) jaamme ensimmäisen lounaspaketin kaikille etäopetukseen siirtyville oppilaille. Jos haluat lapsellesi lounaspaketin myös seuraaviksi etäopetusviikoiksi, lue ohje alta ja ilmoittaudu&nbsp;<strong>5.3. kello 8 mennessä.</strong></p>
<p><u>Lounaspakettien jakeluaika ja -paikka:</u></p>
<p>Ensimmäinen jako on 5.3.2021 iltapäivällä tuntien päätyttyä klo 13 – 15. Jokaiselle oppilaalle on varattu lounaspaketti.&nbsp;<strong>Tähän ensimmäiseen hakuun ei tarvitse ilmoittautua erikseen!</strong></p>
<p>Seuraavat jakelut tapahtuvat aina perjantaisin klo 14-16. Lounaspaketit jaetaan koulun ruokalassa. Näihin hakuihin tulee ilmoittautua 5.3. kello 8.00 mennessä.</p>
<p>Lounaspaketin voi hakea oppilas tai perheenjäsen. Tule paikalle vain terveenä ja muista turvavälit. Suosittelemme maskin käyttöä.</p>
<p><u>Lounaspakettien sisältö ja erityisruokavaliot:</u></p>
<p>a. Lounaspakettijakelu sekaruokalounas (laktoositon, saattaa sisältää sianlihaa)</p>
<p>b. Lounaspakettijakelu kasvisruokavalio, sopii sianlihatonta ruokavaliota noudattaville (laktoositon, lihaton)</p>
<p>c. Lounaspakettijakelu gluteeniton sekaruokalounas (laktoositon)</p>
<p>d. Lapsellani on jokin muu erityisruokavalio ja hän tulee koulupäivinä syömään koululounaan.</p>
<p>e. En tarvitse lounaspakettia kyseisenä aikana</p>
<p>Ilmoittaudu alla olevan linkin kautta 5.3. klo 8 mennessä. Kun ilmoittaudut jakeluun, lapsesi saa lounaspaketit ajalle 15.-26.3. Ilmoittautuminen on sitova.</p>
<p>Huom! mikäli ilmoittautumistietoihisi tulee muutoksia, ilmoita niistä koulusihteerille osoitteeseen tuire.s.isotalo@espoo.fi</p>
<p>Vastaa kyselyyn tämän linkin kautta:&nbsp;<a href="https://docs.google.com/forms/d/e/1FAIpQLSf5JfbS5K23yRrs6h4GgbYE-6nlNOpKLfXv3FLHiA6oAoB9jA/viewform?usp=sf_link">https://docs.google.com/forms/d/e/1FAIpQLSf5JfbS5K23yRrs6h4GgbYE-6nlNOpKLfXv3FLHiA6oAoB9jA/viewform?usp=sf_link</a></p>
<p>Dear guardians,</p>
<p>Grades 7-10 (excluding 7G,8H/9G, 9F&nbsp;and pupils in special support) will participate in distance learning 8.-28.3. During this period the pupils in distance learning are offered lunch packages as a replacement for school lunch. The lunch packages will be handed out about once a week on Friday. If your child follow a special diet which cannot be taken into account in the lunch package distribution, you can sign up for lunch at school.</p>
<p>Lunch package distribution timetables:</p>
<p>1. First distribution Friday afternoon, after the end of lessons at 13-15 o’clock</p>
<p>2. Second distribution 12.3. afternoon at 14-16 o’clock</p>
<p>The pupil/student or their family member (guardian or sibling) can come to pick up a lunch package. Only come to pick up the lunch package if you are healthy and keep a safe distance. We recommend wearing a mask.</p>
<p>Lunch options:</p>
<p>a. Lunch package distribution, mixed diet (lactose free, may contain pork)</p>
<p>b. Lunch package distribution, vegetarian (lactose free, does not contain meat)</p>
<p>c. Lunch package distribution, mixed diet, gluten free (lactose free)</p>
<p>d. I follow another special diet: I will eat lunch at school daily at the time specified by the school.</p>
<p>e. I do not need lunch packages during this period.</p>
<p>Sign up 5.3. at 8.00 at latest</p>
<p>We need a binding sign up for lunch package distribution covering all three weeks. Please sign up by responding to the lunch survey on 5.3. at 8.00 o’clock at latest. By responding the survey your child will get lunch packages between 15 and 26 March 2021.</p>
<p>Link to survey:&nbsp;&nbsp;<a href="https://docs.google.com/forms/d/e/1FAIpQLSf5JfbS5K23yRrs6h4GgbYE-6nlNOpKLfXv3FLHiA6oAoB9jA/viewform?usp=sf_link">https://docs.google.com/forms/d/e/1FAIpQLSf5JfbS5K23yRrs6h4GgbYE-6nlNOpKLfXv3FLHiA6oAoB9jA/viewform?usp=sf_link</a></p>
<p>&nbsp;</p>
