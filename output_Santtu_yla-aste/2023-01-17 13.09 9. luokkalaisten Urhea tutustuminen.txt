9. luokkalaisten Urhea tutustuminen
Paloniemi Jasmin (PAL)
Piilotettu
2023-01-17 13.09

<p>Moi!</p>
<p>Maanantaina <strong>30.1.2023 klo 8.15-14.00</strong> järjestetään Urhea-hallilla&nbsp;päiväleiri&nbsp;9. -luokkalaisille, jotka suunnittelevat hakevansa opiskelemaan toiselle asteelle urheiluoppilaitoksiin. Paikkana toimii Mäkelänrinteen uusi Urhea-halli. Näemme päivän aluksi <strong>Pasilan asemalla n. klo 8</strong>, mistä siirrymme yhdessä Urhea-hallille. Voit myös tulla halutessasi omalla kyydillä suoraan hallille (tästä täytyy ilmoittaa erikseen). Päivä päättyy n. klo 14.15 Pasilan asemalle, josta siirtyminen omatoimisesti kotiin. Koululta saa tietysti matkaliput käyttöön. Huomioithan,&nbsp;että sinun tulee olla Pasilassa jo klo 8.</p>
<p>Leirillä nuoret pääsevät akatemian valmentajien pitämiin aamutreeneihin, sekä saavat tietoa toisen asteen urheiluoppilaaksi hakemisesta ja opiskelusta.&nbsp;Päivä sisältää teoriaosuuden ja 2 liikunnallista osuutta.</p>
<p><u><strong>Mikäli olet kiinnostunut lähtemään mukaan, ilmoittaudu vastaamalla&nbsp;tähän viestiin.</strong></u></p>
<p><em>Päivän aikataulu:</em></p>
<p><em>Mukaan:</em></p>
<p>Yst. Jasmin</p>
