TÄRKEÄ: TAKSVÄRKIN TYÖLOMAKE
Lehmusvuori Teija (LEH)
Piilotettu
2022-05-19 11.43

<p>Hei!</p>
<p>Olet ilmoittautunut taksvärkkipäivään.<br>
Tässä sinulle linkki työlomakkeeseen:<span style="font-size:16px;"><strong><a href="https://unicef.studio.crasman.fi/pub/kavely/pdf+%232/kauklahden-koulu-paivatyokerays-tyolomake.pdf">Työlomake</a></strong></span></p>
<p>Lomakkeessa on mahdollista valita isompikin summa lahjoitettavaksi, mutta ohjeellinen minimisumma lahjoitukseen on 10 euroa, jonka voi rastittaa vaihtoehtoon "muu summa".</p>
<p>Alunperin oli tarkoitus ohjata lahjoitukset Ukrainaan, mutta Unicefin tämän kevään ainoa päivätyökeräyskohde on Myanmar. Hyvään tarkoitukseen rahat siis joka tapauksessa menevät.</p>
<p>T: Taksvärkkitoimikunta</p>
