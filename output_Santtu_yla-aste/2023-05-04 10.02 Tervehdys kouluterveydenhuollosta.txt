Tervehdys kouluterveydenhuollosta
Mikkonen Heta (H.M)
Piilotettu
2023-05-04 10.02

<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Hei ysiluokkalainen ja vanhemmille tiedoksi,</p>
<p>Huomioithan, että mikäli et ole käynyt 8.luokan terveystarkastuksessa, sinulta puuttuu luultavasti rokotusohjelman mukainen jäykkäkouristus-hinkuyskä-kurkkumätä-vahvisterokote. Voit varata ajan terveystarkastukseen joko sähköisesti, avovastaanotolla tai puhelimitse. Lyhyisiin kontrolleihin tai esimerkiksi rokotuksiin voit tulla avovastaanottoaikoina ma-pe klo 11.20-11.45. Viikolla 19 ei ole avovastaanottoa.</p>
<p>&nbsp;</p>
<p>Aikoja varattavissa myös kesäkuuksi .&nbsp;Lisätietoa ajanvarauksesta ja palveluista <a href="https://www.luvn.fi/fi/palvelut/perhekeskus/opiskeluhuolto/kouluterveydenhuolto">Kouluterveydenhuolto | Länsi-Uudenmaan hyvinvointialue (luvn.fi)</a></p>
<p>ystävällisin terveisin terveydenhoitaja Heta Mikkonen</p>
