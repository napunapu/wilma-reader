Santun kaverisuhteista
Lehmusvuori Teija (LEH)
Tarvainen Outi (Santtu Tarvainen, 9D), Korpela Panu (Santtu Tarvainen, 9D), Fleming Johanna (FLE)
2022-04-26 17.00

<p>Hei Santun vanhemmat ja Johanna!</p>
<p>Sivusin juuri Johannan kanssa jutellessa tätä asiaa tänään ja kun Santun luokkakaveri Essi tuli juttelemaan<br>
tänään kanssani kahden kesken, ajattelin, että laitan viestiä teille kaikille aiheesta.</p>
<p>Santun sosiaalinen opiskeluympäristö on varmasti hänelle raskas.<br>
Välitunnilla tänään Johanna kävi purkamassa isompaa porukkaa,<br>
joka tuntui ottaneen Santun "hampaisiinsa". Olemme jutelleet näitä tilanteita<br>
havaitessamme kyseisten nuorten kanssa mutta teinien empatiakyky tuntuu olevan<br>
olematon eikä huomautuksista tai keskusteluista näytä olevan juurikaan apua.</p>
<p>Tunneilla minusta hän saa olla rauhassa huomatuksilta yms. varmasti sen takia,<br>
että opettaja on siinä läsnä. Hän tekisi mielellään muiden kanssa yhteistyötä, mutta<br>
paria tai ryhmää on usein hankalampi löytää. Itse teen usein englannissa Santun kanssa töitä,<br>
jotta voin myös tukea muistin kanssa ilman, että se tapahtuu "aina muiden kuullen".</p>
<p>Santtu oli pitkään hyvä työpari luokkatoverinsa Essin kanssa mutta sekin tilanne on nyt muuttunut.<br>
Essillä on omat isot haasteensa ja kun hän on yrittänyt nyt ottaa etäisyyttä, Santtu on sinnikkäästi etsinyt häntä käsiinsä<br>
somessa ja viestittelee ja vaikka Essi on pyytänyt selvästi, että saisiko olla rauhassa, Santtu ei tunnu hyväksyvän tätä.<br>
Essi ei pidä siitä, että häntä kosketellaan ja vaikka mitään asiatonta koskettelua ei ole esiintynyt, tämäkin ahdistaa<br>
Essiä. Hän on nyt estänyt Santun yhteydenottoja somessa ja Santtu kokee tietysti tilanteen ikävänä ja kokee että häntä kiusataan.<br>
Sain juuri Santulta tästä wilma-viestin.</p>
<p>Tilanne on todella hankala ja tuntuu ikävältä laittaa tätä viestiä, mutta on tärkeää, että tekin huoltajina tiedätte,<br>
miten täällä sujuu. Santtu on varmasti pahoilla mielin tilanteesta ja olisi hyvä, jos hän edes vapaa-ajallaan löytäisi<br>
ystäviä ja saisi tukea ja keskustelutukea. Luonnollisesti seuraamme tilannetta aina, kun mahdollista täällä koulussa, ja isompi<br>
tuki esim. avustajaresurssina olisi enemmän kuin tarpeen.<br>
<br>
Yst. terv. Teija LEH</p>
