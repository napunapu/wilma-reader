Santun tukipaperin julkaiseminen teille
Lehmusvuori Teija (LEH)
Tarvainen Outi (Santtu Tarvainen, 9D), Korpela Panu (Santtu Tarvainen, 9D)
2022-03-05 13.04

<p>Hei!</p>
<p>Aika kauan minulta meni, ennen kuin sain palaverimme jälkeen viimeisteltyä uuden oppimissuunnitelman<br>
wilmassa. Nyt se kuitenkin on tehty ja julkaistu teille huoltajille.<br>
Se löytyy edelleen kohdasta TUKI. Huhtikuun alkupuolella sitten tarkistetaan tilanne yhteisessä palaverissa,<br>
johon kutsutaan myös oppilaanohjaaja, kuten sovittua.</p>
<p>Ystävällisin terveisin,<br>
Teija LEH</p>
