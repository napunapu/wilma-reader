Santun kuulemisajankohdan siirtäminen lakon takia
Fleming Johanna (FLE)
Tarvainen Outi (Santtu Tarvainen, 9D), Korpela Panu (Santtu Tarvainen, 9D), Lehmusvuori Teija (LEH)
2022-05-10 10.03

<p>Hei,</p>
<p>&nbsp;</p>
<p>Joudumme valitettavasti siirtämään Santun kuulemisajankohtaa eteenpäin lakon takia. Esittäisin uudeksi ajaksi vkoa 21. ma-ke. Mitkä ajat kävisivät teille. Voimme tehdä kuulemisen sähköisesti.</p>
<p>Yst</p>
<p>Johanna</p>
