package fi.panukorpela.wilmareader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Properties;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WilmaReader {
    private static final String OUTPUT_DIRECTORY = "output";
    private static Log log = LogFactory.getLog(WilmaReader.class);
    private static DateTimeFormatter inFormatter = DateTimeFormatter.ofPattern("d.M.yyyy H:mm");
    private static DateTimeFormatter outFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm");

    public static void main(String[] args) throws Exception {
        // Credentials
        Properties prop = new Properties();
        File propFile = new File("config.properties");
        if (propFile.exists()) {
            try (InputStream propInputStream = new FileInputStream(propFile)) {
                prop.load(propInputStream);
            }
        } else {
            throw new FileNotFoundException("property file 'config.properties' not found");
        }
        // Output directory
        File directory = new File(OUTPUT_DIRECTORY);
        if (!directory.exists()) {
            directory.mkdir();
        }
        // Web UI handling
        WebDriver webDriver = new ChromeDriver();
        // Login
        webDriver.navigate().to("https://" + prop.getProperty("server") + "/login");
        WebElement element = webDriver.findElement(By.cssSelector("#login-frontdoor"));
        element.sendKeys(prop.getProperty("username"));
        element = webDriver.findElement(By.cssSelector("#password"));
        element.sendKeys(prop.getProperty("password"));
<<<<<<< HEAD
        element = webDriver.findElement(By.cssSelector(
                "body > main#main-content > div.login-box > div.login-content > form > div:nth-child(3) > div > input"));
=======
        element = webDriver.findElement(
                By.cssSelector("#main-content > div > div.login-content > form > div:nth-child(3) > div > input"));
        element.click();
        // Select child
        element = webDriver.findElement(
                By.cssSelector("div.resp-column:nth-child(3) > div:nth-child(1) > div:nth-child(1) > h1:nth-child(1) > a:nth-child(1)"));
>>>>>>> d1da086 (Updated for newest Wilma)
        element.click();
        // Go to message list via "Viestit" link
        element = webDriver.findElement(
                By.cssSelector("div.resp-column:nth-child(3) > div:nth-child(1) > div:nth-child(1) > h1:nth-child(1) > a:nth-child(1)"));
        element.click();
        element = (new WebDriverWait(webDriver, 10))
                .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#message-list-table > tbody > tr")));
        // Approve cookies
//        element = webDriver.findElement(By.cssSelector("#approve-cookies"));
//        element.click();
        // Handle pages
        boolean morePages = false;
        do {
            int messageIndex = 0;
            List<WebElement> messages = webDriver.findElements(By.cssSelector("#message-list-table > tbody > tr"));
            log.info("size " + messages.size());
            do {
                // Message titles
                (new WebDriverWait(webDriver, 10)).until(ExpectedConditions
                        .presenceOfElementLocated(By.cssSelector("#message-list-table > tbody > tr")));
                messages = webDriver.findElements(By.cssSelector("#message-list-table > tbody > tr"));
                WebElement messageElement = messages.get(messageIndex);
                messageIndex++;
                log.info("index " + messageIndex);
                element = messageElement.findElement(By.cssSelector("td:nth-child(3) > a"));
                String scrollElementIntoMiddle = "var viewPortHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);"
                        + "var elementTop = arguments[0].getBoundingClientRect().top;"
                        + "window.scrollBy(0, elementTop-(viewPortHeight/2));";
                ((JavascriptExecutor) webDriver).executeScript(scrollElementIntoMiddle, element);
                log.info(element.getText());
                element.click();
                (new WebDriverWait(webDriver, 10))
                        .until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.ckeditor p")));
                String title = webDriver.findElement(By.cssSelector("#page-content-area > h1")).getText();
                String sender = webDriver.findElement(By.xpath("//th[contains(text(),'Lähettäjä: ')]"))
                        .findElement(By.xpath("../td[1]")).getText();
                String recipients = webDriver.findElement(By.cssSelector("#recipients-cell")).getText();
                String sent = webDriver
                        .findElement(By.cssSelector(
                                ".table > tbody:nth-child(2) > tr:nth-child(3) > td:nth-child(2)"))
                        .getText().replaceAll("klo ", "");
                sent = LocalDateTime.parse(sent, inFormatter).format(outFormatter);
                List<WebElement> contentParagraphs = webDriver.findElements(By.cssSelector("div.ckeditor p"));
                log.info(title);
                File outputFile = new File(OUTPUT_DIRECTORY + "/" + sent + " "
                        + title.replace(":", "_").replace("/", "_").replace("?", "_").replace("\"", "'") + ".txt");
                // If two messages come within the same minute, add "b" to the
                // end of the 2nd one.
                // FIXME: Add handling that can take an unlimited amount of
                // messages within the same minute.
                if (outputFile.exists()) {
                    outputFile = new File(OUTPUT_DIRECTORY + "/" + sent + " "
                            + title.replace(":", "_").replace("/", "_").replace("?", "_").replace("\"", "'") + "b.txt");
                }
                FileWriter out = new FileWriter(outputFile);
                out.write(title + "\n");
                out.write(sender + "\n");
                out.write(recipients + "\n");
                out.write(sent + "\n");
                out.write("\n");
                for (WebElement contentParagraph : contentParagraphs) {
                    out.write("<p>" + contentParagraph.getAttribute("innerHTML") + "</p>\n");
                }
                out.flush();
                out.close();
                webDriver.navigate().back();
            } while (messageIndex < messages.size());
            (new WebDriverWait(webDriver, 10)).until(
                    ExpectedConditions.presenceOfElementLocated(By.cssSelector("#message-list-table > tbody > tr")));
            WebElement nextPage = webDriver.findElement(By.xpath("//a[@aria-label='Seuraava sivu']"));
            if (nextPage.getAttribute("class").indexOf("disabled") == -1) {
                morePages = true;
                nextPage = nextPage.findElement(By.xpath("./.."));
                nextPage.click();
            } else {
                morePages = false;
            }
        } while (morePages);
        webDriver.quit();
        System.exit(0);   // Release all Selenium threads 
    }
}
