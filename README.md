# README #

## Quick summary

This program fetches and stores messages from the educational messaging software Wilma. This version has been tested with Wilma used at an elementary school in Espoo.

## How do I set it up? (untested)

### Summary of set up

In these instructions a package manager (macOS: Homebrew, Windows: Chocolatey) is used. Setup links and notes for these are below.

### Installations

#### For all platforms

Chrome browser: https://www.google.com/chrome/

#### macOS

Homebrew: https://brew.sh/

When Homebrew has been installed, use it to install the other required parts:

* Java: `brew install openjdk`
* Maven, a build tool for Java: `brew install maven`
* Git, a version control tool: `brew install git`
* Chrome driver, a tool for running Chrome from tests: `brew tap homebrew/cask && brew cask install chromedriver`

#### Windows

Chocolatey: https://chocolatey.org/install

When Chocolatey has been installed, use it to install the other required parts in the list below. Note that for Chocolatey
to work you have to run it in the administrative mode.

* Java: `choco install -y openjdk`
* Maven, a build tool for Java: `choco install -y maven`
* Git, a version control tool: `choco install -y git`
* Chrome driver, a tool for running Chrome from tests: `choco install -y chromedriver`

### Getting the source code

If you have not copied the files yet, run `git clone git@bitbucket.org:napunapu/wilma-reader.git`

### Configuration

Copy the sample file `config.properties.sample` to a name `config.properties` and edit it with your personal information.

## Running

From the command line: `mvn compile exec:java`

Running may take a long time, in the neighbourhood of 10 to 15 minutes for every 500 messages. First run will be even longer as Java dependencies are being downloaded by Maven.

It will create a folder called `output` and the resulting files are stored there.

## Development setup

Notes for Eclipse:

* macOS: `brew install eclipse-jee`
* Windows: `chocolatey install -y eclipse`

In the command line: `mvn eclipse:eclipse` (not actually needed for modern Eclipses, but will work with older ones also)

Start Eclipse and select **File/Import**, **General/Existing Projects into Workspace**

## Miscellaneous

### Background

This was created as the way to store Wilma messages with as little programming effort as possible. Selenium was chosen as the technique without even considering anything else.

### Contribution guidelines ###

* Only spaces for indentations :)
* TBD

### Who do I talk to? ###

Repository owner Panu Korpela, panu.korpela@iki.fi

## Licence

Copyright (c) 2020-2022 Panu Korpela

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
