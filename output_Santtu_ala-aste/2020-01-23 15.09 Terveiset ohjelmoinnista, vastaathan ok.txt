Terveiset ohjelmoinnista, vastaathan ok
Särkkä Hanna (HannaS)
Piilotettu
2020-01-23 15.09

<p>Hei,</p>
<p>Olemme&nbsp; aloittaneet kevään ohjelmoinnin valinnaisen&nbsp;mukavissa merkeissä. Päivä (tiistai)&nbsp;on pitkä, joten pienet eväät voi syödä luokassani ennen hommiin ryhtymistä.<br>
Aloittelemme suoraan edellisen tunnin päätteeksi, joten päivä päättyy klo 15.00.</p>
<p>Ohjelmoinnin valinnaisen sisältö löytyy <a href="https://docs.google.com/document/d/1c4Ag4-SjvQPBL7Ys8n2QTgtnWP_hUjgs1a736VS6na4/edit?usp=sharing">kokonaisuudessaan täältä </a>ja alla kootusti:</p>
<p>*Ohjelmoinnin peruskäsitteet&nbsp;</p>
<p>*Soveltavat harjoitukset eri ohjelmilla kuten</p>
<p>*Valinnaisen päätteeksi opinnäytetyö. Opinnäytteen ja tuntityöskentelyn pohjalta tulee&nbsp;sanallinen arviointi todistukseen.&nbsp;</p>
<p>Huom! Jotta voisimme säästää harjoituksemme&nbsp;<a href="https://scratch.mit.edu/">Scratch- ohjelmaan</a>, meidän täytyy luoda Schratch tunnukset. Alusta on luotu lasten ohjelmointitaitojen harjoittelua varten. Jotta voisimme luoda tunnukset,&nbsp;tarvitsisimme teidän huoltajien sähköposti- osoitteet "valvoja- " kohtaan. Tarkoitus ei ole jakaa töitä muille kuin kanssaopiskelijoille tutustuttavaksi. Voisitteko vastausviestillä lähettää ystävällisesti&nbsp;<strong>sähköpostinne ja samalla kuittauksen, että tunnusten luominen on ok</strong>. Myös tieto siitä, jos tunnus on jo olemassa, olisi hyvä saada. Jos tunnuksen luominen ei sovi, voi oppilas opiskella ilman tallentamista (ja tehdä opinnäytteen muutoin).</p>
<p>Terveisin, Hanna Särkkä</p>
<p>ohjelmoinnin(kin) ope</p>
