Book Beat- koodi
Lukkarinen Elina (ElinaL)
Piilotettu
2020-04-17 16.41

<p>Hei!&nbsp;</p>
<p>Tässä henkilökohtainen Book Beat -koodisi:&nbsp;XJW8N-Q6VFP-FSGCW-CLSWJ</p>
<p>&nbsp;</p>
<p>Näin pääset alkuun!&nbsp;</p>
<p>1. Lunasta lahjakorttikoodisi:</p>
<p>Olet saanut koulultasi lahjakorttikoodin, joka on 20 merkkiä pitkä uniikki koodi. Koodin voit lunastaa osoitteessa bookbeat.fi/koulu&nbsp;</p>
<p>2. Luo käyttäjätili:</p>
<p>Rekisteröidy käyttäjäksi sähköpostilla ja salasanalla. <u>Sinun ei tarvitse lisätä maksukortin tietoja. </u>Kun BookBeatin lahjakortti on käytetty loppuun, tilaus loppuu automaattisesti. Halutessaan huoltaja voi pitää käyttäjätilinsä tai voimme pyynnöstä poistaa sen.&nbsp;</p>
<p>3. Pelkästään lastenkirjoja vai kaikki kirjat? Jos lapsesi on 12-vuotias tai nuorempi, voit valita profiiliksi lasten profiilin, jolloin sovelluksessa näkyy pelkästään lastenkirjoja. Kirjaudu käyttäjätilillesi osoitteessa bookbeat.fi/kirjaudu ja valitse "Profiilit" ja vaihda tili lasten profiiliksi.&nbsp;</p>
<p>4. Lataa sovellus älypuhelimeesi tai tabletillesi Hae AppStoresta tai Google Play Kaupasta "BookBeat". Kirjaudu sovellukseen sähköpostillasi ja salasanallasi. Voit aloittaa lukemisen ja kuuntelemisen heti!&nbsp;</p>
<p>&nbsp;</p>
