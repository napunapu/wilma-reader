Lukujärjestysmuutos! TÄRKEÄÄ!
Lukkarinen Elina (ElinaL)
Piilotettu
2017-08-22 14.24

<p>Hei kotiväki!&nbsp;</p>
<p>Nelosluokkalaiset ovat valmistelleet koko koulun mahtavan yhteisen päivänavauksen, joka on torstaina 24.8. klo 9.20! <img height="23" src="/shared/scripts/ckeditor/plugins/smiley/images/thumbs_up.png" width="23"></p>
<p>Tämän vuoksi <u><strong>koulu alkaa torstaina poikkeuksellisesti kello 9.00!</strong></u>&nbsp;Koulupäivä päättyy lukujärjestyksen mukaan kello 15.00! Ylimääräinen tunti tasataan syksyn kuluessa.&nbsp;</p>
<p>&nbsp;</p>
<p>Olen reissussa ja&nbsp;virkavapaalla to 24.8.-pe 25.8. ja sijaisenani on Henna Kokkonen. Tärkeät viestit sijaiselle voitte lähettää oppilaan mukana paperilla tai välittää koulusihteerin kautta (puh.numero&nbsp;(09) 816 32095).&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin,</p>
<p>Elina&nbsp;</p>
