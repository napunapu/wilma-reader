Hansakallion koulussa turha palohälytys
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2017-12-20 11.38

<p>Hei</p>
<p>Hansakallion koulussa oli keskiviikona 20.12 klo 11.10-11.25 palohälytys, joka osoittautui vääräksi. Palolaitos kävi tarkastamassa tilat/talon. Oppilaat kokoontuivat opettajien johdolla ryhmänä sovitusti kokoontumispaikalle. Osa oppilaista oli liikkeellä sukkasilleen. Jalkansa kastaneet oppilaat voivat viettää loppupäivän välituntinsa koulurakennuksessa. Käymme lisäksi " yllättävän" tilanteen ja asian luokissa läpi.</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
<p>Hansakallion koulu</p>
<p>&nbsp;</p>
<p>Jarmo Jääskeläinen</p>
<p>&nbsp;</p>
