Hansakallion koulun Kirjanvaihtopäivä
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2019-09-25 09.17

<p>&nbsp;</p>
<p>Arvoisat huoltajat,</p>
<p>Koulussamme järjestetään jo perinteikkäät kirjanvaihtopäivät 8.10.- 10.10. välisenä aikana. Vaihtopäivän ideana on kestävä kehitys ja kierrätys: saamme mukavat kirjan uusien lapsien luettaviksi. Lukuharrastuksen ja -innostuksen virittäminen on myös osa tapahtuman tavoitetta.</p>
<p>Kirjanvaihtopäivään voi tuoda kirja, joka on</p>
<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; hyväkuntoinen (ei piirustuksia, repeytymiä, sotkuja)</p>
<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; alakoulun ikäiselle sopivaa luettavaa (ei aikuisten kirjoja eikä pienten lasten katselukirjoja)</p>
<p>Tapahtumaan voi tuoda kirjoja haluamansa määrän, mitään ala- tai ylärajaa ei ole. Voitte kysellä kirjoja myös tuttaviltanne ja sukulaisiltanne: useat kotitaloudet luopuvat mielellään kirjoista, joita heillä ei enää lueta.</p>
<p>Kun oppilas tuo kirjan kouluun, hän saa opettajalta koulumme omaa valuttaa, Hansareita. Hansareita saa seuraavasti:</p>
<p>- lehdestä (sarjakuvat yms.) 1 Hansarin</p>
<p>- pokkarista eli pehmeäkantisesta kirjasta (taskukirjat yms.) 2 Hansaria</p>
<p>- kovakantisesta kirjasta 5 Hansaria.</p>
<p>Kirjanvaihtopäivänä nämä Hansari-setelit ovat rahaa, joilla voi ostaa tapahtumaan tuotuja kirjoja. Luokan opettaja käy ostoksilla oman luokkansa niiden oppilaiden kanssa, jotka ovat osallistuneet vaihtopäivään. Hinnat ovat samat: lehti = 1 Hansari, pokkari = 2 Hansaria ja kovakantinen kirja = 5 Hansaria.</p>
<p><u>Tapahtumaan osallistuminen on vapaaehtoista.</u> Jos teillä ei ole tänä vuonna sellaisia kirjoja, joista raaskisitte luopua, voitte harkita osallistumista seuraavalla kerralla (tapahtuma pidetään joka toinen vuosi).</p>
<p>Tapahtumasta mahdollisesti ylijääneet kirjat jaamme luokkiin opettajien käyttöön luokkakirjastoihin.</p>
<p>&nbsp;</p>
<p>Kirjoja voi tuoda oman luokan opettajalle to 26.9. - &nbsp;pe 4.10. välisenä aikana</p>
<p>&nbsp;</p>
<p>Löytötavarapisteemme A-käytävän 0-kerroksessa (esiopetuskäytävän pääty) on täynnä&nbsp;loppukevään ja &nbsp;syyslukukaudella kouluun jääneitä vaatteita yms. Toivomme, että haette&nbsp;omanne pois. Lyötötavarapiste tyhjennetään heti lokakuun alussa ensiviikolla.</p>
<p>&nbsp;</p>
<p>Kierrätysterveisin,</p>
<p>Hansakallion koulun kestävän kehityksen tiimi</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
