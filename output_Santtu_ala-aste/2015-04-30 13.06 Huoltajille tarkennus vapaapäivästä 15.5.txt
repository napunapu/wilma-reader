Huoltajille tarkennus vapaapäivästä 15.5
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2015-04-30 13.06

<p>Arvoisat huoltajat,
</p>
<p>Tarkennus Hansakallion koulun tiedotteeseen muistettavat päivämäärät: vapaapäivä on siis perjantai 15.5 ja sitä korvaava koulupäivä on koulun kevätjuhlapäivä 23.5.</p>
<p>Jarmo Jääskeläinen<br>
Hansakallio<br>
rehtori</p>
<p></p>
