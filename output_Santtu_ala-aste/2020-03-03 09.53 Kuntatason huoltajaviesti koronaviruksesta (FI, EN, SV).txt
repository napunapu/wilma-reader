Kuntatason huoltajaviesti koronaviruksesta (FI, EN, SV)
Kass Veronika (V.K)
Piilotettu
2020-03-03 09.53

<p>Hyvät huoltajat,</p>
<p>Opetushallituksen kehotuksen mukaisesti koronaviruksen leviämiseen varautuminen on ajankohtaista kouluissa, oppilaitoksissa ja varhaiskasvatuksessa. Siksi Espoon varhaiskasvatuksessa ja opetuksessa suunnitellaan jo etukäteen toimintatapoja esimerkiksi tilanteisiin, joissa oppilaita/opiskelijoita tai opettajia on poissa suuri määrä tai oppilas/opiskelija tai henkilökunnan jäsen sairastuu kesken koulupäivän. Opetusta on järjestettävä oppilaalle, joka on karanteenissa tai eristyksissä.</p>
<p>Seuraamme jatkuvasti THL:n ja HUS:n sekä muiden viranomaisten antamia ohjeita. Espoossa toimitaan kansallisten ohjeiden mukaisesti kaikissa tilanteissa. Sivistystoimen johtoryhmä ohjeistaa kouluja, lukioita ja päiväkoteja yhteistyössä näiden kansallisten ohjeiden pohjalta.</p>
<p>Mahdollisista karanteeneista tai yksiköiden sulkemisesta päättävät terveydenhuoltoviranomaiset yhteistyössä eri viranomaisten kanssa.</p>
<p>Karanteenitapauksessa oppilas opiskelee koulun antamien ohjeiden mukaisesti itsenäisesti. Opetuksessa hyödynnetään mahdollisuuksien mukaan etäyhteyksiä.</p>
<p>Sosiaali- ja terveyspalveluilla on pandemiasuunnitelma, ja valmiusryhmä seuraa tarkasti tilanteen kehittymistä. Ohjeistusta sosiaali- ja terveyspalveluista saa aina tilanteen vaatiessa.</p>
<p><strong>Vanhemmille ja kaikille muillekin espoolaisille </strong>on tietoa verkkosivuillamme:</p>
<p>&nbsp;</p>
<p>Dear guardians,</p>
<p>As advised by the Finnish National Agency for Education, it is important for early childhood education units, schools and other educational institutions to prepare for the spread of the coronavirus. For this reason, the City of Espoo’s education and early childhood education units have started proactive planning. We are planning ways to handle situations where, for example, a large number of pupils/students or teachers are absent or a pupil/student or a staff member falls ill during the day. Teaching must be provided for a pupil/student who is in quarantine or isolation.</p>
<p>We will keep following the instructions given by the Finnish Institute for Health and Welfare (THL) and the Helsinki and Uusimaa Hospital District (HUS). The City of Espoo will comply with the national guidelines in all situations. The management team of the city’s Education and Cultural Services will provide guidelines for schools and day care centres based on these national guidelines.</p>
<p>Decisions on possible quarantines or closing of schools/day care centres will be made by health care authorities in cooperation with other authorities.</p>
<p>In a quarantine situation, a pupil/student will do independent study at home in accordance with their school’s instructions. Remote communication will be used as much as possible.</p>
<p>The City of Espoo Social and Health Services has a pandemic preparedness plan, and the rapid intervention team is closely monitoring the situation. Social and Health Services provides advice whenever required.</p>
<p><strong>Parents and everyone in Espoo</strong> can read more on our website:</p>
<p>&nbsp;</p>
<p>Bästa vårdnadshavare,</p>
<p>Enligt Utbildningsstyrelsens uppmaning är det aktuellt i skolor, läroanstalter och småbarnspedagogik att förbereda sig på att coronaviruset sprids. Därför planeras det redan på förhand tillvägagångssätt inom Esbo stads småbarnspedagogik och undervisning till exempel för situationer där ett stort antal elever/studerande eller lärare är frånvarande eller där en elev/studerande eller en medlem av personalen insjuknar mitt under skoldagen. Det ska ordnas undervisning för elever som är i karantän eller isolerade.</p>
<p>Vi följer kontinuerligt Institutet för hälsa och välfärds och Helsingfors och Nylands sjukvårdsdistrikts samt andra myndigheters anvisningar. Esbo stad handlar i enlighet med nationella anvisningar i alla situationer. Bildningssektorns ledningsgrupp ger skolor, gymnasier och daghem anvisningar i samarbete utifrån dessa nationella anvisningar.</p>
<p>Beslut om eventuella karantäner eller stängning av enheter fattas av hälso- och sjukvårdsmyndigheterna i samarbete med olika myndigheter.</p>
<p>I karantänfall studerar eleven självständigt enligt skolans anvisningar. I undervisningen utnyttjas om möjligt distansförbindelser.</p>
<p>Social- och hälsovårdstjänsterna har en pandemiplan, och beredskapsgruppen följer noggrant hur situationen utvecklas. Social- och hälsovårdstjänsterna ger anvisningar alltid när situationen kräver det.</p>
<p><strong>Föräldrarna och alla andra Esbobor</strong> får information på stadens webbplats:</p>
