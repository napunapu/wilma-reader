Hansakallion koulun neljänsien luokkien hyvinvointikyselyn tuloksia
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2017-11-22 09.09

<p>Arvoisat huoltajat</p>
<p>Olemme Hansakallion koulussa lokakuussa kartoittaneet kaikkien luokkatasojen oppilaiden hyvinvointia. Seuraavassa joitain keskeisiä poimintoja neljäsluokkalaisten oppilaiden hyvinvointikyselyn vastauksista. Vastausprosentti oli noin 90.</p>
<p>Kysyttäessä oppilaiden vireystilasta koulussa, aamiaisen syömisestä ja nukkumisen määrästä tulokset ovat myönteisiä. Silti muutama oppilas ilmoittaa ettei syö aamupalaa. Nukkumisesta tinkii vähän useampi; noin viisi prosenttia vastanneista sanoo ettei nuku riittävästi. Toki joskus muulloinkin yöunet eivät ole riittävän pitkät. Koko koulun tuloksiin verrattuna tilanne on hyvä, samoin arviot oppilaiden vireystilasta koulussa. Vain muutama oppilas ilmoittaa, että vireystila on koulussa heikko.</p>
<p>Kiusaamista on syksyn aikana esiintynyt jonkin verran. Välitunneilla on joskus nimittelyä, pilkkaamista ja haukkumista. Uhkatilanteista koulumatkalla on myös muutama maininta. Tämä on oppilaiden huomio kysyttäessä koko koulun tasoa. Luokkatason vastausten perusteella tilannetta voi luonnehtia kuitenkin hyväksi. Kiusaaminen on ollut satunnaista ja yksittäisiä tapahtumia. Toistuvaa ja pitkäkestoista kiusaamista ei oppilaiden mielestä tapahdu.</p>
<p>Luokkatason ns. puolustuskulttuuri on vahva. Oppilaat kokevat vahvaa yhteisöllisyyttä ja luokkatasotarkastelussa voi nähdä sen vähentävän kiusaamista. Oppilaat mainitsevat myös, että kunkin kaveritilanne on hyvä, he viihtyvät hyvin yhdessä ja koulua on kiva käydä.</p>
<p>Vastausten mukaan oppilaat luottavat vahvasti opettajiinsa, he kokevat saavansa koulussa onnistumisen kokemuksia ja oikeudenmukaista kohtelua. Heidät hyväksytään sellaisina kuin he ovat.</p>
<p>Kaiken kaikkiaan hyvinvointikysely antaa kuvaa neljännen luokan oppilaiden vahvasta yhteisöllisyydestä ja samalla oppilaiden hyvästä minäkuvasta ja itseluottamuksesta. Luokissa ja oppilailla on vahvaa puolustuskulttuuria. Torjuttaessa kiusaamista ilmiönä &nbsp;puolustuskulttuurilla onkin hyvin tärkeä merkitys.&nbsp;</p>
<p>&nbsp;</p>
<p>Hansakallion koulun oppilashuoltotiimin puolesta,</p>
<p>&nbsp;</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
