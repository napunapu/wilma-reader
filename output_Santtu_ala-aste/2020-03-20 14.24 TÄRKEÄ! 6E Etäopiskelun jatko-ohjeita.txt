TÄRKEÄ! 6E Etäopiskelun jatko-ohjeita
Lukkarinen Elina (ElinaL)
Piilotettu
2020-03-20 14.24

<p>Hei kotiväki!</p>
<p>Ensimmäinen vajaa viikko etäopiskelua on takana. Viikko on mennyt uuden oppimisen, kokeilemisen ja ohjaamisen äärellä. Olen päättänyt linjata hieman tarkemmin muutamia toimintatapoja ensi viikosta eteenpäin, tässä viestissä tulee siis tärkeitä tarkennuksia etäopiskeluohjeistukseen.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Tämä viesti menee hieman muokattuna myös oppilaille. Käyttehän sen silti yhdessä tarkasti läpi!&nbsp;</p>
<p>Aurinkoista ja rentouttavaa viikonloppua kaikille!&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin Elina&nbsp;</p>
