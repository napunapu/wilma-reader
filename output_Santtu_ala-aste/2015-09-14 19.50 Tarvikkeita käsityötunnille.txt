Tarvikkeita käsityötunnille
Mannersalo Annu (AnnuM)
Piilotettu
2015-09-14 19.50

<p>Hei,
</p>
<p>Olemme piakkoin aloittamassa 2.a -luokan kanssa käsityössä askartelua, johon kaipaamme kotoa hieman apua.</p>
<p>Tarvitsemme tyhjiä ja puhtaita säilyketölkkejä. Tölkit voivat olla 'hernekeittotölkin' kokoisia ja isompiakin, joita saa esimerkiksi suolakurkuista tai vastaavista. Pienet ja matalat tölkit eivät valitettavasti käy.<br>
Jokainen oppilas voisi tuoda muutaman tölkin.</p>
<p>Kiitoset avustanne,</p>
<p>Aurinkoa syksyyn,<br>
Annu,<br>
resurssiope</p>
<p></p>
