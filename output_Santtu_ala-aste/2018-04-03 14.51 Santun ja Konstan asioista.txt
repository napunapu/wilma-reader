Santun ja Konstan asioista
Mannersalo Annu (AnnuM)
Tarvainen Outi (Santtu Tarvainen, 6E/2020), Korpela Panu (Santtu Tarvainen, 6E/2020), Lukkarinen Elina (ElinaL)
2018-04-03 14.51

<p><span style="font-size:14px;">Hei Outi!</span></p>
<p><span style="font-size:14px;">Elina välitti minulle viestin reilu viikkoa ennen pääsiäistä huolestanne Santun suhteen. Elina on itse pois ja minä olen koulun Kiva-tiimissä ( kiusaamisen vastainen ) ja lisäksi työskentelen usein&nbsp;tämän luokan kanssa, joten oli minulle luontevaa ottaa pojat keskusteluun. Pahoittelen, että viestitän asiasta vasta nyt, koska olin itse yllättäen pääsiäisviikon poissa työstä.</span></p>
<p><span style="font-size:14px;">Juttelin Santun kanssa pitkään ja rauhallisesti. Minulle hän kertoi, ettei Konsta&nbsp;ole koulussa kiusannut häntä sanallisesti eikä fyysisesti enää kolmosella eikä nelosella. Kysyin asian muutamaan eri otteeseen ja eri tavoin, vastaus oli aina sama. Koulun jälkeen koulumatkalla Konsta on kerran tönäissyt Santtua. Tämä asia selvitettiin ja oli käsittääkseni selvitetty jo aiemminkin.</span></p>
<p><span style="font-size:14px;">Sen sijaan vapaa-ajalla pojilla on joskus tullut erimielisyyttä leikeistä. Käsitin, että jos Santtu on ollut Konstalla leikkimässä ja uusi kaveri on tullut paikalle, on Santtu jäänyt leikistä sivuun. Tämä luonnollisesti on harmittanut. Keskustelin asiasta myös Konstan kanssa. Kävimme läpi reilun kaveruuden tapoja, jolloin Konsta lupasi huomioida tämän asian paremmin. Me koulussa voimme puhua asiasta yleisesti, painottaa hyvän kaverin tapoja, mutta emme luonnollisestikaan voi rajoittaa kenenkään vapaa-ajalla tapahtuvaa toimintaa.</span></p>
<p><span style="font-size:14px;">Molemmat kuitenkin vaikuttivat siltä, että haluavat olla kavereita, enkä Santusta saanut esille ainakaan mitään koulussa tapahtuvaa kiusaamista. Painotin, että tämä on tärkeää meidän kuulla, mikäli niin tapahtuu. Kyselen asiasta vielä tällä viikolla, onko mitään uutta ilmennyt asian tiimoilta.&nbsp;</span></p>
<p><span style="font-size:14px;">Mikäli teille tulee vielä mitään kerrottavaa, ottakaa mielellään yhteyttä.</span></p>
<p><span style="font-size:14px;">ystävällisin terveisin</span></p>
<p><span style="font-size:14px;">Annu</span></p>
<p><span style="font-size:14px;">resurssiopettaja</span></p>
<p>&nbsp;</p>
