Tokaluokkalaisten terveystarkatukset
Tammenkoski Manta (M.T)
Piilotettu
2016-02-15 10.20

<p><b><span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial">HYVÄT VANHEMMAT,</span></span></span></b><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"> </span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"> </span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"><u><b><i>tokaluokkalaisten terveystarkastukset ovat alkamassa hiihtoloman jälkeen.</i></b></u></span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"> </span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial">Oppilaat kutsutaan koulupäivän aikana luokasta. Tapaamisessa tarkistetaan kasvu ja muut yksilöllisesti tarpeenmukaiset asiat.</span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"> </span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial">Jos teillä on toiveita terveystarkastukseen liittyen, olkaa yhteydessä joko puhelimitse tai Wilma -viestillä terveydenhoitajaan.</span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"> </span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial">Terveystarkastuksesta saatte kirjallisen palautteen lapsen mukana. Jos tarkastuksessa ilmenee jotain erityistä, olen yhteydessä teihin.</span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial"> </span></span></span><br>
<span style="font-size:18px"><span style="color:#000000"><span style="font-family:Arial">Ystävällisin terveisin, </span></span></span><br>
</p>
<p><span style="font-size:18px"><span style="color:#000000"><span style="font-family:Times New Roman"> </span></span></span></p>
<p><span style="font-size:18px"><span style="color:#000000"><span style="font-family:Times New Roman">Kouluterveydenhoitaja Manta Tammenkoski</span></span></span></p>
<p><span style="font-size:18px"><span style="color:#000000"><span style="font-family:Times New Roman">Puh 046 8771453</span></span></span></p>
<p><b><span style="font-size:18px"><span style="color:#000000"><span style="font-family:Times New Roman"> </span></span></span></b></p>
<p><span style="font-size:18px"><span style="color:#000000"><span style="font-family:Times New Roman"> Alakoululaisen terveyteen ja hyvinvointiin liittyvää tietoa</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Times New Roman"> </span></span></span></p>
<p><a href="http://www.mll.fi/vanhempainnetti/ika/6-9/" target="_blank" rel="noopener" class="outbound"><span style="color:#249fff"><span style="font-size:18px"><span style="font-family:Times New Roman">http://www.mll.fi/vanhempainnetti/ika/6-9/</span></span></span></a></p>
<p><a href="http://www.thl.fi/fi_FI/web/fi/aiheet/tietopaketit/ravitsemustietoa/suomalaiset/kouluikaiset" target="_blank" rel="noopener" class="outbound"><span style="color:#249fff"><span style="font-size:18px"><span style="font-family:Times New Roman">http://www.thl.fi/fi_FI/web/fi/aiheet/tietopaketit/ravitsemustietoa/suomalaiset/kouluikaiset</span></span></span></a></p>
<p></p>
