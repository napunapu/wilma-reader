Terveisiä elämänkatsomustiedon tunnilta
Alén-Niskasaari Katri (KatriA)
Piilotettu
2014-09-23 10.11

<p>Hei!<br>
Olemme aloittaneet oppiaineen tutustumalla toisiimme,kun ryhmässä on oppilaita eri luokilta.Oikein mukavasti meillä on tunnit sujuneet. Olemme keskustelleet ystävyydestä ja siitä,että jokainen meistä on tärkeä jne.
</p>
<p>Tänään annoin ensimmäisen läksyn. Repusta löytyy siis et-vihko,johon olen liimannut tehtävämonisteen. Kävimme sen yhdessä läpi mutta toivon,että te siellä kotona voitte kysäistä lapselta muistaako hän mitä piti tehdä.Ja auttaa mikäli on tarvetta.<br>
Tehtävässä väritetään asioita eli on siis mukava ja helppo tehtävä.Sen teemana on tervehtiminen,ystävällisyys,auttaminen ja ilo.</p>
<p>Sitten tulisi vielä muistaa tuoda vihko  ensi tiistain tunnille. Puhuimme siitä,että vihkon voi tuoda kouluun jo ennen tiistaita ja laittaa  se pulpettiin odottamaan tuntia. Tai sitten laittaa reppuun maanantai-iltana jotta se on mukana tiistaiaamuna.</p>
<p>Toivottelen teille iloa syyspäiviin!</p>
<p>Ystävällisin yhteistyöterveisin Katri Alén,alämänkatsomustiedon opettaja</p>
<p></p>
