Huoltajatiedote 11.12.2015
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2015-12-11 12.09

<p>Arvoisat huoltajat,
</p>
<p>Lukukauden viimeisen kouluviikon lukujärjestys on seuraava: maanantai - keskiviikko koulua käydään normaalisti lukujärjestyksen mukaan, to. 17.12 koulua käydään klo 9-13.15 ja pe. 18.12 klo 9-12.15. Perjantaina käymme joulukirkossa kolmessa erässä. Oppilaat saavat välitodistuksen niin ikään perjantaina.</p>
<p>Koulun joulujuhla järjestetään lauantaina 12.12. Tilaisuudesta on lähetetty tarkempi kutsu aikatauluineen. Tervetuloa!</p>
<p>5.A-luokka järjestää juhlan aikana buffetin leirikoulurahaston kasvattamiseksi.</p>
<p></p>
<p>Rauhallista joulun odotusta ja hyvää uutta vuotta 2016 toivottaa</p>
<p></p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki</p>
<p></p>
