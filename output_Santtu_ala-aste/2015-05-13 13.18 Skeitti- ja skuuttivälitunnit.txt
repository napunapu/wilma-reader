Skeitti- ja skuuttivälitunnit
Helotie Anni (HeAn)
Piilotettu
2015-05-13 13.18

<p>Hei!<br>
Ensi viikon maanantaina ja tiistaina saa kouluun tuoda skeitin ja skuutin. Niitä on pitkällä välitunnilla lupa käyttää. Kypärä on pakollinen!<br>
Muistuttelen vielä, että kouluun tullaan seuraavan kerran maanantaina!<br>
Terkuin, Anni-ope</p>
