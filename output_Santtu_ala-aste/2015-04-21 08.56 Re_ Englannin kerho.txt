Re: Englannin kerho
Tarvainen Outi (Santtu Tarvainen, 6E/2020)
Rauhalammi Sonja (S.R), Korpela Panu (Santtu Tarvainen, 6E/2020)
2015-04-21 08.56

<p>Hei Sonja,<br>
Unohdimme muistuttaa viime viikolla Santtua englanninkerhosta. Hänellä on aina hyppytunti ennen kerhoa ja tällä kertaa emme saaneet hänelle ajoissa sanaa kouluun eikä hän ehtinyt kotiin ajoissa, että olisimme voineet tuoda hänet uudelleen koululle.
</p>
<p>Santtu oli kauhean pahoillaan, että kerho jäi väliin. Olisinkin kysynyt kuinka täyttä teillä on B-ryhmässä eli voisiko Santtu tulla nyt tämän viikon torstaina paikkaamaan viime viikkoista? Ymmärrän hyvin, jos on täyttä ja tämä ei onnistu, mutta lupasin Santulle kuitenkin kysyä.</p>
<p>Parhain teveisin,</p>
<p>                 Outi, Santun äiti</p>
<p></p>
