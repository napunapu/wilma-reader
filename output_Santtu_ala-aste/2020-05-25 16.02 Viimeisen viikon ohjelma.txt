Viimeisen viikon ohjelma
Lukkarinen Elina (ElinaL)
Piilotettu
2020-05-25 16.02

<p>Hei kotiväki!&nbsp;</p>
<p>Viimeistä alakouluviikkoa viedään. Viime viikolla oppilaat pitivät huikeita, hauskoja ja koskettavia puheita omalle luokalle, mikä myös konkretisoi sitä, että alakoulun päivät alkavat käymään vähiin. Viimeiselle viikolle olemme miettineet kivaa ohjelmaa. Viimeisellä viikolla oma ope pitää kaikki tunnit ja aikataulut poikkeavat hieman lukujärjestyksestä. Tässä viimeisen viikon aikataulu:&nbsp;</p>
<p>&nbsp;</p>
<p>Aurinkoista viikkoa kaikille!&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin Elina&nbsp;</p>
