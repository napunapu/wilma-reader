Fwd: Hansakallio Home and School Association: Christmas tree and Christmas flower orders
Väyrynen Sirpa (SirpaV)
Piilotettu
2019-11-14 19.41

<p><strong>Hansakallio Home and School Association: Christmas tree and Christmas flower orders</strong></p>
<p>Hansakallio Home and School Association sells quality Christmas trees and Christmas flowers again!<br>
With the funds we raise, we help and delight our children through events and enliven the school's everyday life.</p>
<p><strong><u>Christmas Trees</u></strong><br>
The Christmas trees we sell are 1st class grown Finnish firs.<br>
Spruce comes in two sizes, ie 2.1-2.4 m (normal) and 2.7-3 m (large). Size ranges are indicative.&nbsp;<br>
Price 50 € regardless of size.&nbsp;You can choose your favorite tree on the spot. The firs are netted for home delivery.</p>
<p>Orders must be saved no later than 04.12.2019! Distribution of Christmas trees in the courtyard of Hansakallio School: 19.12.2019 at 17:00-19:00. The method of payment is cash.</p>
<p>You can place an order for Christmas trees using<a href="https://docs.google.com/forms/d/e/1FAIpQLSfF7z5738FllcnRODQvzzkDFyU4TgozVdxA_2mFVtPT9KrMTg/viewform"> this form</a>. Order is binding!</p>
<p><u><strong>Christmas Flowers</strong></u><br>
The Christmas flowers we sell are Finnish grown amaryllis, poinsettia, Christmas kalanchoe and Christmas cypress.</p>
<p>Orders must be saved no later than 04.12.2019! Flowers will be picked up on: 14.12.2009 at 9:00-12:00 from Hansakallio School.<br>
At the same time there is also a Christmas market at Hansakallio School.</p>
<p>Payment method is Cash or MobilePay.<br>
You can place your order for the Christmas flowers using <a href="https://docs.google.com/forms/d/e/1FAIpQLSfZl5svQ5mzcC42mGl6qTJ6PEn7t2yyzSG_IzfAf0oV2zo9vg/viewform">this form</a>. Order is binding!<br>
<br>
Merry Christmas and Happy Holidays!<br>
Hansakallion Koti- ja Koulu yhdistys</p>
