Re: Parkour-kerhosta
Tarvainen Outi (Santtu Tarvainen, 6E/2020)
Korpela Panu (Santtu Tarvainen, 6E/2020), Koukkumäki Jari (JariK)
2015-11-04 15.54

<p>Hei,<br>
Todella kivasti ajateltu, että kaikki pääsisivät mukaan! <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span><br>
En tiedä onko sitä huomioitu, että torstaisin on jo kakkosluokkalaisten enkkukerho klo 13-14 ja esim. 2A:lla on liikuntaa kaksi tuntia juuri torstaina. Muutenkin koulu alkaa jo kahdeksalta, joten päivästä tulisi 8-15 eli aika pitkä. En tiedä kuinka paljon lapset enää jaksavat siinä vaiheessa iltapäivää. Toisaalta into olisi kova, joten mukana ollaan oli se niin tai näin.<br>
t. Outi</p>
