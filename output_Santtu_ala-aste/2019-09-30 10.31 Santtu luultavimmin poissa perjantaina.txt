Santtu luultavimmin poissa perjantaina
Korpela Panu (Santtu Tarvainen, 6E/2020)
Tarvainen Outi (Santtu Tarvainen, 6E/2020), Lukkarinen Elina (ElinaL)
2019-09-30 10.31

<p>Moi,</p>
<p>Santulla on silmälääkäri perjantaina alkaen klo 9. Jos tutkimuksessa laitetaan laajentavat tipat kuten yleensä, kestää käynti pitkään ja muutamaan tuntiin ei näe lukea. Jos tippoja ei laiteta, ehdimme varmaankin klo 11 alkavalle tunnille.</p>
<p>Terv, Panu</p>
