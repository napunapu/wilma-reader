Huoltajaviesti 12.12.2018
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2018-12-12 19.36

<p>Arvoisat huoltajat,</p>
<p>Hansakallion koulun vieressä, Hansavalkaman puolella on loppusyksyn mittaan käynnistynyt mittava rivitalojen korjausurakka. Alueella liikkuu isoja työkoneita ja kiinteistöjen piha-alueita rakennetaan uusiksi. Liikuttaessa kouluun ja pois koulusta on syytä noudattaa erityistä varovaisuutta. Rakentamisalueen aidottaminen on rakennusurakoitsijan vastuulla. Merkittyä aluetta on kunnioitettava eikä rakennusaluetta saa käyttää kulkureittinä. Asiasta kannattaa keskustella kotona, me muistutamme asiasta koulussa.</p>
<p>Juhlistamme Lucia -neitoa torstaina 13.12 ja ensi viikolla käymme keskiviikkona seurakunnan järjestämässä joulukirkossa. Joulukirkon aikana&nbsp;on tarjolla vaihtoehtoista ohjelmaa, suunnittelu ja toteutus yhteistyössä Suomen&nbsp;punaisen ristin kanssa.</p>
<p>Viimeisen viikon oppilaiden työjärjestyksessä on yksi muutos. Perjantaina 21.12 koulu loppuu kaikilla klo 12.15.</p>
<p>&nbsp;</p>
<p>Hansakallion koulun väki toivottaa kaikille rauhallista joulun aikaa.</p>
<p>Jarmo Jääskeläinen</p>
