Vesipelastuskoulutuksesta
Mannersalo Annu (AnnuM)
Piilotettu
2018-08-30 11.04

<p><span style="font-size:14px;">Hei,</span></p>
<p><span style="font-size:14px;">Koko luokka lähtee tiistaina vesipelastuskoulutukseen huolimatta siitä, osallistuuko koulutukseen vedessä vai katsomossa.</span></p>
<p><span style="font-size:14px;">Mikäli Santtu pystyy osallistumaan uintiin, Santulle uintikamat mukaan. Muuten hän osallistuu tarkkailu- ja raportointitehtävään muiden kanssa.</span></p>
<p><span style="font-size:14px;">Koululle siis tiistaina 4.9. jo kello 7.45. Seuraavana keskiviikkona 5.9.kouluun vasta kello 9.00. Tällä korvataan aikainen aamuherätys tiistaina.</span></p>
<p><span style="font-size:14px;">terveisin</span></p>
<p><span style="font-size:14px;">Annu</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
