Tiedoksi puhelinnumeroita ja muuta liittyen poikkeavaan tilanteeseen
Häkkinen Jonna (J.H)
Piilotettu
2020-03-20 11.29

<p>Hei,</p>
<p>Tiedoksi koteihin, että oppilashuolto on edelleen käytettävissä. Tapaamiset tullaan näillä näkymin järjestämään joko puhelimitse, etäyhteyksin tai yksittäisten oppilaiden käynteinä ajanvarauksen kautta. <strong>Tästä&nbsp;on tulossa&nbsp;tarkempi tiedote wilmaan myöhemmin.</strong></p>
<p><strong>*************************************************************************************************************************************************************************************************************************</strong></p>
<p><strong>Vinkkejä liittyen koronakaranteeniin ja koti-opetukseen:</strong></p>
<p><a href="https://www.lskl.fi/blogi/huoltajan-selviytymisopas-koronaviruskaranteeniin-ja-kotiopetukseen/?fbclid=IwAR2E9Ib-EG26-UGdL8Tb81uUIl-i-bSnlKazaaZilRJWwMowblGqczBkuBs">https://www.lskl.fi/blogi/huoltajan-selviytymisopas-koronaviruskaranteeniin-ja-kotiopetukseen/?fbclid=IwAR2E9Ib-EG26-UGdL8Tb81uUIl-i-bSnlKazaaZilRJWwMowblGqczBkuBs</a></p>
<p><a href="https://www.nuortennetti.fi/koulu-ja-tyo/koulu/vinkkeja-etaopiskeluun/">https://www.nuortennetti.fi/koulu-ja-tyo/koulu/vinkkeja-etaopiskeluun/</a></p>
<p><a href="https://yle.fi/uutiset/3-11260168?origin=rss">https://yle.fi/uutiset/3-11260168?origin=rss</a></p>
<p>**************************************************************************************************************************************************************************************************************************</p>
<p><strong>&nbsp;Hyödyllisiä puhelinnumeroita /chatteja/nettisivuja:</strong></p>
<p>-<strong>Kriisipuhelin</strong> 09 2525 0111, <strong>Kristelefon</strong> 09 2525 0112, <strong>Crisis Telephone in English and Arabic 09 2525 0113</strong>, lisätietoa: <a href="https://mieli.fi/fi/tukea-ja-apua/kriisipuhelin-keskusteluapua-numerossa-09-2525-0111"><u>https://mieli.fi/fi/tukea-ja-apua/kriisipuhelin-keskusteluapua-numerossa-09-2525-0111</u></a></p>
<p>-MIELI Suomen Mielenterveys ry Instagram ja Facebook -sivut</p>
<p>-Sekaisin-chat <a href="https://sekasin247.fi/">https://sekasin247.fi/</a>&nbsp; (12- 29 vuotiaille)</p>
<p>-MLL vanhempainpuhelin 0800 92277, lisätietoa: <a href="https://www.mll.fi/vanhemmille/toiminta-lapsiperheille/vanhempainpuhelin/?gclid=EAIaIQobChMI0q7o97ej6AIVBKWaCh2yyggvEAAYASAAEgKJUPD_BwE">https://www.mll.fi/vanhemmille/toiminta-lapsiperheille/vanhempainpuhelin/?gclid=EAIaIQobChMI0q7o97ej6AIVBKWaCh2yyggvEAAYASAAEgKJUPD_BwE</a></p>
<p>-MLL lasten ja nuorten puhelin 116&nbsp;111, lisätietoa: <a href="https://www.mll.fi/vanhemmille/toiminta-lapsiperheille/lasten-ja-nuorten-puhelin/">https://www.mll.fi/vanhemmille/toiminta-lapsiperheille/lasten-ja-nuorten-puhelin/</a></p>
<p>**************************************************************************************************************************************************************************************************************************</p>
<p><strong>Seuraavissa linkeissä tietoa, miten koronasta kannattaisi puhua lasten ja nuorten kanssa.</strong></p>
<p><u><a href="https://www.unicef.fi/korona-virus-8-ohjetta-nain-puhut-lapselle/">https://www.unicef.fi/korona-virus-8-ohjetta-nain-puhut-lapselle/</a></u></p>
<p><a href="https://www.mll.fi/uutiset/lapset-puhuvat-koronasta-mita-aikuinen-voi-tehda/">https://www.mll.fi/uutiset/lapset-puhuvat-koronasta-mita-aikuinen-voi-tehda/</a></p>
<p>**************************************************************************************************************************************************************************************************************************</p>
<p>Voimia ja kärsivällisyyttä kaikille tähän poikkeukselliseen aikaan!</p>
<p>Yt.</p>
<p>Jonna Häkkinen</p>
<p>kuraattori</p>
<p>p.0438246377</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
