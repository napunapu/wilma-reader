Ensi viikosta ja vähän ensi vuodestakin!
Lukkarinen Elina (ElinaL)
Piilotettu
2016-04-22 14.57

<p>Hei vain!
</p>
<p>Ensi viikolla koulussamme vietetään taas "kahmi kilsoja"-viikkoa. Viikon aikana siis kerätään rasteja, ja yhden rastin saa kävellen, pyöräillen tai skuutaten liikutusta kilometristä(1km). Mukaan lasketaan koulumatkat (kävellen tai skuutaten/skeitaten) sekä jokainen vapaa-ajalla (pyörällä, kävellen tai skuutaten) liikuttu kilometri. Ahkerimmin liikkunut luokka palkitaan retkellä Tempputemmellykseen, mikä tuntui olevan tavoittelemisen arvoinen palkinto myös lasten mielestä <span class="emoticon" data-file="wink" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/wink.png&quot;);"></span></p>
<p>Ensi viikolla koululla järjestetään 1.-2.luokkalaisille myös mahdollisuus skuutata ja skeitata pitkillä välitunneilla. Oppilailla tulee olla kypärä päässä myös välitunneilla liikuttaessa. Ihanan liikunnallinen viikko siis tulossa!</p>
<p>Lisäksi uutisia ensi vuodesta. Ilokseni voin nyt ilmoittaa, että tulen jatkamaan luokkamme opettajana myös ensi vuonna! Meillä on ollut mahtava ja hauska vuosi, joten samalla meiningillä jatketaan myös ensi vuonna <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span> Luokkatilamme tulee todennäköisesti pysymään myös samana. Uusi opetussuunnitelma tulee kuitenkin tuomaan uusia tuulia koko koulumaailmaan!</p>
<p>Rentouttavaa viikonloppua toivotellen,</p>
<p>Elina-ope</p>
<p></p>
