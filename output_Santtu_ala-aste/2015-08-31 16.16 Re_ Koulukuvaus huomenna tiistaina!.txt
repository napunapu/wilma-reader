Re: Koulukuvaus huomenna tiistaina!
Tarvainen Outi (Santtu Tarvainen, 6E/2020)
Korpela Panu (Santtu Tarvainen, 6E/2020), Lukkarinen Elina (ElinaL)
2015-08-31 16.16

<p>Hei,<br>
Onko koululla ylimääräisiä kuvauslappuja? Santun lappu oli mennyt pilalle märkien vaatteiden alla repussa. Santulla on huomenna yhdeksän aamu, joten hän voisi kipaista hakemaan uuden lapun aamulla ja käydä tuomassa sen kotiin täytettäväksi. Tai jos koululla on joku paikalla kl0 8 maissa, kun vien nuoremman veljen hoitoon siihen viereen, voisin täyttää lapun siinä lennossa.<br>
t. Outi</p>
