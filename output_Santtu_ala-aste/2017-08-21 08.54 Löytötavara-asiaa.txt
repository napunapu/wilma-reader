Löytötavara-asiaa
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2017-08-21 08.54

<p>Arvoisat huoltajat</p>
<p>&nbsp;</p>
<p>Viime kevään kiireissä unohdimme muistuttaa oppilaita ja vanhempia kouluun jääneistä löytötavaroista. Sovimme koulun siivoushenkilökunnan kanssa, että he keräävät naulakkoihin jääneet vaatteet ja muut tavarat talteen. Kevätlukukauden 2017 aikana mahdollisesti kouluun jäänyttä omaisuutta voi tulla etsimään koulun löytötavaroista syyskuun ensimmäiselle viikolla saakka. Löytötavarat ovat alakerran käytävällä, jonne helpoiten pääsee opettajan huoneen vieressä olevia rappusia pitkin. Tavarat ovat läpinäkyvissä laatikoissa ja säkeissä.&nbsp;Syyskuun ensimmäisen&nbsp;kokonaisen viikon &nbsp;jälkeen tyhjennämme löytötavarat. Sen jälkeen sinne aletaan kerätä tänä syksynä löytyneitä tavaroita.</p>
<p>&nbsp;</p>
<p>Tavaroiden häviämistä ennaltaehkäisee vaatteiden nimikointi. Löytötavaroiden palauttaminen nimen perusteella on osoittautunut hirvittävän haasteelliseksi tehtäväksi, koska monista vaatteista kierrätyksen vuoksi saattaa löytyä lapsen nimi, jota ei edes ole koulussamme. Siksi emme valitettavasti ehdi emmekä voi lähteä jokaisesta vaatteesta nimeä etsimään. Usein nimikoitu vaate löytää oikein omistajansa oman luokan naulakosta jo ennen kuin se päätyy löytötavaroihin, joten siksi nimikointi olisi suositeltavaa.</p>
<p>&nbsp;</p>
<p>Hansakallion koulu</p>
