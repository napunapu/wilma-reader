Terveystarkastukset 3 lk
Varjoinen Outimaria (O.V)
Piilotettu
2017-01-10 08.17

<p><strong>HYVÄT VANHEMMAT,</strong></p>
<p>&nbsp;</p>
<p>kolmasluokkalaisten terveystarkastukset ovat alkamassa. Olette tervetulleita mukaan lapsenne terveystarkastukseen. Varatkaa aika tämän kirjeen saatuanne, jos haluatte osallistua tarkastukseen. Muussa tapauksessa lapsenne kutsutaan koulupäivän aikana luokasta.</p>
<p>Tulette saamaan reppupostina terveyskyselyn, joka täytetään yhdessä lapsen kanssa.&nbsp; Kysely palautetaan opettajalle suljetussa kirjekuoressa&nbsp; 25.1.2017&nbsp; mennessä. Tapaamisessa tarkistetaan kasvu ja muut yksilöllisesti tarpeenmukaiset asiat.</p>
<p>Jos teillä on toiveita terveystarkastukseen liittyen, olkaa yhteydessä joko puhelimitse tai Wilma -viestillä terveydenhoitajaan.</p>
<p>Terveystarkastuksesta saatte kirjallisen palautteen lapsenne mukana. Jos jotain erityistä ilmenee, olen yhteydessä teihin.</p>
<p>&nbsp;</p>
<p>Ystävällisin terveisin,</p>
<p>Terveydenhoitaja Outimaria Varjoinen&nbsp;</p>
<p>p. 046-8771453</p>
<p>&nbsp;</p>
<p>Alakoululaisen terveyteen ja hyvinvointiin liittyvää tietoa</p>
<p><a href="http://www.mll.fi/vanhempainnetti/">http://www.mll.fi/vanhempainnetti/</a></p>
<p><a href="http://www.thl.fi/fi_FI/web/fi/aiheet/tietopaketit/ravitsemustietoa/suomalaiset/kouluikaiset">http://www.thl.fi/fi_FI/web/fi/aiheet/tietopaketit/ravitsemustietoa/suomalaiset/kouluikaiset</a></p>
<p>&nbsp;</p>
