ru 6. lk etätehtävät viikko nro 7
Halpin Leila (LeilaH)
Piilotettu
2020-04-27 15.53

<p>God eftermiddag&nbsp;alla!</p>
<p>Vappuviikon (etäviikko nro 7) tehtävät tulevat tässä!</p>
<p>Tule liveen ke 29.4. klo 11!&nbsp;</p>
<p>Katso Lasse-Maijan -etsivätoimisto von Bromsin salaisuus -elokuva ja vastaa kysymyksiin suomeksi vihkoosi:<br>
https://areena.yle.fi/1-3261137</p>
<p>Lue tehtävät etukäteen, jotta osaat miettiä niitä elokuvan aikana. Vastaukset tehtäviin 1 ja 7 selviävät päättelemällä, kun koko elokuva katsottu. Muuten vastaukset ovat helposti löydettävissä. Jos et löydä vastauksia kaikkiin, ei haittaa. Jos osaat laittaa tekstitykset ruotsiksi, niin se olisi jättebra! Elokuva kestää 1h 17 minuuttia ja vastaamisessakin kestää, joten tämä tehtävä kattaa hyvin tämän viikon ja ensi maanantain tehtävät.&nbsp;</p>
<p>1. Mitä Lasse ja Maija etsivät?<br>
2. Mitä tapahtui, kun Maija hyppäsi ruutua?<br>
3. Miksi von Bromsin duo halusi tulla kirkkoon esiintymään?<br>
4. Miksi kirkon kattoa ei voitu korjata heti?<br>
5. Miten pappi pääsi pois lukitusta huoneesta?<br>
6. Mitä Ronaldille oli tapahtunut kirkossa?<br>
7. Miten Richard huijasi muita (kaksi asiaa)?<br>
8. Missä Werner asui?<br>
9. Miksi sormukset olivat tärkeitä?<br>
10. Miten Lasse ja Maija pääsivät Julian huoneeseen?<br>
11. Miten Lasse ja Maija pääsivät Richardin jäljille?<br>
12. Miksi Werner pääsi soittamaan von Bromsin duoon?<br>
13. Kuka varasti lippaan? Miksi?<br>
14. Mikä oli von Bromsin salaisuus?<br>
15. Piditkö elokuvasta?</p>
<p>Glad Valborg!</p>
<p>Önskar er alla,</p>
<p>Leila</p>
<p>&nbsp;</p>
