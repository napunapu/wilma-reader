Kesäisiä taideleirejä tarjolla kouluikäisille
Sorvari Heidi (HeidiS)
Piilotettu
2018-04-25 15.56

<p>Hei Hansakallion koulun huoltajat!</p>
<p>Tässä info- ja ilmoittautumislinkki Soukassa ja Kauklahdessa järjestettäviin taidekesäleireihin. Leirit on tarkoitettu kaikille yli 7-vuotiaille lapsille. Toiminnan järjestää Taidekoulu Tatavuu.</p>
<p><a href="https://soukantaideseura.fi/2018/04/16/taidekoulu-tatavuun-taidekesa-soukassa-ja-kauklahdessa/">https://soukantaideseura.fi/2018/04/16/taidekoulu-tatavuun-taidekesa-soukassa-ja-kauklahdessa/</a></p>
<p>Leiriterveisin,</p>
<p>Heidi Sorvari</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
