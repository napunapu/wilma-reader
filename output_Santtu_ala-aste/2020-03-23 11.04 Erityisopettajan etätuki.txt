Erityisopettajan etätuki
Vasara Liisa (LiisaV)
Piilotettu
2020-03-23 11.04

<p><span style="font-size:12px;">Hei,</span></p>
<p><span style="font-size:12px;">tämä viesti saapuu kohdennetusti vain&nbsp;<u>tehostettua/erityistä&nbsp;tukea saavan&nbsp;<strong>JA/TAI</strong> minun opetuksessani säännöllisesti olleen</u>&nbsp;2.-/5.-/6.-luokkalaisen huoltajille.</span></p>
<p><span style="font-size:12px;">Olen laaja-alaisena erityisopettajana normaalisti antanut lapsellesi osa-aikaista erityisopetusta jossakin aineessa joitakin tunteja viikossa -&nbsp; tyypillisimmin suomen kielessä tai matematiikassa. Olen myös saattanut tukea&nbsp;lastasi esimerkiksi toiminnanohjauksessa tai&nbsp;itsesäätelytaidoissa.&nbsp;</span></p>
<p><span style="font-size:12px;">Nyt poikkeustilassa tämä tuki on katkolla - on silti erittäin tärkeää, että oppilas saa tarvitsemaansa tukea jollain tavalla.&nbsp;</span><span style="font-size:12px;"><strong>Olen säännöllisesti luokanopettajien tavoitettavissa</strong>, ja tarjoan heille apuani esimerkiksi eriyttävien tehtävien laatimiseen.&nbsp;He myös viestivät minulle luokkansa tilanteesta ja etäopiskelun sujumisesta.&nbsp;</span></p>
<p><span style="font-size:12px;"><strong>Tässä keinoja, kuinka voin tarjota tukeani sinne koteihin:</strong></span></p>
<p><span style="font-size:12px;">Kotiopiskelussa on tärkeää luoda päivään rutiineja ja pitää riittävästi taukoja. Päivän ohjelma&nbsp;ja tehtävät kannattaa kirjata paperille näkyviin, jotta oppilas hahmottaa, mitä päivän aikana tulisi saada tehtyä. </span></p>
<p><span style="font-size:12px;"><strong>Tärkeintä mielestäni&nbsp;on, että etäopiskelussa päästään nyt&nbsp;pikku hiljaa alkuun ja saamme kaikki aikaa totutella uuteen, haastavaankin tilanteeseen.&nbsp;</strong>Olemme uuden äärellä, eikä kaiken tarvitse mennä heti nappiin.&nbsp;</span></p>
<p><span style="font-size:12px;">Luokanopettajat ovat varmasti valmiita joustamaan ohjeissa ja&nbsp;mukauttamaan tehtäviä (määrää/laajuutta/tehtävätyyppiä), jos näyttää siltä, että ne ovat oppilaalle syystä tai toisesta kohtuuttoman vaativia tai aikaavieviä. Olkaa meihin opettajiin reippaasti yhteydessä, jos näin on.&nbsp;</span></p>
<p><u><span style="font-size:12px;">Ethän epäröi ottaa minuun yhteyttä, jos oppimisen haasteita&nbsp;ilmaantuu.</span></u></p>
<p><span style="font-size:12px;">Kovasti kaikille tsemppiä toivotellen,</span></p>
<p><span style="font-size:12px;">Liisa-erityisopettaja</span></p>
<p>&nbsp;</p>
