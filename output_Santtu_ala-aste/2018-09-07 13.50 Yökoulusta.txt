Yökoulusta
Mannersalo Annu (AnnuM)
Piilotettu
2018-09-07 13.50

<p><span style="font-size:12px;">Hei,</span></p>
<p><span style="font-size:12px;">Emme puhuneet luokassa yökoulusta sen kummemmin aiemmin, ettei Santtu pahoittaisi tästä mieltään. Jos Santtu kuitenkin kuulee asiasta, kertonette hänelle, että järjestämme ehdottomasti yökoulun kevätpuolella uudestaan, niin että Santtukin pääsee yöpymään koululla.</span></p>
<p><span style="font-size:12px;">Ystävällisin terveisin</span></p>
<p><span style="font-size:12px;">Annu</span></p>
<p><span style="font-size:12px;">ps. pahoittelen, että luokkaviestit näissäkin asioissa saattavat lähteä myös teille</span></p>
