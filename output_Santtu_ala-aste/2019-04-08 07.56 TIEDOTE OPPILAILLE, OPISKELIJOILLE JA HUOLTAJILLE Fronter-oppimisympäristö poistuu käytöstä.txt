TIEDOTE OPPILAILLE, OPISKELIJOILLE JA HUOLTAJILLE Fronter-oppimisympäristö poistuu käytöstä
Lippo Asko (A.L)
Piilotettu
2019-04-08 07.56

<p><strong>Tallenna tiedostosi Fronterista 31.5.2019 mennessä - Espoon kaupunki luopuu sähköisen oppimisympäristöpalvelu Fronterin käytöstä kevätlukukauden 2019 lopussa. </strong></p>
<p><strong>Hyvä oppilas/opiskelija, tiedoksi huoltaja: </strong>&nbsp;</p>
<p>Tallenna itsellesi Fronter-oppimisympäristöstä kaikki omat tiedostosi mahdollisimman pian ja viimeistään ennen lukuvuoden päättymistä.&nbsp;</p>
<p>Voit tallentaa tiedostosi omalle työasemallesi tai mobiililaitteellesi. Tallentamisen jälkeen voit ladata tiedostot halutessasi Office 365 - tai G Suite -pilvipalvelun tallennustilaan.</p>
<p>Mikäli sinulla on kysyttävää tai tarvitset apua tiedostojen tallentamiseen Fronter-oppimisympäristöstä, voit pyytää apua ja tukea opettajaltasi.</p>
<p><strong>Fronterin käyttö päättyy lopullisesti 30.6.2019</strong></p>
<p>--------</p>
<p>Espoon kaupunki tarjoaa oppimisen tueksi monimuotoisen oppimisympäristön, joka tukee oppijoiden yksilöllisiä oppimisen tarpeita sekä tukee lasten ja nuorten mediataitojen kehittymistä. Koulussa oppilaat perehdytetään tieto- ja viestintäteknologian vastuulliseen käyttöön opetussuunnitelman mukaisesti oppilaan ikätaso huomioiden. Oppilaiden käytössä on sähköinen opiskeluympäristö, joka koostuu työasemaympäristöstä ohjelmistoineen, verkkopalveluista ja digitaalisista sisällöistä. Kaupunki vastaa sähköisen opiskeluympäristön turvallisuudesta, sekä rekisterinpitäjänä henkilötietojen käsittelystä ja sen lainmukaisuudesta.</p>
<p>Espoon kaupunki tarjoaa oppilaiden ja opiskelijoiden käyttöön Office 365 - G Suite -pilvipalvelut.</p>
<p>Opiskelijarekisterin tietosuojaseloste ja kaupungin keskitetysti tarjoaman opiskeluympäristön rekisteriselosteet ovat nähtävillä Espoon kaupungin verkkosivulla [[https://www.espoo.fi/tietosuojaselosteet]] Sivistystoimi &gt; Koulutus-kohdasta. Luettelo opiskeluympäristön käytettävistä palveluista löytyy Kasvatus ja opetus -välilehdeltä Oppiva Espoo -kohdasta. Mikäli koulussa on käytössä muita palveluja, koulu informoi niistä erikseen.</p>
<p>&nbsp;</p>
<p>Lisätietoja:</p>
<p>Asko Lippo, puh. +358 43 8257554, sähköposti: asko.lippo (@) espoo.fi</p>
<p>vastuualuepäällikkö, Espoon kaupunki</p>
