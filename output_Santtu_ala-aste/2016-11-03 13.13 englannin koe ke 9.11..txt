englannin koe ke 9.11.
Salmi Päivi (PäiviS)
Piilotettu
2016-11-03 13.13

<p>Hei!</p>
<p>Pidän englannin kokeen keskiviikkona 9.11. Kokeeseen tulee tekstikirjasta (Reader) alkukappaleet A, B, C, D, E ja kappaleet 1-2 (myös Start-kappaleet)</p>
<p>s. 6-26&nbsp;ja työkirjasta (Writer) s. 5-38. Tarkemmat ohjeet kokeeseen luettavista asioista löytyy oppilaille jaetusta monisteesta.</p>
<p>Myös sanomapron sivuilla on tehtäviä kappaleisiin.</p>
<p>Terveisin Päivi</p>
