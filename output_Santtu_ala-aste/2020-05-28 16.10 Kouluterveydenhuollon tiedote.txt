Kouluterveydenhuollon tiedote
Auvinen Arja (A.A)
Piilotettu
2020-05-28 16.10

<p><strong>KOULUTERVEYDENHUOLLON TIEDOTE SEITSEMÄNNEN LUOKAN OPISKELIJAN TERVEYSTARKASTUKSESTA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></p>
<p>Seitsemännen luokan opiskelijoiden terveystarkastukset alkavat touko-kesäkussa jatkuen koko lukuvuoden. Terveystarkastukset tehdään tulevalla yläkoululla.</p>
<p>Huoltajat ovat tervetulleita terveystarkastukseen yhdessä nuoren kanssa. Jos haluatte osallistua, olkaa hyvä ja varatkaa aika terveydenhoitajalta puhelimitse tai sähköisesti <a href="https://www.espoo.fi/fi-FI/Asioi_verkossa/Sosiaali_ja_terveyspalvelut/Ajanvaraukset_verkossa"><u>https://www.espoo.fi/fi-FI/Asioi_verkossa/Sosiaali_ja_terveyspalvelut/Ajanvaraukset_verkossa</u></a> ennen 30.8. Opiskelija voi tulla myös varaamaan aikaa avovastaanottoaikana lukuvuoden alkaessa. Jos teillä on jotain huomioitavaa terveystarkastukseen liittyen, olkaa yhteydessä terveydenhoitajaan.</p>
<p>&nbsp;</p>
<p>Terveystarkastusaika tulee peruuttaa viimeistään tarkastusta edeltävänä päivänä klo 15 mennessä. Sairaustapauksessa tai vastaavassa muussa äkillisessä tilanteessa vastaanottoaika tulee peruuttaa mahdollisimman pian terveydenhoitajalle puhelimitse.</p>
<p>Opiskelijat, joille ei ole varattu aikaa tulevat 1.9 alkaen terveydenhoitajan vastaanotolle koulupäivän aikana.&nbsp; Jos terveystarkastuksessa ilmenee jotain erityistä terveydenhoitaja on huoltajiin yhteydessä. Opiskelija saa kirjallisen terveystarkastuspalautteen</p>
<p>Ystävällisin terveisin,</p>
<p>Espoon koulu- ja opiskeluterveydenhuolto</p>
<p>&nbsp;</p>
<p><a href="https://www.espoo.fi/fi-FI/Sosiaali_ja_terveyspalvelut/Terveyspalvelut/Koululaiset_ja_opiskelijat/Terveydenhoitajien_yhteystiedot"><u>Yläkoulujen terveydenhoitajien yhteystiedot</u></a></p>
<p><a href="https://www.espoo.fi/fi-FI/Sosiaali_ja_terveyspalvelut/Terveyspalvelut/Koululaiset_ja_opiskelijat"><u>Koululaiset ja opiskelijat</u></a></p>
<p><a href="https://www.espoo.fi/fi-FI/Sosiaali_ja_terveyspalvelut/Terveyspalvelut/Hammashoito"><u>Espoon hammashoito</u></a></p>
<p><a href="https://thl.fi/web/infektiotaudit-ja-rokotukset/tietoa-rokotuksista/kansallinen-rokotusohjelma"><u>Kansallinen rokotusohjelma</u></a></p>
