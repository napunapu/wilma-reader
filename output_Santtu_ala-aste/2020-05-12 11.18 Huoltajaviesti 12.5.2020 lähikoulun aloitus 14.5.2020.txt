Huoltajaviesti 12.5.2020 lähikoulun aloitus 14.5.2020
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2020-05-12 11.18

<p>Arvoisat huoltaja</p>
<p>&nbsp;</p>
<p><strong>Peruskouluissa palataan lähiopetukseen 14.5.2020. Lukioissa etäopetus jatkuu lukukauden loppuun saakka.&nbsp;</strong>&nbsp;</p>
<p>Hallitus päätti 29.4.2020, että varhaiskasvatuksen ja perusopetuksen rajoitteet puretaan. Lähiopetukseen peruskouluille palataan 14.5.2020 alkaen. Tämän jälkeen kunnat eivät voi järjestää etäopetusta perusopetuksen oppilaille.&nbsp;&nbsp;Noudatamme Terveyden ja hyvinvoinnin laitoksen sekä Opetus- ja kulttuuriministeriön laatimia ohjeita, jotta lähiopetukseen palaaminen sujuisi mahdollisimman turvallisesti. Hygieniaan, siivoukseen ja koulutilojen väljempään käyttöön kiinnitetään erityistä huomiota.&nbsp;</p>
<p><strong>Kouluun ei saa tulla sairaana</strong></p>
<p>Kouluun ei saa tulla, jos oppilas on sairaana tai hänellä on sairastumiseen viittaavia oireita.&nbsp;&nbsp;</p>
<p>Jos lapsellasi, sinulla tai perheenjäsenelläsi on koronavirustartuntaan viittaavia oireita, kuten hengitystieoireita (kuume, yskä, kurkkukipu, nuha, hengenahdistus, haju/makuaistin menetys) tai ripuli ilman ilmeistä syytä, soita Espoon koronaneuvontaan, p. 09 816 34600 ma−pe klo 7−18 ja muina aikoina Päivystysapuun, p. 116117 (numerot palvelevat espoolaisia). </p>
<p><strong>Poissaoloja haetaan opettajalta tai rehtorilta</strong></p>
<p>Poissaolojen hakeminen ja myöntäminen tapahtuu normaaliin tapaan ja poissaoloon tulee hakea lupa. Riskiryhmiin kuuluvien lasten hoitava lääkäri arvioi, voiko lapsi palata kouluun.&nbsp;</p>
<p><strong>Järjestelyt Hansakallioin koulussa</strong></p>
<p>1. Raamin koulupäivälle antaa voimassa oleva oppilaan lukujärjestys. Porrastamme jonkin verran koulun alkamisaikoja. Täsmällisen aloitusajan koteihin lähettää luokanopettaja. Toivomme, että oppilaat tulevat jämptisti annettuun aikaan ja paikkaan&nbsp;koulun pihalle. Mukaan otetaan lukujärjestyksen mukaiset kirjat ja opiskelutarvikkeet. On tärkeää, että oppilaalla on mukanaan oma kynä ja kumi. Vältämme opiskelutarvikkeiden lainaamista luokan sisällä.</p>
<p>2.&nbsp;Porrastamme ruokailuja ja haemme väljyyttä ruokasaliin. Osa oppilaista syö luokissa. Ruoka jaetaan valmiiksi. Jonossa pidetään turvaväli. Vältämme lisäannoksien hakemista,&nbsp;koskee myös&nbsp;maitoa, näkkäriä yms. Tavoittelemme, että oppilaalla on rauha aterioida, mutta itse ruokailutilanne hoidetaan sujuvasti.</p>
<p>3. Porrastamme välitunteja ja rajaamme niiden pituutta. Luokat voivat käydä myös joustavasti välituntitauoilla. Käytämme joustavasti kaikkia koulun ulos/sisäänmeno-ovia. Joudumme olemaan aiempaa tarkempia siinä, miten pihalla voi leikkiä ja käyttää pihan yhteisiä&nbsp;leikkivälineitä. Ohjeistus koskee myös liikunnan tunteja. Tavoitteena on tietty väljyys myös välituntien osalta.</p>
<p>4. Yleisestä hygieniasta huolehditaan mahdollisuuksien mukaan. Kädet pestään, kun tullaan ulkoa sisälle ja ennen ruokalua. Oppilasryhmät pidetään omana ryhmänään mahdollisuuksien mukaan. Kotiin luvalla jääneille oppilaille opettaja toimittaa ns. läksypaketin. Lähikoulu korvaa etäkoulun. Molempia ei voi olla.</p>
<p>5. Varmasti selviämme hyvin&nbsp;yhdessä tulevasta runsaasta kahdesta viikosta.&nbsp;Lukuvuoden viimeisen päivän, 30.5.2020 ohjelmaan palataan erikseen myöhemmin.&nbsp;Hansakallion koulu toivottaa&nbsp;kaikille tsemppiä ja mukavaa kevättä!</p>
<p>&nbsp;</p>
<p>Iltapäivätoiminta jatkuu koululla 14.5. alkaen. Jos lapsesi ei osallistu iltapäivätoimintaan, ilmoita siitä iltapäivätoiminnan järjestäjälle.</p>
<p>Ilmoita koululle, jos lapsesi tai ei tarvitse koulukuljetusta tai iltapäivätoiminnan vuoksi kuljetusaikaa tulee muuttaa.</p>
<p>Muiden kuin oppilaiden ja koulun henkilökunnan oleskelua koulualueella vältetään. Oppilaiden toivotaan tuovan lainassa oleva tietokone tai muu vastaava mahdollisimman nopeasti takaisin koululle/ vahtimestari Oskari Kajander, kuitenkin viimeistään maanantaina 18.5.2020.</p>
<p>&nbsp;</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
<p>Hansakallion koulu</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
