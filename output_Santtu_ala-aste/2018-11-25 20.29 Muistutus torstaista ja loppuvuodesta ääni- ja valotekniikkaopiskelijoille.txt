Muistutus torstaista ja loppuvuodesta ääni- ja valotekniikkaopiskelijoille
Hietaranta Veera (VeeraR)
Piilotettu
2018-11-25 20.29

<p>Hei,</p>
<p>muistuttaisimme&nbsp;teitä ääni- ja valotekniikkavalinnaisen opiskelijoiden koteja, että koulu alkaa torstaina klo 8.30, kuten aiemmin syksyllä ilmoitimme. Oppilaat voivat tulla liikuntasaliin tuolloin. Laitamme heidän kanssaan salin tekniikan kuntoon päivän Lava on vapaa -esityksiä varten.</p>
<p>Se onkin sitten viimeinen koko valinnaisryhmän kokoontuminen, sillä torstaina ei ole iltapäivällä valinnaistuntia juhlaviikon lyhennetyn päivän takia ja viimeiset kaksi kertaa on oppilailla vapaita, koska olemme tehneet tuplatunteja. Lucia-juhlan tekniikkavastaavilla on omaa toimintaa vielä viikolla 50. Heidän kanssaan olemme aikataulun&nbsp;käyneet&nbsp;läpi ja laitamme&nbsp;heidän koteihin vielä lähempänä erillisen viestin asiasta,</p>
<p>Valinnaista on ollut kiva ohjata tämän syksyn ajan ja oppilaat ovat olleet innokkaita perehtymään koulun&nbsp;ääni- ja valotekniikkaan ja suunnittelemaan juhlien sekä tapahtumien tekniikka-asioita. He ovat myös ottaneet hienosti vastuuta tekniikan hoitamisesta eri tilaisuuksissa.&nbsp;</p>
<p>Riemukasta joulun odotusta kaikille!</p>
<p>Terveisin,</p>
<p>Veera ja Pauliina</p>
