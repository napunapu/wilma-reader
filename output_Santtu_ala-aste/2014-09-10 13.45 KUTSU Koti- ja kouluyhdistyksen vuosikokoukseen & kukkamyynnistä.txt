KUTSU Koti- ja kouluyhdistyksen vuosikokoukseen & kukkamyynnistä
Väyrynen Sirpa (SirpaV)
Piilotettu
2014-09-10 13.45

<p>Hei,
</p>
<p>KUTSU VUOSIKOKOUKSEEN:<br>
Hansakallion koti- ja kouluyhdistyksen vuosikokous pidetään keskiviikkona 24.9. klo 18 koulun ruokalassa.<br>
Vuosikokouksessa valitaan uusi hallitus lukuvuodelle 2014-2015. Uudet jäsenet ovat lämpimästi tervetulleita.<br>
Työskentely ei vaadi aikaisempaa kokemusta, vain halua osallistua keskusteluun ja<br>
päätöksentekoon koulun ja lasten arkeen liittyen.</p>
<p>Yhdistyksen toimintasuunnitelmassa on valmiita aihioita syksyn tapahtumista,<br>
mutta uusi hallitus päivittää suunnitelman ja tekee asioista lopulliset päätökset.</p>
<p>Ennakkoilmoittautumista vuosikokoukseen ei tarvita paitsi tilanteessa,<br>
että haluaa mukaan hallitukseen, mutta ei pääse kokoukseen.<br>
Tällöin tarvitaan kirjallinen ilmoitus hallitukselle (ole yhteydessä jutta.tikkanen@trainershouse.fi tai 050 376 1121).</p>
<p>Kutsumme kaikki vanhemmat mukaan!</p>
<p>HUOM! Yhdistys pitää välittömästi uuden hallituksen valinnan jälkeen kokouksen luokkatoimikuntien kanssa.<br>
Suunnittelemme yhdessä marraskuun tapahtuman koululle. Suunnittelu aloitetaan klo 18.30.<br>
Tervetuloa mukaan tähänkin!</p>
<p>KUKKAMYYNNISTÄ:<br>
Syyskukkia tulee tuttuun tapaan myyntiin tällä viikolla.<br>
Tilauslappu lähetetään koteihin reppupostissa torstaina 11.9. ja<br>
se palautetaan luokanopettajalle tiistaina 16.9.</p>
<p>Tilattavissa ovat:<br>
- Calluna, punainen (12cm ruukku) 12 eur/3 kpl<br>
- Calluna ISO punainen (25cm ruukku) 14 eur/kpl<br>
- Hopealanka (12cm ruukku), 2,50eur/kpl<br>
- Hebe greens (lajitelma, 12cm ruukku), 4 eur/kpl</p>
<p>Kukat noudetaan koululta ke 24.9. klo 18.30-20 välillä tai to 25.9. koulupäivän aikana.<br>
Kukkamyynnin tuotto käytetään koulun ja lasten hyväksi.</p>
<p>Ystävällisin terveisin,<br>
Yhdistyksen hallitus</p>
<p></p>
