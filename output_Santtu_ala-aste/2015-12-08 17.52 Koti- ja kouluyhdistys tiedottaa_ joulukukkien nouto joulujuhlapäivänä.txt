Koti- ja kouluyhdistys tiedottaa: joulukukkien nouto joulujuhlapäivänä
Väyrynen Sirpa (SirpaV)
Piilotettu
2015-12-08 17.52

<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">Joulukukat ovat noudettavissa koulun ruokalasta lauantaina 12.12.2015 </span></span></span><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri"> klo 8:30-9, klo 10-11 ja klo 12-12:30. Varaattehan tasarahan maksuun.</span></span></span>
</p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">terveisin Hansakallion koti-ja kouluyhdistys</span></span></span></p>
<p></p>
