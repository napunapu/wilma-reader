5-luokkalaisen lääkärintarkastus, sähköinen ajanvaraus
Auvinen Arja (A.A)
Piilotettu
2018-11-26 11.23

<p>Hei viidesluokkalaisten vanhemmat!</p>
<p>Koululääkärin ajanvarauslistat jo täynnä tältä vuodelta, ja osa lapsista vielä käymättä vo:lla.</p>
<p>&nbsp;- tiedotan tuonnempana&nbsp;teille milloin sähköinen ajanvaraus jälleen aloitetaan vuona&nbsp; -19 puolella.</p>
<p>- Hyvää adventinaikaa!</p>
<p>Arja Auvinen, th</p>
