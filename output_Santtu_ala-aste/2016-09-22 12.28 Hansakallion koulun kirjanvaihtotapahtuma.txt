Hansakallion koulun kirjanvaihtotapahtuma
Selkälä Terhi (TerhiS)
Piilotettu
2016-09-22 12.28

<p>Hei,</p>
<p>Haluamme Hansakallion koulussamme tutustuttaa lapsia monipuolisesti kirjallisuuteen ja innostaa lapsia lukemaan. Lisäksi haluamme edistää kestävän kehityksen ajatusta myös kaunokirjallisuuden tuotteiden kohdalla. Siksi järjestämme koulussamme kirjanvaihtotapahtuman. Kirjanvaihtotapahtumassa oppilas saa vaihtaa tuomansa sarjakuvat, pokkarit ja kirjat. Oppilas saa jokaisesta tuomastaan sarjiksesta, pokkarista tai kirjasta Hansakallion koulun leikkirahaa, joilla hän voi ostaa tapahtumapäivinä muiden oppilaiden tuomia kirjoja.</p>
<p>&nbsp;</p>
<p>Nyt toivomme, että lapsenne toisi kouluun EHJIÄ, SIISTEJÄ JA MIELENKIINTOISIA lasten ja nuorten kirjoja, sarjiksia ja pokkareita. Mikäli myös&nbsp;naapurinne haluaa eroon omista lasten- ja nuorten kirjoistaan, voitte pyytä lahjoituksia myös heiltä (eli jos omasta hyllystä ei löydy vaihtokirjoja, pyytäkää tutuiltanne).</p>
<p>&nbsp;</p>
<p>Tapahtumasta jäljelle jääneet kirjat lahjoitetaan koulukirjastoon&nbsp;ja &nbsp;luokkakohtaisiin kirjastoihin. Näistä ylijääneet kirjat, pokkarit ja sarjikset viemme kierrätyskeskukseen.</p>
<p>&nbsp;</p>
<p>Kirjoja voi tuoda kouluun perjantaista 23.9. alkaen aina perjantaihin 30.9. asti oman luokan opettajalle. Kirjanvaihtopäivät ovat koulullamme 11.10. - 13.10.2016.</p>
<p>&nbsp;</p>
<p>Nyt laitetaan hyvä kiertämään!</p>
<p>&nbsp;</p>
<p>Hansakallion koulun kestävän kehityksen tiimi</p>
