Lukuhaaste
Tahkola Eeva (E.T)
Piilotettu
2018-05-03 12.11

<p>Hei kotiväki!</p>
<p>Ala-asteen lukutestin (Allu) tulokset ovat näkyvissä Wilmassa. Näette tulokset kohdassa<strong> Lomakkeet </strong>ja sieltä edelleen <strong>Allu-testitulokset.&nbsp;</strong></p>
<p>Kävimme tällä viikolla erityisopettaja Sirpan kanssa luokan allutestitulokset läpi. Sovimme Sirpan kanssa, että otamme loppukeväästä luokalle lukuhaasteen. Tarkoituksena on, että jokainen oppilas lukee vähintään 5 x 15min joka viikko, tai vaihtoehtoisesti pidempinä pätkinä niin, että kokonaisaika on 75 minuuttia lukuaikaa viikossa. Oppilaat ovat saaneet lomakkeen, johon muun muassa merkitään lukuaika ja vanhemman kuittaus. Lukupäiväkirjojen tarkistus on perjantaisin.</p>
<p>Luettava voi olla kirja, sarjakuva, tekstitetyt elokuvat, uutiset, nettiuutiset, sanomalehdet, aikakauslehdet ym. Suosittelen monipuolista lukemista, joten ei ole hyvä, jos kaikki lukeminen on esimerkiksi sarjakuvaa.</p>
<p>Nyt loppukeväästä muista aineista ei tule paljoakaan läksyä, joten nyt on hyvä aika kunnostautua lukemisessa. Lukutaidon kehittyminen on erittäin merkityksellinen asia oppilaan jatkon kannalta. Ohessa linkki lukutaidosta Suomessa ja lukutaidon heikkenemisestä.</p>
<p><span style="color:#333333;font-family:sans-serif,Arial,Verdana,&quot;Trebuchet MS&quot;;font-size:12px;">https://www.talouselama.fi/uutiset/kesalomalle-tekemista-ss-opettajat-antavat-lapsille-lomallekin-lukutehtavia/d10d5485-193f-3f66-80c6-d4a2a9d8a926</span></p>
<p><span style="color:#333333;font-family:sans-serif,Arial,Verdana,&quot;Trebuchet MS&quot;;font-size:12px;">Mukavia lukuhetkiä,</span></p>
<p><span style="color:#333333;font-family:sans-serif,Arial,Verdana,&quot;Trebuchet MS&quot;;font-size:12px;">Eeva</span></p>
