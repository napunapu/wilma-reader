Huoltajaviesti 12.10.2018
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2018-10-12 14.17

<p>Arvoisat huoltajat,</p>
<p>Hansakallion koulu on perustettu vuonna 1958. Täytämme tänä vuonna siis 60-vuotta. Syntymäpäiväjuhlia vietämme marraskuun lopussa maanantaina 26.11 alkavalla viikolla. Juhlaviikko pitää sisällään monia erilaisia&nbsp; oppilaille suunnattuja tapahtumia näyttelyistä esityksiin. Oppilaille ja huoltajille suunnattu juhlapäivä on lauantaina 1.12.2018. Tällöin pidämme koulupäivän. Tapahtumasta lähetetään erillinen kutsu. Koulupäivää vastaava vapaapäivä pidetään kiirastorstaina 18.4.2019.</p>
<p>Hansakallion koulu osallistui&nbsp;syksyllä Nälkäpäiväkeräykseen. Keräyksen tuotto oli yhteensä 466,58 euroa. Kiitokset kaikille osallistujille.</p>
<p>Toivotamme kaikille miellyttävää syysloman aikaa!</p>
<p>&nbsp;</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki.</p>
