Huoltajatiedote 5.12.2018: Arviointikeskustelut
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2018-12-05 10.07

<p>Arvoisat huoltajat,</p>
<p>Hansakallion&nbsp;60 -vuotias koulu kiittää kaikkia asianomaisia upeasta juhlaviikosta. Saimme paljon kannustusta yhdistää lauantai -koulupäivä ja luokkatoimikuntien syys-/joulumarkkinat samalle päivälle. Toivotamme kaikille hyvää itsenäisyyspäivää 2018. Ohessa tietoa pian käynnistyvistä arviointikeskusteluista.&nbsp;</p>
<p>Arviointikeskustelut käydään Hansakallion koulussa tammi-helmikuun aikana. Arviointikeskustelu on osa lukuvuoden aikaista arviointia. Keskustelussa kiinnitetään huomiota oppilaan vahvuuksiin ja onnistumisiin oppijana sekä käsitellään oppimistavoitteita ja oppimisen edistymistä. Arviointikeskustelulomakkeeseen kirjataan keskustelun olennaiset asiat. Arviointikeskustelulomake ei ole todistus.</p>
<p>Läsnä ovat siis oppilas, huoltaja/t ja luokanopettaja/erityisluokanopettaja; lisäksi mukana voi olla myös aineenopettaja, laaja-alainen erityisopettaja, kieli- ja kulttuuriryhmien opettaja tai resurssiopettaja. Oppilas ja huoltaja tuovat keskusteluun kodin kannalta merkittäviä asioita. Keskustelu voidaan käydä myös ilman huoltajaa, mikäli yrityksestä huolimatta &nbsp;yhteistä aikaa ei löydy.</p>
<p>&nbsp;</p>
<p>Perjantaina 14.12. avaa luokanopettaja Wilma -tapahtumakutsun luokkansa huoltajille. Sieltä voi käydä varaamassa perheelle sopivan ajan lapsen arviointikeskusteluun.</p>
<p>&nbsp;</p>
<p>Hansakallion koulu</p>
