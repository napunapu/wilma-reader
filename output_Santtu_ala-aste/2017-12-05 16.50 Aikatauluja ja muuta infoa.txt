Aikatauluja ja muuta infoa
Lukkarinen Elina (ElinaL)
Piilotettu
2017-12-05 16.50

<p>Hei kotiväki!
</p>
<p>Tässä aikatauluja viimeisille viikoille ennen joululomaa. Uinnit, itsenäisyyspäiväjuhlat, harjoitukset ym. aiheuttavat paljon muutoksia, mutta yritetään pysyä kartalla(myös allekirjoittanut). </p>
<p>- Torstaina 7.12. koulu alkaa kaikilla klo 9.00, kenraaliharjoituksen vuoksi. </p>
<p>- Perjantaina 8.12. A-ryhmäläisillä alkaa koulu vasta klo 9.00(tasataan uintien ylitunteja)</p>
<p>- Lauantaina 9.12. juhlimme 100-vuotiasta Suomea. Oppilailla on koulua klo 9.00-13.15. 4A-ja 4D-luokkalaiset esiintyvät klo 11.00 ja klo 12.15 alkavissa juhlissa. Lämpimästi tervetuloa katsomaan klo 12.15 alkavaa juhlaa ja oppilaiden valmistelemaa esitystä!</p>
<p>- Perjantaina 15.12 kaikilla alkaa koulu klo 10.15(tasataan uinneissa ja harjoituksista tulleita ylitunteja)!</p>
<p>- Viimeisellä kouluviikolla koulua on lukujärjestyksen mukaisesti maanantaista torstaihin. Perjantaina 22.12. koulua on kaikilla klo 8.15-12.15, jonka jälkeen siirrymme joululoman viettoon.</p>
<p>Lisäksi itsenäisyyspäivän jälkeen 4.luokkalaiset toimivat vuorollaan koulumme käytävävalvojina. Tällöin oppilaat eivät osallistu oppitunneille(paitsi kokeisiin), vaan tekevät päivän tehtävät itsenäisesti. Käytävalvojat työskentelevät päivän koulun aikuisten apuna(klo 8.15-13.15), huolehtien esim. ruokalan siisteydestä ja muista pienistä askareista. Näin osallistamme oppilaita osaksi koulun arkea ja oppilaat pääsevät harjoittelemaan vastuunkantoa. </p>
<p>On ollut huikeaa huomata, kuinka oppilaista on todella kasvanut vastuuntuntoisia  1disoja oppilaita 1d! Vaikka viime viikot ovat olleet kiireisiä ja muutoksia on tullut paljon, on oppilailla säilynyt hyvä tekemisen meininki ja asenne. </p>
<p>Hyvää itsenäisyyspäivää!</p>
<p>Terveisin Elina</p>
<p></p>
