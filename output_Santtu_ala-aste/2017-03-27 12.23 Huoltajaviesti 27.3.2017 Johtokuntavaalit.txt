Huoltajaviesti 27.3.2017 Johtokuntavaalit
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2017-03-27 12.23

<p>Arvoisat Hansakallion koulun oppilaiden huoltajat,</p>
<p>Tervetuloa Hansakallion koulun huoltajien kokoukseen valitsemaan uutta johtokuntaa. Kokous pidetään tiistaina 4.4.2017 klo 17.30 Hansakallion koulun ruokalassa, Hansakallio 4, 02780 Espoo.</p>
<p>Espoon peruskouluissa ja lukioissa toimivat koulukohtaiset johtokunnat. Johtokunnassa voi yhteistyössä muiden huoltajien ja koulun edustajien kanssa vaikuttaa mm. kodin ja koulun yhteistyöhön, koulun opetussuunnitelmaan, oppilaiden hyvinvointiin ja kouluyhteisön kehittämiseen.</p>
<p>Johtokuntien jäsenten ja varajäsenten valintaa varten koulussa pidetään huoltajien kokous, jossa oppilaiden huoltajat laativat opetus-ja varhaiskasvatuslautakunnalle ehdotuksen siitä, keitä henkilöitä tulisi valita koulun johtokuntaan toimikaudeksi 2017-2021.</p>
<p>Ehdokasasettelu tapahtuu kokouksessa ja siihen voivat osallistua vain paikalla olevat henkilöt.</p>
<p>Ehdotettavien henkilöiden on oltava kokouksessa paikalla tai heiltä pitää olla kirjallinen suostumus ehdokkuuteen. Johtokunassa voi olla 3, 4 tai 5 huoltajajäsentä ja jokaisella henkilökohtainen varajäsen. Ehdokkaina pitää olla sekä naisia että miehiä. Kokous myös ehdottaa, kenet tulisi nimetä johtokunnan puheenjohtajaksi ja kenet varapuheenjohtajaksi.</p>
<p>Ehdotettavilla on lapsi tai lapsia lukuvuoden 2017-2018 alussa oppilaana koulussa. Lisäksi olisi hyvä,&nbsp;että johtokuntaan saataisiin myös pienryhmien huoltajaedustus.</p>
<p>&nbsp;</p>
<p>Espoossa 27. maaliskuuta 2017</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
<p>Hansakallion koulu</p>
<p>&nbsp;</p>
