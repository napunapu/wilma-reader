Tervetuloa kouluun!
Mannersalo Annu (AnnuM)
Piilotettu
2018-08-09 10.36

<p><span style="font-size:14px;">Tervehdys&nbsp;'viis een' huoltajille!</span></p>
<p><span style="font-size:14px;">Oppilaat saapuivat iloisina ja virkistyneen oloisina kouluun. Mukava nähdä heitä kaikkia.</span></p>
<p><span style="font-size:14px;">Aloitamme ensi viikolla oppitunnit lukujärjestyksen mukaisesti, mutta huomenna perjantaina vielä hieman muokattu aikataulu.</span></p>
<p><span style="font-size:14px;">Koulu alkaa ruotsin opiskelijoilla klo 9.00, muilla klo 10.15.</span>&nbsp;<span style="font-size:14px;">Koulupäivä päättyy klo 13.15.</span></p>
<p><span style="font-size:14px;">Yhteistyöterveisin</span></p>
<p><span style="font-size:14px;">Annu Mannersalo</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
