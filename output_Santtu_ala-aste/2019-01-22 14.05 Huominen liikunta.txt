Huominen liikunta
Hult Heidi (HeidiH)
Piilotettu
2019-01-22 14.05

<p>Hei kotiväki!</p>
<p>Edellisestä ilmoituksesta poiketen, huomisella vitosten liikuntatunnilla on mahdollisuus vain ulkoliikuntaan. Sali on pois opetuskäytöstä iltapäivän. Eli huomenna siis mahdollista luistella tai osallistua muuhun ulkoliikuntaan.</p>
<p>Sisäliikuntamahdollisuus palaa ohjelmistoon taas ensi viikolla.&nbsp;</p>
<p>Terkuin,</p>
<p>Heidi + muut vitosten opet</p>
