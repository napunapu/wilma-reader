Huoltajatiedote 30.1.2019
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2019-01-30 12.33

<p>Arvoisat huoltajat</p>
<p>Lukukausi on taas alkanut ja arki pyörii täysillä. Luokissa käydään arviointikeskusteluja ja toiveena on, että kaikki arviointikeskustelut ovat käyty hiihtolomaan mennessä. Kevätlukukausi tuo&nbsp;oppilaiden ja&nbsp;huoltajien vastattavaksi paljon erilaisia kyselyitä. Valinnaisainekysely 4. - 6.luokkalaisille ja A2-kielikysely tulee vastattavaksi lähiaikoina. Koulumme neljänneltä vuosiluokalta alkavat A2-kielet ovat ruotsi ja saksa. Kieltä voi silloin jatkaa yläkoulussa pitkänä kielenä. Lisäksi kaupunki kysyy kaikilta 3-6 -luokan oppilailaita heidän käsityksiään koulun sisäilmasta. Tästä tulee vielä erillinen viesti koteihin. Sisäilmakysely toteutetaan oppilaille koulussa 28.1 - 7.2.2019. Ja vielä; huoltajilta kysytään koulun itsearvioinnin osana 1.2.2019 mennessä käsityksiä Hansakallion koulusta. Syyslukukaudella selvitettiin, mitä mieltä oppilaat ovat kouluahjostaan. Kyselyitä on kieltämättä paljon, mutta niiden tarkoitus on saada tietoa toimintamme kehittämiseksi. Kannustamme teitä vastaamaan!</p>
<p>Hansakallion koulussa on käynnistynyt Hansapilotti. Pilotin tavoitteet liittyvät koulun selviämiseen alati monimuotoistuvassa kasvatus- ja opetustehtässään. Yleisellä tasolla voi todeta, että monet oppilaiden erilaiset haasteet tuovat luokkiin tilanteita, joissa tarvitaan&nbsp;koulun tasolla uusia työkaluja selvitä. Samalla koulua ympäröivältä palveluverkostolta, kuten perhetyö, lastensuojelu, lasten psyykkinen&nbsp;hoito ja tuki&nbsp;sekä muu moniammatillinen tuki&nbsp;vaaditaan uutta vaikuttavampaa työotetta. Kaikkea tätä pilotoidaan Hansapilotin osana lukuvuosina 2018-20.</p>
<p>Luokkatasot järjestävät kukin omaan tahtiinsa hiihtoloman molemmin puolin talviliikkumispäivän. Päiviä järjestetään Oittaalla, Leppävaarassa tai koulun lähimaastossa. Luokkatasot tiedottavat päivästä tarkemmin.</p>
<p>Koulun parkkipaikoilla on nyt talvella erityisen ahdasta. Kevättä ja pyöräilykelejä odotellessa toivomme, että autoliikenne ottaa koulun läheisyydessä tarkasti huomioon kaikki koulumme oppilaat!</p>
<p>Talvi on nyt upeimmillaan ja liikkumismahdollisuudet ovat&nbsp;kaikkille oppilaille tarjolla. Ruokala ponnistelee tarjotakseen tarveellisen ruuan joka päivä. Välillä ruuan kysyntä voi selvästi ylittää totutun määrän ja tämä voi joskus haastaa ruokalahenkilökuntaa&nbsp;ruuan mitoitustehtävässä.&nbsp;&nbsp;</p>
<p>Talviterveisin&nbsp;</p>
<p>&nbsp;</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki</p>
