Bändiasiaa
Hietaranta Veera (VeeraR)
Piilotettu
2016-09-24 14.59

<p>Hei kolmosten vanhemmat!<br>
&nbsp;</p>
<p>Olen Veera Raivio, 2D-luokan opettaja ja vedän koulussa bändikerhoa. Viime vuodelta kerhossa jatkaa 5. ja 6. luokkalaisia poikia ja tyttöjä, mutta nyt myös muutamalle innokkaalle 3. luokkalaiselle &nbsp;on avautunut mahdollisuus liittyä koulun bändiin. Bändissä on auki kaksi laulajan paikkaa sekä&nbsp;yksi basistin ja yksi kitaristin paikka.&nbsp;</p>
<p>Jos lapsesi innostui ajatuksesta, laita minulle asiasta viestiä viimeistään tiistaina 27.9. Aikaisempaa kokemusta bändissä laulamisesta tai soittamisesta ei välttämättä tarvita, mutta musikaalisuutta, innokkuutta, sitoutumista sekä halua treenata tarvitaan. Bändi harjoittelee aina torstaisin klo 15-16 koulun musiikkiluokassa.</p>
<p>Valitettavasti en voi etukäteen luvata, mitä lapsesi saa bändissä tehdä. Bändiin tulijoiden kesken katsotaan, kuka alkaa soittaa mitäkin soitinta ja kuka laulaa. Toki toiveita kuunnellaan&nbsp;<img height="23" src="/shared/scripts/ckeditor/plugins/smiley/images/regular_smile.png" width="23">. Aluksi kokeillaan eri juttuja ja sitten vasta mietitään, kuka mitäkin tekee. Jos halukkaita tulijoita on liikaa tälle vuodelle, joudumme ehkä arpomaan osallistujat. Tämä kannattaa ehkä tuoda lapselle varmuuden vuoksi heti alussa esille, kun asiasta puhutte, ettei sitten mahdollinen pettymys tule yllätyksenä.</p>
<p>Bänditerveisin,</p>
<p>Veera Raivio</p>
