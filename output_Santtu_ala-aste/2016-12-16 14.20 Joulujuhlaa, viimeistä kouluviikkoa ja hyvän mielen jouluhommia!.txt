Joulujuhlaa, viimeistä kouluviikkoa ja hyvän mielen jouluhommia!
Lukkarinen Elina (ElinaL)
Piilotettu
2016-12-16 14.20

<p>Moikka! 
</p>
<p>Muistutan vielä huomisesta joulujuhlasta ja koulupäivästä. Koulu alkaa aamulla kello 9.00 ja päättyy kello 13.00. Olette erittäin lämpimästi tervetulleita yleisöksi kello 11.00 alkavaan juhlaan, jossa siis esiinnymme yhdessä 3 D-luokan kanssa. Tulemme myös seuraamaan klo 11.00 alkavan juhlan katsomoon. Kello 9.00-10.40 askartelemme, oleilemme ja syömme luokassa, joten tervetuloa myös piipahtamaan siellä! Normaalin koulupäivän tapaan, ilmoitattehan aamulla minulle Wilmassa jos sairastumisia tai muuta tulee. Toivon mukaan tietysti ei <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span></p>
<p>Ensi viikolla polkaistaan viimeinen kouluviikko käyntiin. Koulua on lukujärjestyksen mukaisesti maanantaista keskiviikkoon, torstaina oppilaiden koulupäivä on klo 9.00-12.15. Uuden opetussuunnitelman mukaan välitodistusta ei enää jaeta, vaan pidän tammi-helmikuussa jokaiselle oppilaalle henkilökohtaisen arviointikeskustelun yhdessä teidän huoltajien kanssa. Arviointikeskusteluajat tulevat varattavaksi tänne Wilmaan ensi viikon alussa. Laitan lisäohjeistusta vielä viestinä! </p>
<p>Ensi viikon tiistaina 20.12. juhlimme luokkamme pikkujouluja. Tällöin oppilaat voivat tuoda mukanaan kouluun (kohtuuden nimissä) jotain  herkkua ja vietämme päivää herkuttelun, yhdessäolon ja joulufiilistelyn merkeissä. Myös lelut ovat tervetulleita kouluun tällöin <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span></p>
<p></p>
<p>Lähestyvän joululoman kunniaksi haluaisin kiittää ihania oppilaitani lukukauden sinnikkyydestä. Materian sijaan ajattelin muistaa jokaista oppilasta hyvän mielen joulukortilla, jossa on kerrottu oppilaasta hyviä asioita ja vahvuuksia. Ensi viikolla keräilen kehuja luokkakavereilta ja mietin niitä itse. Lisäksi toivoisin myös teidän huoltajien osallistuvan kehumiseen! Toivoisin siis mahdollisimman monilta teistä vastausviestiä, jossa on muutama asia, mistä olette lapsessanne ylpeitä ja mistä haluatte häntä kehua ja kiittää. <br>
Toivottavasti toiveeni ei jäänyt nyt hirveän epäselväksi! Lisätietoja ja tarkennuksia annan mielelläni. Vastauksia toivon ensi keskiviikkoon 21.12. mennessä, jotta saan kortit valmiiksi torstaihin mennessä. Pidetään kehut vielä salaisuutena lapsille <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span></p>
<p>Jouluterkuin, <br>
Elina</p>
<p></p>
