Arviointikeskusteluun pohjaksi vieraiden kielten osalta
Halpin Leila (LeilaH)
Piilotettu
2020-01-07 14.57

<p>Hyvät luokanopettajat ja vanhemmat/huoltajat,</p>
<p>ja ihan ensiksi hyvää alkanutta uutta vuotta 2020!</p>
<p>Olen luonut Wilmaan "kokeen" vuoden 2019 viimeisille päiville, jonka tuloksista näkyy arvosana 5. ja 6. lk:lla / kirjainarvointi 4. lk:lla, jonka oppilas saisi, jos nyt olisi saanut joulu-/välitodistuksen.&nbsp; Samassa dokumentissa lisäksi sanallinen palaute tukemaan arvosanaa jokaiselle oppilaalle erikseen. <strong>Luokanopettaja</strong> voi käyttää tätä alkuvuoden arviointikeskustelussa kunkin oppilaan ja hänen huoltajansa kanssa.&nbsp; Kokeiden arvosanojen keskiarvot löytyvät myös Wilmasta.</p>
<p>Pidän itse vuoden ensimmäisellä kielten tunnilla myös lyhyen tavoitekeskustelun jokaisen oppilaan kanssa ja kerron sekä arvosanan että suullisen palautteen hänelle itselleen myös siinä tilanteessa.&nbsp; Julkaisen arvosanat Wilmaan sitä mukaa, kun olen pitänyt henk.koht. keskustelut kunkin luokan kanssa.</p>
<p>Ystävällisin terveisin, Leila Halpin, kieltenope (en, sa, ru)</p>
<p>&nbsp;</p>
