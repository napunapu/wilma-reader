Hansakallion KKY tiedottaa: Kesäkukkamyynti ja Kesäfestarit
Väyrynen Sirpa (SirpaV)
Piilotettu
2016-05-13 13.08

<p>Hei,
</p>
<p>oppilaiden reppuihin on tänään jaettu jo<b> kesäkukkien tilauslomake</b>. Myynnissä on siis laadukkaita Huiskulan kukkia. Tilauslomake palautetaan oppilaan luokanopettajalle keskiviikkona 18.05.2016 mennessä.Kukkien nouto on 1.6. klo 16-18 koululta.</p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">KKY järjestää k</span></span></span><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">e 1.6.2016 <b>kesäfestarit Hansakallion koulun oppilaille</b>. Tilaisuudessa on discoilua, buffaa ja yllätysesiintyjä. Mikäli lapsi tarvitsee jonkun tutun mukaansa festareille, on hänen oltava lapsen huoltaja.</span></span></span><br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">Tilaisuuden ajankohta:</span></span></span> <br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">1.-3. lk klo 18-19:30</span></span></span><br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">4.-6. lk klo 18-20:30</span></span></span><br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri"> </span></span></span><br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Calibri">Tarvitsemme 10 hlö valvojiksi kumpaankin vuoroon, ensimmäinen vuoro klo 17:30-19:30 ja toinen klo 19-21, loppusiivous. </span></span></span><span style="font-size:12px"><span style="font-family:Calibri,sans-serif"><span style="color:#000000">Ilmoittautumiset KKY:n  sähköpostiin <a href="mailto:hansakallionvanhemmat@gmail.com">hansakallionvanhemmat@gmail.com</a> tai Facebook sivuille 25.5 mennessä. Jos emme saa riittävästi valvojia ennakkoilmoittautumiseen, täytyy festarit perua.</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:12px"><span style="font-family:Arial">Tervetuloa mukaan toimintaan!</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:12px"><span style="font-family:Arial">Hansakallion KKY:n puolesta,</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:12px"><span style="font-family:Arial">Sirpa Väyrynen</span></span></span><br>
<span style="color:#000000"><span style="font-size:12px"><span style="font-family:Arial">apulaisrehtori</span></span></span></p>
<p></p>
