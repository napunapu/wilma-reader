Koulun bändi hakee uusia jäseniä!
Hietaranta Veera (VeeraR)
Piilotettu
2018-01-15 14.48

<p>Hei kaikki nelosten ja vitosten huoltajat!</p>
<p>Koulun bändi hakee muutamia uusia jäseniä. Tällä hetkellä bändissä on jäseniä kolmoselta, neloselta ja kutoselta, kaikki tyttöjä, mutta myös kaikki pojat ovat enemmän kuin tervetulleita! Bändi harjoittelee maanantaisin koulun musaluokassa klo 14.15-15.15.</p>
<p>Paikkoja on vapaana seuraavasti:</p>
<p>rummut (1)</p>
<p>koskettimet (1)</p>
<p>basso (1)</p>
<p>kitara (1-2)</p>
<p>Koskettimissa jatkaa ainakin vielä tämän kevään vanha soittaja, joten uusi soittaja saa rauhassa harjoitella soittoa ja vähitellen ottaa bändissä enemmän tilaa. Bassossa on myös jo ennestään yksi soittaja.&nbsp;</p>
<p>Aikaisempi soittokokemus on eduksi, mutta se ei ole välttämätöntä, jos innostus on kova ja on valmis harjoittelemaan. Rytmitaju ja yleinen musikaalisuus on myös eduksi.</p>
<p>Voit ilmoittaa lapsesi kiinnostuksesta minulle vastaamalla tähän viestiin viimeistään perjantaina 19.1. Tiettyä soitinta ei tässä vaiheessa voida luvata. Soittimet jaetaan uusien jäsenten kesken bänditreeneissä. On hyvä jutella lapsen kanssa jo etukäteen, ettei hän välttämättä saa sitä soitinta, mitä toivoo.</p>
<p>Jos halukkaita bändin aloittajia on enemmän kuin soittimia on tarjolla, täytetään paikat ilmoittautumisjärjestyksessä.</p>
<p>Bänditerkuin,</p>
<p>Veera Raivio ja bändiläiset</p>
<p>&nbsp;</p>
