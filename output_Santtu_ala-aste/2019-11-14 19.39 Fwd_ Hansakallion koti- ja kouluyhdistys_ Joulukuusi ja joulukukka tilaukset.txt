Fwd: Hansakallion koti- ja kouluyhdistys: Joulukuusi ja joulukukka tilaukset
Väyrynen Sirpa (SirpaV)
Piilotettu
2019-11-14 19.39

<p><strong>Hansakallion koti- ja kouluyhdistys: Joulukuusi ja joulukukka tilaukset</strong></p>
<p>Hansakallion Koti- ja kouluyhdistys myy jälleen laadukkaita joulukuusia ja joulukukkia!&nbsp;<br>
Keräämillämme varoilla autamme ja ilahdutamme lapsiamme erilaisten tapahtumien muodossa sekä elävöitämme koulun arkea ja kustannamme koululle tarvikkeita.&nbsp;</p>
<p><u><strong>Joulukuuset</strong></u><br>
Myymämme joulukuuset ovat 1. luokan kasvatettuja, suomalaisia kuusia.&nbsp;<br>
Kuusia toimitetaan kahta kokoluokkaa eli n. 2,1-2,4 m (normaali) ja n. 2,7-3 m (iso). Kokoluokat ovat ohjeellisia.&nbsp;Hinta 50€ koosta riippumatta.<br>
Mieleisen kuusen saa valita paikan päällä. Kuuset verkotetaan kotiinkuljetusta varten.<br>
<br>
Tilausten oltava tallennettuna viimeistään <u>04.12.2019</u>! Joulukuusien jako Hansakallion koulun pihalla: 19.12.2019 Klo: 17:00-19:00. Maksutapana käteinen.</p>
<p>Voit tehdä joulukuusitilauksen <a href="https://docs.google.com/forms/d/e/1FAIpQLSfF7z5738FllcnRODQvzzkDFyU4TgozVdxA_2mFVtPT9KrMTg/viewform">tällä lomakkeella</a>. Tilaus on sitova!</p>
<p><br>
<u><strong>Joulukukat</strong></u><br>
Myynnissä on kotimaisia Amarylliksiä, joulutähtiä, tulilatvoja ja sypressejä.<br>
Tilausten oltava tallennettuna viimeistään <u>04.12.2019</u>.&nbsp;Kukkien noutopäivä on:<strong> </strong>14.12.2019 klo: 9:00-12:00 Hansakallion koululta.&nbsp;<br>
Samaan aikaan on myös Hansakallion koulun joulumyyjäiset sekä joulujuhla.</p>
<p>Maksutapana käteinen tai MobilePay.<br>
Voit tehdä joulukukkatilauksen <a href="https://docs.google.com/forms/d/e/1FAIpQLSfZl5svQ5mzcC42mGl6qTJ6PEn7t2yyzSG_IzfAf0oV2zo9vg/viewform">tällä lomakkeella</a>. Tilaus on sitova!<br>
&nbsp;</p>
<p>Iloista joulun odotusta!<br>
Hansallion Koti- ja kouluyhdistys</p>
