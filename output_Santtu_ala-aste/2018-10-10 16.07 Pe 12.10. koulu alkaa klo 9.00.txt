Pe 12.10. koulu alkaa klo 9.00
Aittola Johanna (JohannaA)
Piilotettu
2018-10-10 16.07

<p>Hei!</p>
<p>&nbsp;</p>
<p>Perjantaina 12.10. koulu alkaa kaikilla poikkeuksellisesti jo klo 9.00, koska silloin on Kino Hansis. Tästä johtuen B-ryhmällä loppuu koulu tuolloin perjantaina jo klo 13.15 ja A-ryhmä saa puolestaan tunnin hyvityksenä loman jälkeen keskiviikkona 24.10., jolloin heillä alkaa koulu vasta klo 9.00.</p>
<p>Eli muutokset vielä kootusti:</p>
<p>pe 12.10. koulu alkaa kaikilla 9.00</p>
<p>pe 12.10. B-ryhmällä loppuu koulu 13.15</p>
<p>ke 24.10. A-ryhmällä alkaa koulu 9.00</p>
<p>Terveisin Johanna</p>
<p>&nbsp;</p>
