Reppupostia tulossa
Helotie Anni (HeAn)
Piilotettu
2015-04-02 10.03

<p>Hei!<br>
Laitan tänään lasten mukana kotiin kihomatotiedotteen. Pääsiäisloman kunniaksi lapset eivät saa tänään läksyä, mutta teille vanhemmille annan vähän ikävämmän kotitehtävän, eli jokainen lapsi pitäisi kihomatotartunnan varalta kunnolla tarkastaa. Mikäli tartuntoja löytyy, niistä on hyvä ilmoittaa kouluterveydenhoitajalle. Tiedotteessa on ohjeet tarkastuksen tekemiseen sekä mahdolliseen hoitoon.
</p>
<p>Tästäkin huolimatta leppoisaa pääsiäislomaa toivotellen, Anni-ope</p>
<p></p>
