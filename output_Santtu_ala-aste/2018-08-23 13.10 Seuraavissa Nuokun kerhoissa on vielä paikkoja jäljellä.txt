Seuraavissa Nuokun kerhoissa on vielä paikkoja jäljellä
Smolander Maritta (MarittaS)
Piilotettu
2018-08-23 13.10

<p>Hei!</p>
<p>&nbsp;</p>
<p>Ilmoittautua voi&nbsp;sähköpostilla. Laita&nbsp;viestiin kerho, johon lapsi ilmoittautuu, lapsen ja huoltajan nimi, lapsen syntymäaika sekä huoltajan sähköpostiosoite ja puhelinnumero. Kokkikerhoihin ilmoittautuessa lisää viestiin myös tiedot lapsen mahdollisista ruoka-aineallergioista ja erikoisruokavalioista. Perjantain puuhakerhoon ei tarvita etukäteisilmoittautumista. Sähköposti ilmoittautumiset osoitteeseen&nbsp;<a href="#OnlyHTTPAndHTTPSAllowed"><strong>kauklahti.nuoriso@espoo.fi</strong></a>.</p>
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
<p><strong>Kauklahden nuorisotilan kerhot syksyllä 2018</strong></p>
<p>&nbsp;</p>
<p><u>MAANANTAI&nbsp;</u></p>
<p>Kokkikerho (max 6 osallistujaa)&nbsp;<br>
5.-6.-luokkalaisille maanantaisin klo 14.30-16.00 Kauklahden nuorisotilalla.<br>
Kokkikerhossa tehdään maittavia, niin suolaisia kuin makeitakin ruokia lasten toiveiden mukaisesti.</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 1 paikka</p>
<p><br>
<br>
<u>KESKIVIIKKO</u></p>
<p>Digikerho (max 5 osallistujaa)&nbsp;<br>
6.-7.-luokkalaisille keskiviikkoisin klo 16-17.30 Kauklahden nuorisotilalla.<br>
Digikerhossa puuhataan digitaalisessa maailmassa mm. tietokoneiden, internetin, somen ja pelien parissa.</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Huom 6. lk alkaen , 1 paikka</p>
<p>&nbsp;</p>
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>
<p><u>TORSTAI</u></p>
<p>Tyttökerho (max 6 osallistujaa)&nbsp;<br>
3.-4.-luokkalaisille torstaisin klo 13.15-14.45 Kauklahden nuorisotilalla.<br>
Kerhossa tehdään joka viikko erilaisia asioita, kuten askarteluja, kokkailuja, pelailua ja kaikenmoista kivaa tytöille.</p>
<p>&nbsp;</p>
<p>Askartelukerho (max 8 osallistujaa)&nbsp;<br>
4.-6.-luokkalaisille torstaisin klo 15.15-16.45 Kauklahden nuorisotilalla.<br>
Kerhossa tehdään kaikenmoista askartelua eri materiaaleilla ja menetelmillä.</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><u>PERJANTAI</u></p>
<p>Puuhakerho (max 20 osallistujaa)&nbsp;<br>
3.-5.-luokkalaisille perjantaisin klo 14.00-15.30 Kauklahden nuorisotilalla.<br>
Kerhossa tehdään joka viikko erilaisia asioita, kuten askarteluja, kokkailuja, pelailua ja kaikenmoista kivaa. HUOM! Kerho alkaa jo 17.8.</p>
<p>Tilaa on, kerho on jo alkanut, ei tarvitse ilmoittautua</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin</p>
<p>&nbsp;</p>
<p>Miira Nygren, Sari Salmela ja Juha Laakso</p>
<p>Nuorisonohjaajat</p>
<p>Kauklahden nuorisotila, Espoon Nuorisopalvelut</p>
<p>Käyntiosoite: Hansakallio 2, 02780 Espoo</p>
<p>PL 3582, 02070 ESPOON KAUPUNKI</p>
<p>Puh. 046-8771356 tai 043-8266668</p>
<p><a href="#OnlyHTTPAndHTTPSAllowed">kauklahti.nuoriso@espoo.fi</a></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
