Valinnaisainelinkki uudestaan 4A:lle
Sorvari Heidi (HeidiS)
Piilotettu
2018-04-18 08.55

<p>Hei 4A-luokan huoltajat!</p>
<p>Eilisen ilmoittautumislomakkeen valikosta puuttui 4A-luokka, pahoitteluni! Tässä uusi linkki 4A:n valinnaisainevalintoja varten. Eli tehkää valinnat tämän kautta.&nbsp;</p>
<p>Kiitos!</p>
<p><a href="https://goo.gl/forms/G60Ep7bkgIXL7ghH2"><span style="color:#333333;font-family:sans-serif,Arial,Verdana,;">https://goo.gl/forms/G60Ep7bkgIXL7ghH2</span></a></p>
<p>T. Heidi Sorvari</p>
