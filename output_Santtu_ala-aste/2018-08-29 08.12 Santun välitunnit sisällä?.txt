Santun välitunnit sisällä?
Korpela Panu (Santtu Tarvainen, 6E/2020)
Tarvainen Outi (Santtu Tarvainen, 6E/2020), Mannersalo Annu (AnnuM)
2018-08-29 08.12

<p>Hei,</p>
<p>Santtu oli eilen välitunnilla mennyt pallokentälle ja saanut vahinkopallon naamaansa. Toisella välitunnilla oli taas kaveri hypännyt selkään. Taitaa olla sen verran vilkas poika, että olisiko mahdollista pitää hänet välitunneilla sisällä -- tähystysleikkaus oli kuitenkin aivojen keskelle asti ja kirurgi käski välttämään kaikenlaisia tärähdyksiä?</p>
<p>Terv, Panu</p>
