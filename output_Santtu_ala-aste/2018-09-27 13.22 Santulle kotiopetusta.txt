Santulle kotiopetusta
Mannersalo Annu (AnnuM)
Piilotettu
2018-09-27 13.22

<p><span style="font-size:14px;">Hei!</span></p>
<p><span style="font-size:14px;">Ensi viikolla voisin tulla Santun kanssa tekemään tehtäviä. Voisimme aloitta matematiikasta ja äidinkielestä sekä yhteiskuntaopista soveltuvin osin. Yhteiskuntaoppi on kuitenkin melko selkeää opiskeltavaa Santulle myös itsekseen, joten sitä vähän vähemmän huomioiden.</span></p>
<p><span style="font-size:14px;">Milloin teille olisi sopivin aika?&nbsp;</span></p>
<p><span style="font-size:14px;">Maanantaina pääsen neljän jälkeen ( tai mahdollisesti jo hieman aiemmin, palaverista riippuen),&nbsp;&nbsp;tiistaina &nbsp;kolmen jälkeen. Keskiviikkona onnistuisi vasta viiden aikaan, joten se lienee huono ajankohta. Torstaina olisi mahdollista lähteä koululta kahden jälkeen. Tämä aikataulu tietysti Santun vointi huomioiden, eli tarvittaessa sitten vaihdamme aikoja.</span></p>
<p><span style="font-size:14px;">ystävällisin terveisin</span></p>
<p><span style="font-size:14px;">Annu</span></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
