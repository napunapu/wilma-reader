Muistutus vanhempainillasta 18.4 klo 18-19.30
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2018-04-16 11.20

<p>Arvoisat huoltajat</p>
<p>Järjestämme Hansakallion koulun vanhempainillan 18.4 klo 18. Sitä ennen on ruokalassa buffet avoinna klo 17.30 (5E -luokka). Illan ohjelma: aluksi rehtorin avaus ajankohtaisista asioista&nbsp;ja johdanto illan aiheisiin. Esittelemme koulun toimintaa&nbsp;neljän&nbsp;eri teeman kautta; laaja-alaiset taidot ja oppimisalueet, käyttäytymisen kriteerit, digi-classroom ja koodaus sekä kielten opetus. Esittely tapahtuu koulun eri tiloissa ruokalatilan molemmin puolin. Toivomme, että saavutte paikalle runsain joukoin&nbsp;kuulemaan, katsomaan ja pohtimaan koulumme&nbsp;arjen opetus- ja oppimistoimintaa.</p>
<p>Koulun välittömässä läheisyydessä rakennetaan uutta kiinteistöä. Tämä rajaa entisestään parkkipaikkavaihtoehtoja. Toivomme, että ottaisitte liikkuessanne tämän huomioon.</p>
<p>Tervetuloa!</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki</p>
