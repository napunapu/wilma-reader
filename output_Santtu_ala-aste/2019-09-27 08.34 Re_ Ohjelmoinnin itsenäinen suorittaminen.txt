Re: Ohjelmoinnin itsenäinen suorittaminen
Korpela Panu (Santtu Tarvainen, 6E/2020)
Tarvainen Outi (Santtu Tarvainen, 6E/2020), Särkkä Hanna (HannaS)
2019-09-27 08.34

<p><span style="font-size:12px;">Hei,</span></p>
<p><span style="font-size:12px;">tässä Santun tekstit tehtävään 1:</span></p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">Tiivistä kustakin osasta oppimasi asia muutamaan virkkeeseen alle.</span></span></p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">1</span> </span>Jokainen robotti ohjelmoidaan ja ohje pitää pilkkoa pieniin palasiin ja niitten pitää olla mahdollisimman yksin kertasia</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">2 </span></span>ohjelmoida voi symbooleilla ja äänellä</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">3 </span></span>Ohjelmoinnissa käytetään myös kuvake kieltä</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">4 </span></span>joskus ohjelmoinnissa toistuu käskyt uudestaan ja uudestaan</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">5 </span></span>sarjoja käytetään ohjelmoinnissa</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">6 </span></span>ehtolauseiden avulla voi tehdä päätöksiä</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">7 </span></span>Roboteilla on sensorit</p>
<p><span style="font-size:12px;"><span style="color:#000000;font-family:Arial;">8 </span></span>Kaikki mitä robotit tekee ne on ohjelmoitu</p>
<p><span style="font-size:12px;">Terv, Panu</span></p>
