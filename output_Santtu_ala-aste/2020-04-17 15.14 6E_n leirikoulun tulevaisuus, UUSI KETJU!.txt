6E:n leirikoulun tulevaisuus, UUSI KETJU!
Lukkarinen Elina (ElinaL)
Akimova Lidia (Kirill Akimov, 6E/2020), AKIMOV STANDBY78 (Kirill Akimov, 6E/2020), Alaoui Juska (Youssef Alaoui Abdallaoui Slimani, 6E/2020), Alaoui Abdallaoui Slimani HICHAM (Youssef Alaoui Abdallaoui Slimani, 6E/2020), Hosio Marita (Maksi Hosio, 6E/2020) … Näytä kaikki vastaanottajat
2020-04-17 15.14

<p>Hei,&nbsp;</p>
<p>Loin uuden ketjun leirikouluun liittyvää keskustelua ja tiedottamista varten, sillä vanha ketju oli venähtänyt todella pitkäksi. <strong>Tässä ketjussa ovat&nbsp;siis mukana kaikki luokan huoltajat&nbsp;sekä Elina-ope.&nbsp;<u>Ethän&nbsp;kirjoita ketjuun lapsesi henkilökohtaisia asioita!&nbsp;</u></strong></p>
<p>&nbsp;</p>
<p>Sain juuri tänään tiedon&nbsp;Vierumäeltä, että kaikki Vierumäen palvelut ovat suljettuina 31.5.2020 saakka. Tämä tarkoittaa sitä, että leirikoulu toukokuulta 2020 perutaan. Tämä on todella harmillista, mutta oli tietysti odotettavissa! Nyt suunnataan siis katseita tulevaisuuteen ja lähdetään miettimään suunnitelma B:tä.&nbsp;</p>
<p>&nbsp;</p>
<p>Itse pohdin seuraavaa vaihtoehtoa:</p>
<p>Leirikoulu järjestettäisiin&nbsp;ensi syyslukukauden alussa ns. luokkakokous-hengessä. Tätä vaihtoehtoa olen kysynyt alustavasti Vierumäeltä ja heille sopii leirikoulun siirtäminen elokuun puolenvälin paikkeille (ei kuitenkaan yläkoulun ensimmäisille päiville). Myös minun esimieheni lupasi joustaa työtehtävissäni, jos&nbsp;leirikoulu toteutuu. Jos tähän ratkaisuun päädytään, niin olen yhteydessä yläkoulun rehtoriin kaikkien oppilaiden poissaolon osalta. Poissaoloa koulusta tulisi todennäköisesti 1-2 koulupäivää, joiden ajalta koulutehtävät tietysti tulisi hoitaa.&nbsp;</p>
<p>&nbsp;</p>
<p>Nyt toivoisin siis keskustelua ja ideoita leirikoulun tulevaisuuden osalta! Tehdään päätökset sitten niiden pohjalta!&nbsp;</p>
<p>Terveisin Elina&nbsp;</p>
