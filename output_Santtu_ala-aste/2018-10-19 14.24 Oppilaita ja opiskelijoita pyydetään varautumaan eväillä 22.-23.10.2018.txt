Oppilaita ja opiskelijoita pyydetään varautumaan eväillä 22.-23.10.2018
Ylläpitäjä Wilma (W.Y)
Piilotettu
2018-10-19 14.24

<p>Huoltajia pyydetään varaamaan eväät oppilaille ja opiskelijoille 22.-23.10.2018. &nbsp;JHL:n julistaman poliittisen työnseisauksen vuoksi. Ateriapalveluja kouluissa ei voida taata.</p>
<p>Tämä johtuu siitä, että varmaa tietoa henkilöstötilanteesta ei tällä hetkellä ole. Myöskään JHL:n julistaman työnseisauksen kohteissa työskentelevät PAM:in jäsenet eivät tee lakonalaisia töitä. (Tilanne on muuttunut edellisen viestin lähettämisen jälkeen.)</p>
<p>Oppilaiden ja opiskelijoiden omia eväitä ei voida säilyttää kylmässä, eikä niitä voida lämmittää.</p>
<p>Iltapäiväkerhojen ja esikoulujen aamu- ja välipaloja ja koulujen välipalamyyntiä ei myöskään pystytä toimittamaan.&nbsp;Siksi pyydämme vanhempia varautumaan ateriapalveluiden rajoituksiin varaamalla lapselle myös mukaan tarvittavat aamu- ja/tai välipalat sekä erityisruokavalioateriat.</p>
<p>Tilanne tulee aiheuttamaan muutoksia koko viikon ateriatarjontaan.</p>
<p>Tilannetta voi seurata <a href="https://www.espoo.fi/fi-FI/Espoon_kaupunki/Organisaatio_ja_yhteystiedot/Espookonserni/Konsernihallinto/Ruokapalvelut">Ruokapalvelujen sivulla.</a></p>
<p>Lisätietoja koulun rehtorilta.</p>
