6. lk B-ruotsi etäviikko nro 6 tehtävät
Halpin Leila (LeilaH)
Piilotettu
2020-04-21 16.52

<p>God kväll alla!</p>
<p>Tässä tulevat viikon nro 6 ohjeet!</p>
<p><strong>Pakolliset tehtävät:</strong></p>
<p><span style="color:#b22222;"><strong>Käy kipin kapin tekemässä KOE kpl 4 classroomissa!&nbsp; Se poistuu sieltä viimeistään huomenna ennen tunnin alkua!</strong></span></p>
<p>Tule liveopetukseen huomenna&nbsp;ke 22.4. klo 11.</p>
<p>Tee viikon nro 6 tehtävät vihkoon&nbsp;classroomissa. Tai tulosta ne ja tee paperille.</p>
<p><br>
<strong>Vapaaehtoiset lisätehtävät:</strong></p>
<p>Luo itsellesi profiili ja pääset ilmaiseksi harjoittelemaan ruotsia (ja myös muita kieliä)&nbsp;<a href="https://www.baamboozle.com/register">https://www.baamboozle.com/register</a>. Kun olet luonut profiilin (sign up for free) ja käynyt klikkaamassa linkkiä sähköpostissasi, kirjaudut luomallasi tunnuksella sisään. Sen jälkeen klikkaat vasemmalta ylhäältä "games", sitten "search games", minkä jälkeen kirjoitat esim. Megafon 1&nbsp;hakukenttään ja saat valtavasti hauskoja harjoitulsia! Lycka till!</p>
<p>Ha&nbsp;en fin vecka!&nbsp; Vi ses nästä gången i morgon&nbsp;kl 11!</p>
<p>Hälsningar från Leila Lärare</p>
