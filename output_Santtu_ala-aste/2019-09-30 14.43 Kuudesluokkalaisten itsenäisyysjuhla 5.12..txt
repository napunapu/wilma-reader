Kuudesluokkalaisten itsenäisyysjuhla 5.12.
Hamari Sallamari (SallamariH)
Piilotettu
2019-09-30 14.43

<p>Hyvät kuudesluokkalaisen vanhemmat,</p>
<p>Espoon kuudesluokkalaiset juhlivat yhdessä itsenäistä Suomea Espoon Opinmäessä <strong>torstaina 5.12.2019</strong>.</p>
<p>Lapsenne saa koulultaan marraskuun alussa virallisen kutsun kuudesluokkaisten itsenäisyyspäiväjuhlaan. Juhlia järjestetään samana päivänä kolme ja ne ovat samansisältöisiä. Juhlista viimeinen päättyy n. klo 15.30. Kouluille ilmoitetaan mihin juhlaan koulun oppilaat osallistuvat.</p>
<p>Itsenäisyysjuhlan aluksi kohotetaan alkumalja Suomen kunniaksi ja tarjotaan pieni cocktail-pala. Alkumaljan jälkeen seuraa kaupungin ja sivistystoimen edustajien kättely sekä tanssia. Musiikista vastaavat Puolustusvoimien varusmiessoittokunta ja varusmiessoittokunnan aliupseerikurssin bändilinja. Juhla on tarkoitettu oppilaille, opettajille sekä kutsuvieraille, ja osallistujilta toivotaan siistiä pukeutumista. Lounaan oppilaat syövät kouluilla ennen tai jälkeen juhlan.</p>
<p>Syksyn aikana oppilaat harjoittelevat oman opettajan johdolla juhlassa tanssittavat tanssit (kikapo ja valssi) sekä Maamme-laulun. Juhlavalmisteluiden ohessa luokissa keskustellaan myös käyttäytymisestä juhlavassa tilaisuudessa.</p>
<p>Itsenäisyysjuhlassa kuvataan ja kuvia käytetään mediassa. Opettajat huolehtivat, että kuvausluvat ovat kunnossa sekä ovat tietoisia oppilaista, joilla kuvauslupaa ei ole. </p>
<p>&nbsp;</p>
<p>Voitte tutustua itsenäisyyspäiväjuhlaan oheisen Youtube-linkin kautta: <a href="https://www.youtube.com/watch?v=FQI4yxUItKI&amp;list=PLsc1N9SexnJVo4FSdfEOilcq5hpzZhVBf">https://www.youtube.com/watch?v=FQI4yxUItKI&amp;list=PLsc1N9SexnJVo4FSdfEOilcq5hpzZhVBf</a></p>
<p>&nbsp;</p>
<p>Itsenäisyysjuhlan järjestävät yhteistyössä Espoon Sivistystoimi, Sunan koulu, Espoo International School, Olarin koulu, Opinmäki ja Puolustusvoimien varusmiessoittokunta.</p>
