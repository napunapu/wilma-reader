School action day-retki 8.9.2015!
Lukkarinen Elina (ElinaL)
Piilotettu
2015-09-03 17.54

<p>Hei vielä, 
</p>
<p>Ensi viikolla tiistaina 8.9 suuntaamme 2A-luokan kanssa School action day-liikuntatapahtumaan, joka kokoaa lähes kaikista pääkaupunkiseudun kouluista lapsia liikkumaan Helsinkiin Sonera Stadionille. Lisätietoa tapahtumasta löytyy esimerkiksi osoitteesta www.liikkuvakoulu.fi/tapahtumat. Hansakallion koululta tapahtumaan osallistuu lisäksi kaksi 4.luokkaa.</p>
<p>Tiistaina koulu alkaa jo klo 8.00 ja tilausbussi kohti Helsinkiä lähtee klo 8.20. Takaisin kohti koulua lähdemme Helsingistä klo 14.10 ja koulu päättyy siis noin kello 15.00. Päivästä tulee pitkä, mutta varmasti myös hauska ja ikimuistoinen <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span> Otamme koululta oppilaille mukaan eväät, mutta varaattehan lapsillenne mukaan riittävästi terveellistä evästä ja juotavaa. Pienen herkun saa myös ottaa mukaan. Liikuntatapahtumassa liikumme siis ulkona, ulkoliikuntavaatetus ja lenkkarit ovat siis hyvä varustus. Myös sateenpitävä takki voi olla tarpeellinen. Reppuun kannattaa varata mukaan vaihtovaatteet, muuten ylimääräiset tavarat kannattaa jättää kotiin. </p>
<p>Liikuntatapahtuman vuoksi lukujärjestykseen tulee muutoksia ensi viikolle. Kirjaathan muutokset muistiin! </p>
<p><b>- keskiviikkona 9.9.2015 kaikilla alkaa koulu kello 10.15 ja päättyy kello 13.15. </b><br>
<b>- torstaina 10.9.2015 kaikilla alkaa koulu kello 9.00</b><br>
<b>- perjantaina 11.9.2015 kaikilla alkaa koulu kello 9.00</b></p>
<p>Mahdolliset kysymykset tapahtumasta voi lähettää paluuviestinä! Olen itse todella innoissani tapahtumasta ja uskon että meillä tulee olemaan todella hauskaa! <span class="emoticon" data-file="cool" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/cool.png&quot;);"></span></p>
<p>Liikuntaterveisin, <br>
Elina-ope</p>
<p></p>
