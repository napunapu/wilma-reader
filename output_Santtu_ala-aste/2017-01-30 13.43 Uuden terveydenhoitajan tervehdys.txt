Uuden terveydenhoitajan tervehdys
Varjoinen Outimaria (O.V)
Piilotettu
2017-01-30 13.43

<p>Hei!</p>
<p>Aloitin tässä kuussa Hansakallion koulun terveydenhoitajana. Toimin Arja Auvisen sijaisena 30.6.2017 asti. Olen pääsääntöisesti töissä ma-to, joskus myös poikkeuksellisesti ma-pe tai ma-ke.</p>
<p>Minut tavoittaa parhaiten näin Wilma-viestillä.&nbsp;Pyrin vastaamaan kaikkiin viesteihin mahdollisimman nopeasti.&nbsp;Osan kanssa olenkin ollut jo yhteyksissä ja muillekin tiedoksi, että aina saa kysyä jos joku mietityttää.</p>
<p>Laitan kevään mittaan lääkäriaikoja Wilma-viestillä vielä lopuille 5-luokkalaisille ja&nbsp;myös kaikille 1-luokkalaisille. Muihin olen yhteydessä tarvittaessa joko Wilman kautta tai puhelimitse.</p>
<p>Mukavaa alkanutta vuotta!</p>
<p>&nbsp;</p>
<p>Ystävällisin terveisin,</p>
<p>Terveydenhoitaja Outimaria Varjoinen&nbsp;</p>
<p>p. 046-8771453</p>
