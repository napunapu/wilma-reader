yhteystiedot Wilmassa
Kass Veronika (V.K)
Piilotettu
2020-03-12 15.59

<p>Hei,</p>
<p>&nbsp;</p>
<p>Pyydän teitä tarkistamaan ja päivittämään yhteystietojanne Wilmassa (nimi, osoite, puhelinnumero, sähköpostiosoite).</p>
<p>&nbsp;</p>
<p>Ystävällisin terveisin,</p>
<p>Veronika Kass</p>
<p>koulusihteeri</p>
