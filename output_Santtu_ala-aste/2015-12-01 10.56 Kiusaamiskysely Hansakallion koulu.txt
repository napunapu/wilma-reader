Kiusaamiskysely Hansakallion koulu
Häkkinen Jonna (J.H)
Piilotettu
2015-12-01 10.56

<p>Hei,
</p>
<p>Hansakallion koulussa tehdään syyslukukaudella kiusaamiskysely, jonka tarkoituksena on kartoittaa tämän hetkistä tilannetta kiusaamiseen liittyen. Tänä vuonna kysely toteutetaan sähköisenä. Opettajat ovat käyneet asiaa läpi luokkansa kanssa.</p>
<p>Tarkoituksena on, että 1. ja 2. luokkalaiset täyttävät kyselyn vanhempiensa kanssa. 3. luokkalaiset ja sitä vanhemmat voivat täyttää kyselyn sovitusti joko yksin tai vanhempiensa kanssa.</p>
<p>Toivomme, että vastaatte kyselyyn <b>15.12.2015</b> mennessä. Kyselyyn voi vastata alla olevan linkin kautta.</p>
<p>Kyselyn tuloksia käsitellään luottamuksellisesti koulun yhteisöllisen oppilashuoltoryhmän, luokanopettajan ja koulun KiVa-tiimin kanssa.</p>
<p><a href="https://my.surveypal.com/kiusaamiskysely-Hansakallio" target="_blank" rel="noopener" class="outbound">https://my.surveypal.com/kiusaamiskysely-Hansakallio</a></p>
<p>Kiitos!</p>
<p>Ystävällisin terveisin</p>
<p>Jonna Häkkinen</p>
<p></p>
