Englannin kerho
Lukkarinen Elina (ElinaL)
Piilotettu
2015-09-23 16.21

<p>Hei kotiväki,
</p>
<p>enkkukerho starttasi viime viikolla ja jatkuu huomenna. Kerho on <b>torstaisin klo 13.15-14.15 </b>(huomaa muuttunut kellonaika!)<br>
Kerhon alussa oppilailla on mahdollisuus syödä omia eväitä. </p>
<p>Terveisin,<br>
Elina-ope ja Helka-ope</p>
<p></p>
