Luokan tunnus muuttuu
Väyrynen Sirpa (SirpaV)
Piilotettu
2018-06-11 13.33

<p>Hei,</p>
<p>&nbsp;</p>
<p>lastenne luokan tunnus muuttuu ensi lukuvuoden alusta. Meillä on luokkatasolla olemassa yhdysluokka 4-5A ja lastenne luokka olisi 5A eli luokkatasolla olisi kaksi A-tunnuksellista luokkaa. Tämän vuoksi lastenne luokan tunnus muuttuu ensi lukuvuoden alusta. Elokuussa lapset aloittavat koulun 5E-luokassa.</p>
<p>&nbsp;</p>
<p>Aurinkoista ja rentoa kesälomaa!</p>
<p>&nbsp;</p>
<p>Sirpa Väyrynen</p>
<p>apulaisrehtori</p>
