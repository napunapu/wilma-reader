Yleistä tiedotettavaa
Lukkarinen Elina (ElinaL)
Piilotettu
2015-08-18 15.18

<p>Hei,
</p>
<p>2A-luokan syyslukukausi on pyörähtänyt mainiosti käyntiin. Tässä muutama tärkeä tiedotus teille vanhemmille, sekä muutama tärkeä päivämäärä jotka voitte jo merkitä kalentereihinne.</p>
<p>- Koulukuvaus Hansakallion koulussa järjestetään ti 1.9-to 3.9. Ilmoitan luokkamme kuvauspäivän heti sen varmistuttua.</p>
<p>- Vanhempainilta järjestetään ke 2.9. Toivottavasti tapaan mahdollisimman monet teistä siellä <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span></p>
<p>- Osallistuin viime keväänä luokkamme nimellä arvontaan, jossa jokaisesta pääkaupunkiseudun koululta arvottiin 3 luokkaa osallistumaan suureen koululaisten urheilutapahtumaan. Onni potkaisi meitä arvonnassa ja osallistumme ti 8.9 koko 2A-luokan voimin School Action-day nimiseen koululaisten liikuntatapahtumaan Helsingissä Sonera Stadionilla! Tiedotan tarkemmin tapahtumasta lähempänä aikataulujen varmistuttua. Nyt jo kuitenkin tiedän, että päivä tulee olemaan normaalia koulupäivää pidempi ja retkeltä palaamme koululle vasta n. klo 15.</p>
<p>- Osa oppilaista on kysellyt lupaa tuoda omat kuulosuojaimet kouluun ja tunnilla käytettäväksi kuten viime vuonnakin. Edelleen oppilailla on lupa tuoda omat kuulosuojaimet kouluun ja käyttää niitä itsenäisen työskentelyn aikana. Kuulosuojaimia tilataan myös koulun puolesta kaikkiin luokkiin ja yritän saada tilattua luokkaamme mahdollisimman monet suojaimet, jotta kaikilla oppilailla olisi mahdollisuus niiden käyttämiseen ainakin vuorotellen. </p>
<p></p>
<p>Iloista viikkoa toivotellen,</p>
<p>Elina-ope</p>
<p></p>
