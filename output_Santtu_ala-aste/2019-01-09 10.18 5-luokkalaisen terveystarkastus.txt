5-luokkalaisen terveystarkastus
Auvinen Arja (A.A)
Piilotettu
2019-01-09 10.18

<p>Hyvää alkanutta vuotta 2019!</p>
<p>Lähetän näin wilmaviestitse tiedotteen kevään - 19 kulusta, liittyen koululääkärin vastaanottoaikoihin. Hansakallion koululla lääkäri on tavattavissa vasta huhtikussa, kolmena tiistaipäivänä.</p>
<p>Joten ne lapset, jotka eivät ole vielä käyneet tarkastuksessa voivat saapua em aikana.</p>
<p>Palaan asiaan myöhemmin keväällä, sitten kun ajanvarauskirjat tulevat käytettävään muotoon.</p>
<p>- Arja Auvinen, th</p>
