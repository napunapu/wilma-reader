Tiedotus- ja keskustelutilaisuus Hansakallion koululla 11.10.2017 klo 17.30-18.30
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2017-10-02 10.03

<p><strong>TIEDOTUS- JA KESKUTELUTILAISUUS HANSAKALLION JA ESPOON</strong></p>
<p><strong>YHTEISLYSEON KOULUJEN YHDISTÄMISSUUNNITELMASTA</strong></p>
<p>Espoon opetustoimessa valmistellaan Espoon peruskoulujen ja lukioiden</p>
<p>tilankäyttösuunnitelmaan perustuen esitystä, jonka mukaan Hansakallion koulusta ja</p>
<p>Espoon yhteislyseon koulusta muodostetaan yhtenäinen peruskoulu 1.8.2018 lukien.</p>
<p>Alueen asukkaille ja koulujen huoltajille sekä nuorisovaltuuston jäsenille järjestetään</p>
<p>valmistelussa olevasta esityksestä <strong>tiedotus- ja keskustelutilaisuus Hansakallion</strong></p>
<p><strong>koululla 11.10.2017 klo 17.30-18.30.</strong></p>
<p>Opetus- ja varhaiskasvatuslautakunta käsittelee esitystä yhtenäisen peruskoulun</p>
<p>muodostamisesta kokouksessaan 22.11.2017.</p>
<p>Tervetuloa!</p>
<p>Kaisa Alaviiri Juha Nurmi</p>
<p>Keski- ja Pohjois-Espoon aluepäällikkö Kehittämispäällikkö</p>
<p>Suomenkielinen perusopetus Suunnittelu- ja hallintopalvelut</p>
