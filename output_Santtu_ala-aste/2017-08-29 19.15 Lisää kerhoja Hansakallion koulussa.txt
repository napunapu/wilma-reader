Lisää kerhoja Hansakallion koulussa
Smolander Maritta (MarittaS)
Piilotettu
2017-08-29 19.15

<p><strong><em>WAU kerho</em></strong><em> jatkuu Hansakallion koululla viikolla 36.&nbsp;</em></p>
<p><em>Kerho on perjantaisin kello 14.15-15.15.</em></p>
<p><em>Kerho on suunnattu 2.-4.luokkalalsille.</em></p>
<p><em>Maksuton ja sisältää vakuutuksen.</em></p>
<p><em>Monipuolista liikuntaa &amp; lasten toiveiden mukaisesti.</em></p>
<p><em>Liikuntasalissa tai koulun pihalla/kentällä.</em></p>
<p><em>Lisätiedot ja ilmoittautuminen kerhoon&nbsp;<a href="http://www.wau-ry.fi/">www.wau-ry.fi</a></em></p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p><strong>Westend Indiansin sählykerho eskari - 4.-luokkalaisille</strong> maanantaisin klo 15.30-16.15.</p>
<p>Kerho järjestetään ensimmäisen kerran syyskuussa viikolla 37 (11.–15.9.) ja siitä eteenpäin joka viikko (pois lukien koulun lomat ja muut koulun puolesta tulevat vuoroperuutukset). Kerhon kustannus 200 € / syys+kevätkausi.</p>
<p>&nbsp;</p>
<p>Kaikilla on mahdollisuus kokeilla ryhmän toimintaa viikon 40 loppuun / 8.10.2017. Kerhoon tulee kuitenkin ilmoittautua ennakkoon. Peruuttamisesta tulee ilmoittaa&nbsp;<a href="#OnlyHTTPAndHTTPSAllowed">toimisto@westendindians.fi</a></p>
<p>&nbsp;</p>
<p>Kerhomaksu sisältää kaiken ryhmän toiminnan: ohjauksen, vuorot, seuramaksun, vakuutuksen, sarjamaksut sekä lisäksi Heimon t-paidan sekä yhden seuran harrastetapahtuman. Ryhmien ohjaajina toimivat seuran koulutetut ja kokeneet liikunnanohjaajat.</p>
<p>&nbsp;</p>
<p>Suora ilmoittautumislinkki kerhoon:</p>
<p>&nbsp;</p>
<p><a href="http://indians.fi/ilmoittautuminen/0imvlu98m0dne834stk282414iyjx0pn">http://indians.fi/ilmoittautuminen/0imvlu98m0dne834stk282414iyjx0pn</a></p>
<p>&nbsp;</p>
