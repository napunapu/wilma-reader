Hansakallion koulun koti-ja kouluyhdistyksen vuosikokous
Väyrynen Sirpa (SirpaV)
Piilotettu
2015-09-21 14.35

<p><b>Tervetuloa kaikki huoltajat Hansakallion koulun koti-ja kouluyhdistyksen vuosikokoukseen tiistaina 22.9.2015 klo 17.30 koulun ruokalaan!</b>
</p>
<p>Hansakallion koulun koti-ja kouluyhdistyksen puolesta</p>
<p>Sirpa Väyrynen</p>
<p></p>
