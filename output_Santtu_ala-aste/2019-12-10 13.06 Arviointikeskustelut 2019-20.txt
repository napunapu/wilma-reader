Arviointikeskustelut 2019-20
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2019-12-10 13.06

<p>Arvoisat huoltajat,</p>
<p>&nbsp;</p>
<p>Väliarviointi toteutetaan Espoossa luokilla 1-6 arviointikeskusteluna tammi-helmikuun aikana.&nbsp;Yksittäisen luokan kohdalla, jos opettaja vaihtuu vuodenvaihteessa, arviointikeskustelun takaraja voi joustaa. Läsnä ovat siis oppilas, huoltaja/t ja opettaja/t. Arviointikeskustelussa kiinnitetään huomiota oppilaan vahvuuksiin ja onnistumisiin oppijana sekä käsitellään oppimistavoitteita ja oppimisen edistymistä. Keskeiset arviointitulokset kirjataan arviointikeskustelulonakkeeseen. Lomaketta säilytetään koululla vuoden ajan.</p>
<p>Ajanvaraus keskusteluun tehdään Wilman kautta. Luokanopettaja lähettää&nbsp;huoltajille Wilmassa tapahtumakutsun arviointikeskusteluun 13.12.2019, ilmoittautumiset keskusteluun toivotaan joululomaan mennessä.</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
<p>Hansakallion koulu</p>
<p>&nbsp;</p>
