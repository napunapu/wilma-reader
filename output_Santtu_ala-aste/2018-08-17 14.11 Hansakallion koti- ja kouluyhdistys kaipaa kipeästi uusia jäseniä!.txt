Hansakallion koti- ja kouluyhdistys kaipaa kipeästi uusia jäseniä!
Väyrynen Sirpa (SirpaV)
Piilotettu
2018-08-17 14.11

<p><span style="color:#1d2129;">Hyvät vanhemmat,&nbsp;</span></p>
<p><span style="color:#1d2129;">Yhdistyksemme jäsenten lapset jo kasvaneet ja siirtyvät tai ovat pian siirtymässä ylä-asteelle, joten uudet jäsenet erittäin tervetulleita!</span></p>
<p><span style="color:#1d2129;"><span style="color:#1d2129;">Sääntömääräinen <strong>vuosikokouksemme on 20.9.18 klo 18:00 Hansakallion koulun ruokalassa.&nbsp;</strong><br>
Tarvitsemme lisää vanhempia mukaan toimintaan! Tulethan paikan päälle tutustumaan 20.9 kokoukseen tai ota yhteyttä sähköpostilla niin kerromme mielellämme lisää.<br>
Ei ole iso työmäärä mutta sitäkin palkitsevampaa!<br>
Hansakallion koti- ja kouluyhdistys toimii kaikkien koulun oppilaiden hyväksi. Yhdistys kustantaa oppilaille opiskeluvälineitä, stipendejä ja tapahtumia kuten koululla järjestetyt discot sekä tukee luokkaretkiä.&nbsp;<br>
Sähköposti&nbsp;<a href="http://#OnlyHTTPAndHTTPSAllowed"><span style="color:#1155cc;"><u>hansakallionvanhemmat@gmail.com</u></span></a>.&nbsp;<br>
<br>
Terveisin<br>
<br>
<br>
Hansakallion koti- ja kouluyhdistys</span> </span></p>
