Syysloman kynnyksellä.
Lukkarinen Elina (ElinaL)
Piilotettu
2016-10-18 14.49

<p>Hei,&nbsp;</p>
<p>koulumme syysloma alkaa tänään, eli 19.10.-23.10.&nbsp;lomaillaan ja nautiskellaan!&nbsp;<img height="23" src="/shared/scripts/ckeditor/plugins/smiley/images/regular_smile.png" width="23">&nbsp;Muutama tiedotusasia vielä ennen lomaa.</p>
<p>&nbsp;</p>
<p>Kokeiden arvioinnista muutama sananen: 3. luokalla kokeisiin valmistautumista harjoitellaan. Arvioinnissa pyritään jatkuvaan arviointiin, eli kokeet ovat vain yksi keino näyttää osaaminen. Tänään palautan oppilaiden mukana matikan kokeet, jotka on läksynä tuoda allekirjoitettuna takaisin kouluun loman jälkeen. Kokeet olen arvioinut sanallisesti käyttäen seuraavaa arviointiasteikkoa, jota tulen käyttämään kaikessa arvioinnissa:</p>
<p>Arvosanat tulevat siis suoraan numeroarvioinnista, josta en kuitenkaan oppilaiden kanssa puhu tässä vaiheessa vielä mitään.&nbsp;</p>
<p>&nbsp;</p>
<p>Toiseksi syysloman läksyistä: Lukupiiriläksy on kaikilla myös syyslomaviikolla. Toisena läksynä kaikilla on tähän saakka harjoiteltujen kertotaulujen (2-6, 8 ja 10) opettelu<u> mahdollisimman hyvin ulkoa</u>. Ohjeistin oppilaita harjoittelemaan kertotauluja vähän joka päivä. <u>Kertotaulut ovat peruslaskutoimituksia, jotka täytyy hallita hyvin!</u>&nbsp;Monet uudet&nbsp;matikan asiat perustuvat kertolaskujen hyvään hallintaan, joten teemme mahdollisimman hyvän pohjatyön tässä vaiheessa. Kaikki kertotaulut löytyvät harjoittelua varten matikan kirjan sisätakakannesta. On hyvä tukea lapsen harjoittelua kyselemällä kertotauluja ihan missä tilanteessa vain ja satunnaisessa järjestyksessä. Myös tietokoneelle/iPadille on paljon erilaisia ohjelmia monipuolistamaan kerotaulujen harjoittelua(esim. tohtori Tulon kertotaulukoulu, ten monkeys ym.). Kaikilla oppilailla on käytettävissä myös Otavan Oppilasmateriaalit, johon tunnukset löytyvät matikan vihkon sisäkannesta tai moni taitaa ne jo muistaa ulkoakin. Tsemppiä myös kotiharjoituksiin!&nbsp;</p>
<p>&nbsp;</p>
<p>Nyt opekin vaihtaa lomavaihteen päälle. Aurinkoista lomaa kaikille!&nbsp;</p>
<p>&nbsp;</p>
<p>Terkuin,</p>
<p>Elina&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
