1-luokkalaisen terveystarkastus
Auvinen Arja (A.A)
Piilotettu
2014-09-15 12.31

<p>Hei huoltaja, jos lapsi ei ole vielä käynyt 1-luokkalaisen terveystarkastuksessa, niin voit ottaa wilmaviestitse yhteyttä minuun ja katsotaan sopiva tapaamisaika.<br>
Yst.terv Arja Auvinen</p>
