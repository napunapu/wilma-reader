Tiedotus Valhallantien lähistöllä asuville
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2015-09-08 09.52

<p>Arvoisat huoltajat
</p>
<p><span style="font-family:Calibri,sans-serif"><span style="color:#000000"><span style="font-size:18px">Viesti koskee Hansakallion koulun oppilaita ja heidän huoltajiaan, jotka asuvat Valhallantien lähistöllä. Koululaiset tulevat valhallantietä pitkin Kauklahden tieltä päin ja käyttävät risteyksen ylittämiseen liikenneympyrää eikä merkittyjä suojateitä. Oppilaiden keksimä reitti on todella vaarallinen! Lasten liikennekäyttäytymisestä liikenneympyrässä on syytä keskustella kotona. Alueella asuva huoltaja on turhaan yrittänyt opastaa oppilaita oikeaan liikennekäyttäytymiseen.</span></span></span></p>
<p><span style="font-family:Calibri,sans-serif"><span style="color:#000000"><span style="font-size:18px">Toivotaan koululaisille turvallisia koulumatkoja!</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Jarmo Jäääskeläinen</span></span></span><br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Hansakallio</span></span></span><br>
<span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">rehtori</span></span></span></p>
<p></p>
