Huoltajatiedote 10.8.2017
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2017-08-10 13.02

<p>Arvoisat huoltajat</p>
<p>Tervetuloa kaikki oppilaat ja huoltajat aloittamaan Hansakallion koulun lukuvuotta 2017-18. Aivan erityisesti on syytä&nbsp;mainita ykkösluokat, joita meillä on neljä yleisopetusluokkaa ja yksi pienryhmä.</p>
<p>Aikatauluasiaa; tänään torstaina oppilaiden päivä päättyy klo 13.15. Huomenna perjantaina koulumme taxioppilaat käyvät koulua kesäkuussa taxiyrittäjälle ilmoitetun lukujärjestyksen mukaisesti. Muutoin koulu päättyy perjantaina viimeistään klo 13.15 (poikkeuksena siis mahdollinen taxioppilas). Luokanopettaja ohjeistaa tarvittaessa oppilaita ja huoltajia aikatauluasiassa. Ensi viikolla tarkoituksena on käydä koulua lukujärjestyksen mukaisesti. Opettaja huolehtii siitä, että tieto oikeasta aikataulutuksesta tavoittaa oppilaat ja huoltajat. Julkaisemme lukujärjestykset wilmassa heti, kun olemme saaneet muutokset ja erilaiset tarkennukset tehdyksi.</p>
<p>Neljäs-, viides- ja kuudesluokkalaisten vapaavalintaisten opetus käynnistyy pääsääntöisesti ensi viikolla (yksi vuosiviikkotunti). Opetus järjestetään vaihtoehtoisesti niin, että opetusta on koko lukuvuoden ajan yksi tunti viikossa tai vaihtoehtoisesti kaksi tunti kerrallaan yhden&nbsp;lukukauden ajan.</p>
<p>Oikein hyvää lukuvuotta kaikille.</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki</p>
