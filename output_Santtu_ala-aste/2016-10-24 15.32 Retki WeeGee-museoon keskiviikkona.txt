Retki WeeGee-museoon keskiviikkona
Lukkarinen Elina (ElinaL)
Piilotettu
2016-10-24 15.32

<p>Hei!&nbsp;</p>
<p>Lähdemme keskiviikkona tutustumaan WeeGee-näyttelykeskukseen ja Kauklahden lasitehtaasta kertovaan näyttelyyn sekä aiheesta tehtyyn näytelmään. Koulua päivän aikana on normaalisti lukujärjestyksen mukaan.</p>
<p>Näyttelyn järjestäjät lähettivät oppilaiden mukana täytettäväksi kuvauslupalomakkeen, jonka pyydän palauttamaan minulle keskiviikkoon mennessä!&nbsp;</p>
<p>&nbsp;</p>
<p>Terkuin,&nbsp;</p>
<p>Elina</p>
