Viikonlopputerveiset!
Lukkarinen Elina (ElinaL)
Piilotettu
2019-08-16 13.04

<p>Hei!&nbsp;</p>
<p>Kouluvuosi 6E-luokalla on pyörähtänyt mukavasti käyntiin. Olemme yhdessä totutelleet arkeen ja käytäntöihin EYL:n tiloissa, ja kaikki on sujunut tosi hyvässä hengessä.&nbsp;</p>
<p>Tässä muutamia käytännön asioita tiedoksi myös teille:</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Mukavaa viikonloppua teille kaikille!&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin Elina&nbsp;</p>
