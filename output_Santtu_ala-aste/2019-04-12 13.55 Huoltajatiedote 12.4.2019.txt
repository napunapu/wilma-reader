Huoltajatiedote 12.4.2019
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2019-04-12 13.55

<p>Arvoisat huoltajat,</p>
<p>Tulevalla viikolla keskiviikkona 17.4 osallistumme seurakunnan järjestämään pääsiäiskirkkoon, paikkana Kauklahden kappeli. Samanaikaisesti koululla järjestetään Suomen pakolaisavun&nbsp;toimesta globaali kasvattaja -teeman alla ohjelmaa.</p>
<p>Kuluvan lukuvuoden aikana on ollut esillä&nbsp;Hansakallion koulun ja Espoon yhteislyseon yhdistäminen yhtenäiseksi peruskouluksi, Kauklahden kouluksi. Asian esittelijä on päättänyt, että yhdistämishanketta ei nyt viedä eteenpäin.</p>
<p>&nbsp;</p>
<p>Rauhallista pääsiäisen aikaa</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki&nbsp;</p>
