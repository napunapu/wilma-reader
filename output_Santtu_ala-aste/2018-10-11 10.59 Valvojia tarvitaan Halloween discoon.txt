Valvojia tarvitaan Halloween discoon
Väyrynen Sirpa (SirpaV)
Piilotettu
2018-10-11 10.59

<p>Hansakallion koti- ja kouluyhdistys järjestää Halloween-discon koululla torstaina 1.11.2018. Disco on tarkoitettu vain Hansakallion koulun oppilaille.</p>
<p><br>
&nbsp;</p>
<p>1-3. luokkalaisten disco klo 18.00-19.30</p>
<p>4-6. luokkalaisten disco klo 19.00-20.30</p>
<p><br>
&nbsp;</p>
<p>Sisäänpääsymaksu 1e. Discossa on buffet eli mukaan voi ottaa pienen summan rahaa. Mitään kilpailevaa myyntitoimintaa ei tilaisuudessa sallita.</p>
<p>Jotta disco voidaan toteuttaa, tarvitsemme vanhempia valvojiksi kahteen eri vuoroon 10 henkilöä / vuoro:</p>
<p><br>
&nbsp;</p>
<p>- Valvojavuoro 1 klo 17.45 – 19.30 (Info valvojille klo 17.45-18.00)</p>
<p>- Valvojavuoro 2 klo 18.45 - 21.00 (Info valvojille klo 18.45-19.00, klo 20.30 alkaen loppusiivous)</p>
<p><br>
&nbsp;</p>
<p>Valvojien tehtävänä on järjestyksenpito, buffet, pääsymaksut ja loppusiivous. Valvojat tulkaa ajoissa infoon. Lapset saavat tulla sisälle vasta kun disco alkaa.</p>
<p><br>
&nbsp;</p>
<p>Ilmoittaudu valvojaksi sunnuntai 28.10 mennessä <a href="#OnlyHTTPAndHTTPSAllowed"><u>hansakallionvanhemmat@gmail.com</u></a> ja ilmoita kumpaan vuoroon olet tulossa. Voit myös tulla molempiin vuoroihin. Jälkimmäiseen vuoroon on aina ollut hankalampi saada tarpeeksi valvojia. Disco voidaan järjestää vain jos valvojia saadaan paikalle riittävästi.</p>
<p><br>
&nbsp;</p>
<p>Keskiviikkona 31.10 klo 18-19 ohjelmassa disco-koristelua koululla. Tervetuloa auttamaan koristelussa. Koti- ja kouluyhdistys hankkii koristeet.</p>
<p><br>
&nbsp;</p>
<p>Terveisin</p>
<p><br>
&nbsp;</p>
<p>Hansakallion koti- ja kouluyhdistys</p>
