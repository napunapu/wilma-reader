Tiedote koronaviruksesta - About the coronavirus
Kass Veronika (V.K)
Piilotettu
2020-02-28 16.01

<p><strong>Tietoa koronaviruksesta</strong></p>
<p>Koronavirus mietityttää monia. Kausi-influenssaa ja tavallisia flunssaviruksia on nyt runsaasti liikkeellä ja influenssakauden huippu lähestyy. Kaikki hengitystieinfektiot eivät ole koronaviruksen aiheuttamia.</p>
<p>&nbsp;</p>
<p>Sairaana ei pidä lähteä kouluun, päiväkotiin eikä töihin. Jos sinulla tai lapsellasi on korkea kuume ja hengitystieoireita, soita omalle terveysasemallesi. Pysy kotona odottamassa toimintaohjeita. Näin pystymme estämään tautien leviämistä.</p>
<p>Suu-nenäsuojuksen käyttö ei useimmissa tilanteissa ole tarpeellista tartunnalta suojautumisen kannalta. Suojuksen käytöstä saattaa olla hyötyä niille henkilöille, joilla on jo tartunta ja jotka joutuvat liikkumaan julkisilla paikoilla; tällöin tartunta ei leviä itsestä muihin.</p>
<p>&nbsp;</p>
<p>-----------------------------</p>
<p>&nbsp;</p>
<p><strong>About the coronavirus</strong></p>
<p>Many of us are concerned about the coronavirus. Seasonal influenza and ordinary flu viruses are very common at the moment, and we are approaching the peak of the influenza season. Not all respiratory infections are caused by the coronavirus.</p>
<p>&nbsp;</p>
<p>If you are ill, do not go to school, day care or work. If you or your child have a high fever and respiratory symptoms, call your health centre. Stay at home while you await further instructions. This helps us prevent the disease from spreading.</p>
<p>&nbsp;</p>
<p>It is usually not necessary to wear a face mask to cover your nose and mouth to avoid an infection. Using a mask may be useful for persons who already have an infection and have to spend time in public areas. A mask will prevent them from spreading the infection to others.</p>
