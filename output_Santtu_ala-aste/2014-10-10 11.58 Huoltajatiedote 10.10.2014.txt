Huoltajatiedote 10.10.2014
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2014-10-10 11.58

<p>Arvoisat huoltajat,
</p>
<p>Vietämme syyslomaa 16.-20.10.2014. Tiistaina 21.10 jatkamme koulunkäyntiä lukujärjestyksen mukaan. Opettajakunta osallistuu torstaina 23.10 kaupungin järjestämään opetussuunnitelmakoulutukseen. Tämän vuoksi koulupäivä päättyy klo 12.15. Koulu huolehtii taksimuutoksista. Koti- ja kouluyhdistys järjestää oppilasdiskon 30.10. klo 18. Koulun joulujuhla järjestetään iltajuhlana keskiviikkona 17.12. Tarkempi aikataulu ilmoitetaan myöhemmin. Koulu toivottaa kaikille hyvää Aleksis Kiven päivää ja virkeää tulevaa syyslomaa!</p>
<p>Syysterveisin,</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki</p>
<p></p>
