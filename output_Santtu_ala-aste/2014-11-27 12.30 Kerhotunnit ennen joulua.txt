Kerhotunnit ennen joulua
Rauhalammi Sonja (S.R)
Piilotettu
2014-11-27 12.30

<p>Hei Englannin kerholaisten huoltajat,
</p>
<p>viimeisen kouluviikon ohjelma ja aikataulu on vähitellen selviämässä. Olin aiemmin ilmoittanut, että 1.-luokkalaisten A-ryhmän viimeinen kerhotunti olisi 18.12., mutta ennen joulua koulupäivien pituuksiin tulee aina muutoksia.</p>
<p>Ilmeisesti viimeisellä viikolla torstaina 18.12. kaikilla koulun oppilailla on lyhennetty koulupäivä, eli koulupäivä päättyy tuolloin 12.15. Englannin kerhon kannalta tämä tarkoittaa, että lapsellanne ei ole kerhotunti to 18.12., sillä muuten väliin tulisi hyppytunti ja opettajilla on jokatapauksessa muuta ohjelmaa klo 12.15 jälkeen. Lapsenne viimeinen kerhotunti tänä syksynä on siis to 4.12 klo 13.15.</p>
<p>En valitettavasti tiennyt viimeisen viikon aikatauluja alkusyksystä, kun lähetin teille listan kerhotunneista. Keväällä on sitten enemmän kerhotunteja A-ryhmälle.</p>
<p>Ystävällisin terveisin,<br>
Sonja Rauhalammi</p>
<p></p>
