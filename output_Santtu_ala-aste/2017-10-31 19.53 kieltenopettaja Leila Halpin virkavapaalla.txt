kieltenopettaja Leila Halpin virkavapaalla
Halpin Leila (LeilaH)
Piilotettu
2017-10-31 19.53

<p>Hyvät huoltajat!</p>
<p>Tiedoksenne, että olen huomisesta 1.11. lähtien virkavapaalla&nbsp;noin 4 viikkoa.&nbsp; Sijaisenani toimii kieltenopettaja Markus Peltonen sekä torstain 6. lk saksan tunnilla Jaana Juslin ja perjantain 4ABCD:n saksan ja 4BC:n ruotsin tunneilla Päivi Salmi.&nbsp; Kaikkiin opettajaiin&nbsp;saatte yhteyden Wilma-viestillä.&nbsp;</p>
<p>Oikein mukavaa marraskuuta koko perheelle ja jaksamista pimeneviin aamuihin ja iltoihin!</p>
<p>Ystävällisesti Leila Halpin</p>
