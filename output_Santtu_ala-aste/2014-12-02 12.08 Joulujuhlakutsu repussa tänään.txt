Joulujuhlakutsu repussa tänään
Helotie Anni (HeAn)
Piilotettu
2014-12-02 12.08

<p>Hei!<br>
Oppilaiden mukana tulee tänään kotiin joulujuhlakutst. 1A-luokan perheet ovat tervetulleet seuraamaan juhlaa, joka alkaa keskiviikkona 17.12. kello 18.00. Luokkamme esiintyy kaikissa kolmessa juhlassa, joten laitan oppilaita koskevan aikataulun vielä myöhemmin kotiin mukaan.<br>
Terveisin, Anni-ope</p>
