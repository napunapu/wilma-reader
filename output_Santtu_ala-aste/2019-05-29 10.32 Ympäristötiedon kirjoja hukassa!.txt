Ympäristötiedon kirjoja hukassa!
Aittola Johanna (JohannaA)
Piilotettu
2019-05-29 10.32

<p>Hei!</p>
<p>Tarkistaisitteko vielä kotona, että kaikki koulukirjat on palautettu koululle. Meillä on nyt hukassa esimerkiksi muutamia ympäristötiedon Pisara-kirjoja.&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin Johanna</p>
