Huoltajatiedote 17.9.2018
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2018-09-17 12.09

<p>Arvoisat huoltajat,</p>
<p>Osallistumme tällä viikolla Nälkäpäiväkeräykseen katastrofirahaston kartuttamiseksi. Rahaston avulla autetaan tukea tarvitsevia Suomessa ja ulkomailla.&nbsp;Lipaskeräys toteutetaan torstaina ja perjantaina. Emme osallistu nettikeräykseen, kuten aiempina vuosina. Koulu tilittää saadun keräysrahan kokonaisuudessaan Suomen punaiselle ristille.</p>
<p>Kaupunki on keskeyttänyt Espoon kouluissa oppilaiden kuntoa kartoittavat ns. valtakunnalliset Move-mittaukset. Koulut saavat myöhemmin tarkempaa ohjeistusta mittauksien jatkamiseen liittyen.</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
<p>Hansakallion koulu</p>
<p>&nbsp;</p>
