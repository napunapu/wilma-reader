Kahmi Kilsoja -tapahtuma 10.10-14.10 aktivoidaan lapsia liikkumaan
Ahtiainen Ella-Maria (EllaA)
Piilotettu
2016-10-06 21.39

<p>Hei,</p>
<p>Nyt aktivoidaan lapsia liikkumaan. Viime vuodesta jollekkin tuttu Kahmi Kilsoja -tapahtuma järjestetään Hansakallionkoulussa. Tavoitteena on lisätä vapaa-ajan liikkumista&nbsp;ja koulumatka liikuntaa, sekä aktivoida oppilaita myös välituntisin. Ideana on kerätä luokalle mahdollisimman paljon rasteja. Palkitsemme sekä yksilö, että ryhmä suorituksen. Eli eniten rasteja kerännyt oppilas 1-2 luokalta, 3-4 luokalta ja 5-6 -luokalta, sekä koko 1-2&nbsp;ja 3-6 paras luokka palkitaan.</p>
<p>Kisassa kerätään merkintöjä tietyn kävely- ja pyöräilymatkan saavuttamisesta päivän aikana. Kisassa hyväksytään kaikki kisan aikana kävellyt ja poljetut kilometrit riippumatta siitä, ovatko ne tehty koulumatkoilla, koulussa tai vapaa-ajalla. Myös skeittilaudalla ja potkulaudalla kuljettu matka hyväksytään suoritukseksi. HUOM! Harrastuksien aikana poljetut kilometrit (esim. kuntopyörä jne eivät kelpaa) vaan nimenomaan välimatkat lasketaan.</p>
<p>Rasteja saa:</p>
<p>• 1-2-luokkalaisille&nbsp;<strong>1 km</strong>&nbsp;kävely ja/tai pyöräily = 1 rasti taulukkoon<br>
• 3-6 -luokkalaisille&nbsp;<strong>2 km&nbsp;</strong>kävely ja/tai pyöräily &nbsp;= 1 rasti taulukkoon</p>
<p>Opettajat keräävät oppilaiden vapaa-ajalla suorittamat rastit. Voitte kotona merkitä ylös opettajan työn helpottamiseksi, kuinka monta rastia oppilas on vapaa-ajalla kerännyt. Rasteja voi päivän aikana olla useampia ja niitä on mahdollisuus suorittaa myös koulussa.</p>
<p>Aktivoidaan yhdessä lapsia liikkumaan!</p>
<p>Terveisin,</p>
<p>Ella &amp; Jenni</p>
