Nelosluokkien projektityöskentely
Lukkarinen Elina (ElinaL)
Piilotettu
2017-12-01 12.57

<p>Hei kotiväki,</p>
<p>&nbsp;</p>
<p>Olemme aloittaneet projektityöskentelyn 4A- ja 4D-luokissa. Oppilailla on Google-tunnukset (etunimi.sukunimi@eduespoo.fi). Ensimmäisessä projektissa oppilaat ovat opiskelleet ympäristöopin sisältöjä, mutta projektissa arvioidaan myös suomen kielen taitoja (virkkeet, oikeinkirjoitus sekä pää- ja sivulauseet). Oppilaat ovat edenneet tehtävissään itsenäisesti valitsemassaan järjestyksessä. Me opettajat ohjaamme työskentelyä, ja annamme henkilökohtaista palautetta suullisesti tai kirjallisesti projektin sivustolla. Tarkoituksena on, että oppilas korjaa ja muokkaa tehtäviä palautteen mukaan.</p>
<p>&nbsp;</p>
<p>Työskentely on mahdollista myös kotona ja olemmekin kehottaneet oppilaita tekemään tehtäviä eteenpäin myös kotona, koska varsinaista läksyä ei ole ympäristöopista tullut. Googlen palveluita voi käyttää tietokoneella, tabletilla tai puhelimella, jossa on internet-yhteys. Tabletilla tarvittavat sovellukset ovat Google Docs ja Google Classroom, jotka ovat molemmat ilmaisia.</p>
<p>&nbsp;</p>
<p>Työskentely on ollut innostunutta ja jatkamme uuden työtavan harjoittelua. Jatkossa kokeilemme projektityöskentelyä myös muissa oppiaineissa.</p>
<p>&nbsp;</p>
<p>Terveisin</p>
<p>Saara, Elina ja Riikka</p>
