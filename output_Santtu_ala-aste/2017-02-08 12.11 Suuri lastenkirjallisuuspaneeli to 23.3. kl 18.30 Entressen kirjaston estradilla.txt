Suuri lastenkirjallisuuspaneeli to 23.3. kl 18.30 Entressen kirjaston estradilla
Vähätalo Riikka (vahatri)
Piilotettu
2017-02-08 12.11

<p><strong>Suuri lastenkirjallisuuspaneeli to 23.3. kl 18.30 Entressen kirjaston estradilla</strong></p>
<p>&nbsp;</p>
<p>"&nbsp;Lue lapselle! Miksi&nbsp;kouluikäisellekin&nbsp;ääneen&nbsp;lukeminen on tärkeää ja merkityksellistä? Miten kirjallisuus tukee lapsen kehitystä?"</p>
<p>&nbsp;</p>
<p>Suuri Lukuseikkailu -kirjallisuuskeskustelu Entressen kirjastolla vahemmille, opettajille ja kaikille&nbsp;kasvattajille. Alustajana FT, lastenkirjallisuuden tutkija, kriitikko, tietokirjailija Päivi Heikkilä-Halttunen. Panelisteina kulttuurilautakunnan varapj.&nbsp;erityisopettaja, kirjallisuuskriitikko Teresia Volotinen, kirjailija Johanna Venho&nbsp;sekä kirjallisuuspainotteisen Ymmerstan koulun opettajat Helena Linna ja Nina Jaakkola. Kirjaston&nbsp;edustajana&nbsp;aluelastenkirjastonhoitaja Pirkko Ilmanen</p>
<p>&nbsp;</p>
<p>Koululaiset esiintyvät.&nbsp;</p>
<p>&nbsp;</p>
<p>Lämpimästi tervetuloa!</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Illan rahoittaa Runomatka -hanke. Lukuvuonna&nbsp;2016-2017 Entressen kirjasto tarjoaa&nbsp;alueen kaikille 2-luokille Runomatka -kiertueen. Kirjavinkkarit käyvät luokissa pitämässä runomaistiaisia ja jokainen tokaluokkalainen pääsee luomaan runoja kirjailija Johanna Venhon työpajoissa.</p>
<p>&nbsp;</p>
<p>hankekuvaus:<a href="http://hankkeet.kirjastot.fi/hankehaku/?search_api_views_fulltext=Aina%20on%20runon%20verran%20aikaa">http://hankkeet.kirjastot.fi/hankehaku/?search_api_views_fulltext=Aina%20on%20runon%20verran%20aikaa</a>​</p>
