Tiedotettavaa liikuntatunneista.
Lukkarinen Elina (ElinaL)
Piilotettu
2015-09-29 15.48

<p>Hei taas kotiväki!
</p>
<p>Muutama tiedotettava asia:</p>
<p>-<b> Luokkamme ulkoliikuntakausi jatkuu syyslomaan saakka</b>. Liikuntatunnit ovat torstaisin, jolloin oppilailla tulee olla säänmukainen ulkoliikuntavarustus. Oppilaille kannattaa kuitenkin varata jo valmiiksi koululle myös sisäliikuntavarusteet, sillä huonon sään sattuessa liikumme liikuntasalissa jo ennen syyslomaa. <b>Viikolla 43 aloitamme virallisesti sisäliikuntajakson!</b></p>
<p>- 2A-luokan viimeinen <b>uintikerta on torstaina 8.10.kello 13.00-15.00.</b> Merkitsethän tästä aiheutuvat lukujärjestysmuutokset muistiin.</p>
<p>Varaattehan uimapäivänä oppilaille eväät mukaan kouluun! Syömme eväät luokassa ennen uimahallille lähtöä.</p>
<p> <br>
Liikuntaterveisin,</p>
<p>Elina-ope</p>
<p></p>
