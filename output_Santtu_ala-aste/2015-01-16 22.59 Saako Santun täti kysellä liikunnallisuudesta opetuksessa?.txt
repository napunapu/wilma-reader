Saako Santun täti kysellä liikunnallisuudesta opetuksessa?
Korpela Panu (Santtu Tarvainen, 6E/2020)
Helotie Anni (HeAn)
2015-01-16 22.59

<p>Hei!
</p>
<p>Saisiko veljeni vaimo Brigette Dunn kysellä käyttämästäsi liikunnallisuudesta 1-luokkalaisen opetuksessa? Hän luennoi nyt syksyllä California State Universityssä mm. tanssin opettamista lapsille, ja olisi kovin kiinnostunut kokemuksistasi.</p>
<p>Jos tämä on ok, niin mitä mailiosoitetta voisi käyttää?</p>
<p>Terv,<br>
     Panu</p>
<p></p>
