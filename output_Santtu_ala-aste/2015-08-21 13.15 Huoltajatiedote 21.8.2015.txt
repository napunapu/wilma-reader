Huoltajatiedote 21.8.2015
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2015-08-21 13.15

<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Arvoisat huoltajat,</span></span></span>
</p>
<p></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Koulutyö on käynnistynyt kauniiden ilmojen saattelemana. Hyvä näin, tosin useiden oppilaiden mukaan loma oli hienoa aikaa säistä riippumatta. Oppilaiden lukumäärä jatkaa kasvuaan. Meitä Hansakallion koulun oppilaita on nyt 467. Lisäksi esiopetuksen oppilaita on 43. Meitä opettajia työskentelee talossamme noin 35, avustavaa henkilökuntaa on lisäksi noin 10. Olemme siis jo aika iso yhteisö!</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Koulun jatkaa entisillä painopistealueillaan. Tarkoituksenamme on tarjota hyvää perusopetusta turvallisessa oppimisympäristössä. Lisäksi kehitämme toimintaamme jo tutuilla Arvokas -koulun teemoilla, joissa korostuvat sosiaalisen kanssakäymisen harjoittelu. Toiminnallisen koulun kautta tuomme puolestaan erilaista liikettä välitunneille ja osaksi oppiainejärjestelyjä. Oppilaiden osallistaminen on molemmilla painopistealueilla tärkeässä roolissa. Lisäksi tavoitteemme on kehittää tieto- ja viestintäteknologiaa osana oppilaan oppimista.</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Koulussamme keittiö tarjoaa kolme kertaa viikossa (ti, ke ja to) mahdollisuuden ostaa välipalaa. Tätä varten oppilas tarvitsee välipalakortin (yhteispalvelupiste). Raha ei siis maksuvälineenä kelpaa. Välipalan ei millään tapaa ole tarkoitus korvata koululounasta. Välipala on tarjolla 1. syyskuuta alkaen.</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Kerhotoiminta käynnistyy syksyn edetessä. Ilmoittautuminen kerhoihin tapahtuu Wilman kautta myöhemmin tulevien ohjeiden mukaisesti. Kerhotoiminta on pääosin opettajavetoista, jossain kerhoissa on koulun ulkopuolinen vetäjä.</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Välitunneilla koulussa on tarjolla ohjattua välkkäritoimintaa. Oppilaat itse ovat mukana suunnittelemassa ja ohjaamassa välituntitoimintaa. Toivomme, että ohjattu toiminta ja muut tavat viettää hetki ulkona innostavat niin paljon, ettei oppilas edes huomaa omistamaansa matkapuhelinta. Koulussa kännykkää voidaan joskus käyttää opettajan johdolla osana opetusta tai välttämättömiin puheluihin. Muutoin ilman sitä pärjää hyvin.</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Toivomme, että tarvittaessa päivität Wilman kautta muuttuneet yhteystietosi.</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial"><b>Syksyn tärkeitä päivämääriä</b>: Koulumme ensimmäisellä luokalla olevien vanhempainilta on 26.8 klo 18. Kaksi - kuudesluokkalaisten vanhempainilta on 2.9. klo 18. Buffet on ennen tilaisuutta auki. Koulukuvat otetaan 1.-3.9. Koulu-uinnit käynnistyvät nopeasti heti elokuun lopusta ja jatkuvat pitkin syksyä. Tarkempi aikataulutus tulee erikseen. Syysloma on 14.- 18.10. Joulujuhlaa vietetään lauantaina 12.12.</span></span></span></p>
<p></p>
<p></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Hyvää lukuvuotta 2015-16 kaikille hansakalliolaisille!</span></span></span></p>
<p><span style="color:#000000"><span style="font-size:18px"><span style="font-family:Arial">Toivottavat Jarmo Jääskeläinen ja Hansakallion koulun väki.</span></span></span></p>
<p></p>
