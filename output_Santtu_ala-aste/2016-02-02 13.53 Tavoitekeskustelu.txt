Tavoitekeskustelu
Lukkarinen Elina (ElinaL)
Piilotettu
2016-02-02 13.53

<p>Heippa!
</p>
<p>Sopisiko teille Santun tavoitekeskustelu iltapäivällä ma 8.2., ti 9.2.? Jos nämä iltapäivät eivät sovi niin myös ma 15.2 tai ti 16.2 sopii minulle. Ilmoitatteko mikä päivä sopii ja mihin aikaan pääsette tulemaan. Minulla loppuu opetus molempina päivinä klo 13.15.</p>
<p>Tapaamisiin!</p>
<p>T: Elina L.</p>
<p></p>
