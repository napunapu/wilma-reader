Erityisopen puhelinnumero sekä tarkennuksia tuen antamisesta
Vasara Liisa (LiisaV)
Piilotettu
2020-03-30 10.09

<p><u>Hei 2.-, 5.-, 6.-luokkalaisen oppilaan huoltajat,</u></p>
<p>olen&nbsp;saanut käyttööni prepaid-liittymän, jonka numero on&nbsp;<u><strong>0449377813</strong></u>. Parhaiten&nbsp;minut tavoittaa päivisin klo 9-14 välillä. Numeron voi antaa myös suoraan oppilaalle. Jos minulla on etäopetus kesken, soitan&nbsp;takaisin myöhemmin. Kun soitan, puhelimessa näkyy&nbsp;<strong>"yksityinen numero"</strong>. Voit kysyä minulta ohjeita tehtäviin tai muuta etäkoulunkäyntiin liittyvää&nbsp;asiaa. Edelleen myös <strong>Wilma-viestillä</strong> tavoitat minut näppärästi noin klo 8-16. Toivon, että otatte yhteyttä matalalla kynnyksellä.&nbsp;</p>
<p><strong>Soitan tällä viikolla joillekin oppilaille (painotus 2.-luokkalaisissa).</strong></p>
<p><strong>5.-6.-luokkalaisille tukea on tarjolla päivittäin ma-pe n. klo 9-11 ja 12-14&nbsp;Meet-sovelluksessa&nbsp;</strong>(poislukien päivät, jolloin olen määrätty lähiopetukseen koululle).</p>
<p>​​ystävällisin terveisin,&nbsp;</p>
<p>Liisa Vasara, erityisopettaja</p>
