Tiistaiksi taskulamppu ja villasukat
Moilanen Jenni (JenniM)
Piilotettu
2018-11-23 12.38

<p><span style="font-size:14px;">Hei kotiväki,</span></p>
<p><span style="font-size:14px;">Osana Hansakallion koulun 60v-juhlaviikkoa kolmoset järjestävät tiistaina koulupäivän aikana&nbsp;<em>valokarnevaalin</em>.</span></p>
<p><span style="font-size:14px;">Karnevaalipäivänä koulun käytävät on pimennetty ja käytäviä valaisemaan toivotaan oppilailta taskulamppuja, joiden kanssa liikkua käytävillä ja ihastella erilaisia karnevaaliaskarteluja. Rauhallista äänimaailmaa luomaan toivomme oppilaiden jalkaan myös ääntävaimentavia villasukkia.</span></p>
<p><span style="font-size:14px;"><em>Tiistaita varten voi siis ottaa mukaan <u>taskulampun ja villasukat</u></em>!</span></p>
<p>&nbsp;</p>
<p><span style="font-size:14px;">Valoa pimeyden keskelle toivottaen,</span></p>
<p><span style="font-size:14px;"><em>Hansiksen kolmoset</em></span></p>
