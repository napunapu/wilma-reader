Muistutus vappupallomyynnistä
Hult Heidi (HeidiH)
Piilotettu
2016-04-08 14.05

<p>Heippa kotiväki!
</p>
<p>Kevät on jo pitkällä ja vappukin on jo melkein oven takana. 4A-luokka myy tänä keväänä leirikoulukassaansa kartuttaakseen vuoden hienoimpia vappupalloja!</p>
<p>Oppilaat ovat saaneet reppupostissa mukaansa tilauslomakkeen, jossa on pallovalikko sekä tilaus-, ja nouto-ohjeet. Pallojen viimeinen tilauspäivä on <b><i>ensi perjantaina 15.4! </i></b>Tilaukset toivotaan tehtäviksi sähköpostitse, mutta otamme tietenkin vastaan myös paperiset tilauslomakkeet. Mikäli tilaat pallosi paperisella lomakkeella, maksu tapahtuu noudon yhteydessä käteisellä.</p>
<p>Lisätietoja palloista saa sähköpostitse osoitteesta <a href="mailto:vappupallot4a@gmail.com">vappupallot4a@gmail.com</a>.</p>
<p>Kiitos jo etukäteen kaikille kannatuksesta!</p>
<p>Keväistä viikonloppua!<span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span></p>
<p>Toivottavat,<br>
4A:n oppilaat, Heidi-ope ja luokkatoimikunta</p>
<p></p>
