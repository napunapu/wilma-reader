Perjantai 27.9.
Konu Pauliina (PauliinaK)
Piilotettu
2019-09-25 13.56

<p>Hei kuudensien luokkien huoltajat!</p>
<p>Lähdemme kaikkien kutosluokkien voimin&nbsp;ylihuomenna perjantaina seuraamaan ilmastomielenosoitusta Helsinkiin eduskuntatalon edustalle. Aihe on ollut tällä viikolla erityisen ajankohtainen ja halusimme tarttua siihen, sillä se menee hienosti yksiin 6. luokan opetussuunnitelman kanssa ainakin seuraavilla tavoilla:</p>
<p>Vietämme paikan päällä tunnin verran ja pysymme väkijoukosta sivummalla. Tarkoituksena on päästä aistimaan tunnelmaa ja näkemään, miltä tämän kaltainen vaikutusmuoto näyttää. Tapahtumaan osallistumisesta on Espoon kaupungin hyväksyntä.</p>
<p>Mikäli päätätte kotona, että ette halua&nbsp;lapsenne&nbsp;osallistuvan ryhmän mukana, hän jää koululle opiskelemaan aiheeseen liittyviä sisältöjä. Ilmoitattehan omalle luokanopettajalle tämän tai huomisen päivän aikana, mikäli lapsenne jää koululle.</p>
<p>Terkuin Pauliina, Heidi, Sallis ja Elina</p>
