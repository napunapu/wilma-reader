Nuokun kerhoissa on tilaa 3-6-luokkalaisille
Smolander Maritta (MarittaS)
Piilotettu
2018-01-08 15.28

<p>SEURAAVISSA KAUKLAHDEN NUORISOTILAN KERHOISSA OLISI VIELÄ TILAA. (Kerhot jatkuvat viikolla 3) MAANANTAI</p>
<p>Kokkikerho 5.-6.-luokkalaisille maanantaisin klo 14.30-16.00 Kauklahden nuorisotilalla. Kokkikerhossa tehdään maittavia, niin suolaisia kuin makeitakin ruokia lasten toiveiden mukaisesti. KESKIVIIKKO</p>
<p>Kuvataidekerho 4.-6.-luokkalaisille keskiviikkoisin klo 14.15-15.45 Kauklahden nuorisotilalla. Kerhossa harjoitellaan piirtämistä, maalaamista ja kuvanveistoa erilaisilla väreillä ja tekniikoilla.</p>
<p>Hansiksen Kokkikerho 4.-6.-luokkalaisille keskiviikkoisin klo 14.30-16.30 Kauklahden nuorisotilalla. Kokkikerhossa tehdään maittavia, niin suolaisia kuin makeitakin ruokia lasten toiveiden mukaisesti.</p>
<p>TORSTAI</p>
<p>Tyttökerho</p>
<p>3.-4.-luokkalaisille torstaisin klo 13.15-14.45 Kauklahden nuorisotilalla. Kerhossa tehdään joka viikko erilaisia asioita, kuten askarteluja, kokkailuja, pelailua ja kaikenmoista kivaa tytöille.</p>
<p>Kokkikerho - Murkinaa muksuille 3.-4.-luokkalaisille torstaisin klo 13.15-14.45 Kauklahden nuorisotilalla. Nuokussa kokkaillaan kavereiden kanssa helppoja välipaloja ja suussa sulavia herkkuja.</p>
<p>Askartelukerho 4.-6.-luokkalaisille torstaisin klo 15.15-16.45 Kauklahden nuorisotilalla. Kerhossa tehdään kaikenmoista askartelua eri materiaaleilla ja menetelmillä. BÄNDIKERHO max. 5 osallistujaa (uusi kerho, alkaa heti kun ilmoittautuneita on tarpeeksi) 5.-6.-luokkalaisille torstaisin klo 15.00-16.00 Kauklahden nuorisotilalla. Kerhossa tutustutaan bändisoittimiin ja yhdessä musisoimisen maailmaan.</p>
<p>PERJANTAI</p>
<p>Puuhakerho 3.-5.-luokkalaisille perjantaisin klo 14.00-15.30 Kauklahden nuorisotilalla. Kerhossa tehdään joka viikko erilaisia asioita, kuten askarteluja, kokkailuja, pelailua ja kaikenmoista kivaa.</p>
<p>ILMOITTAUTUMISOHJEET Ilmoittautua voi sähköpostilla, laita viestiin kerho, johon lapsi ilmoittautuu, lapsen ja huoltajan nimi, lapsen syntymäaika sekä huoltajan sähköpostiosoite ja puhelinnumero. Kokkikerhoihin ja perjantain puuhakerhoon ilmoittautuessa lisää viestiin myös tiedot lapsen mahdollisista ruoka-aineallergioista ja erikoisruokavalioista. Sähköposti ilmoittautumiset osoitteeseen kauklahti.nuoriso@espoo.fi.</p>
<p>HUOM!</p>
<p>Kerhojen tarkat alkamisajankohdat ilmoitetaan kerholaisille henkilökohtaisesti. Kerhot eivät kokoonnu lomien aikana.</p>
<p>HUOMIOI LISÄKSI</p>
<p>Kerhot täytetään ilmoittautumisjärjestyksessä. Kerhot ovat maksuttomia.</p>
<p>Kerhot sopivat kaiken taitoisille lapsille, eikä aikaisempaa kokemusta tarvita.</p>
<p>Ensimmäisellä kerhokerralla osallistuja saa kotiin täytettäväksi nuorisopalveluiden jäsenkorttihakemuksen.</p>
<p>Ilmoittautumalla kerhoon, osallistuja sitoutuu käymään säännöllisesti kerhossa ja mahdollisen poissaolon sattuessa ilmoittamaan siitä nuorisotilalle hyvissä ajoin. Mikäli näitä ei noudateta, saatetaan kerholainen korvata toisella halukkaalla.</p>
<p>Kauklahden nuorisotila Hansakallio 2, 02780 Espoo 0468771356 kauklahti.nuoriso@espoo.fi</p>
