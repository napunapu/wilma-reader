Arvokas-terveiset
Juslin Jaana (JUJ)
Piilotettu
2016-05-25 15.29

<p>Hei kotiväki!
</p>
<p>Tässä kesän kynnyksellä muutama sana Hansakallion koulun Arvokas-vuodesta.<br>
Arvokashan on koulumme käyttämä opetusmenetelmä, jolla vahvistamme oppilaiden sosiaalisia taitoja ja tunnetaitoja, kasvatamme itsehillintää ja jonka avulla pohdimme eettisen kasvatuksen eri teemoja.</p>
<p>Tänäkin lukuvuonna olemme pitäneet luokissa Arvokas-tunteja yhteisten teemojen tiimoilta. Jokainen luokkataso on vuorollaan pitänyt Arvokas-aamunavauksen.<br>
Olemme katselleet neljästi Kino Hansis -elokuvia Arvokas-hengessä. Kevätlukukaudella meillä on ollut suvaitsevaisuuskampanja, jolla pyrimme lisäämään koulussa suvaitsevaisuutta ja erilaisuuden sietämistä. Kampanjaan kuului julistekilpailu, kansainvälisten leikkien päivä ja ystävyyden viikko. Jatkamme suvaitsevaisuuskampanjaa syyslukukaudella.</p>
<p>Aurinkoista ja rentouttavaa kesää toivotellen<br>
Hansakallion koulun Arvokas-tiimin puolesta</p>
<p>Jaana Juslin</p>
<p></p>
