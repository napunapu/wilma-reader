Tavoitekeskusteluajoista.
Lukkarinen Elina (ElinaL)
Piilotettu
2015-12-16 14.30

<p>Hei,
</p>
<p>tavoitekeskusteluille sopivia aikoja kartoittavaan kyselyyn oli tullut muutama nimetön vastaus. Käytkö merkitsemässä vielä uudelleen lapsesi tavoitekeskustelulle sopivat ajankohdat linkin takaa löytyvään taulukkoon(lapsen nimi tulee kohtaan "Oma nimesi"), niin saan tavoitekeskusteluajankohdan ilmoitettua myös koteihin ennen joulua. Suuri kiitos! <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span> Tämän viestin lähetin siis vain huoltajille, joiden merkitsemät ajat eivät vielä näy taulukossa. </p>
<p><a href="http://www.sumpli.com/p.php?i=576bc2f274527d1" target="_blank" rel="noopener" class="outbound">http://www.sumpli.com/p.php?i=576bc2f274527d1</a></p>
<p></p>
<p>Jouluterveisin,</p>
<p>Elina-ope</p>
<p></p>
