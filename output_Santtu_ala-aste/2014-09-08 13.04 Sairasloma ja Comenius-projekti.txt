Sairasloma ja Comenius-projekti
Helotie Anni (HeAn)
Piilotettu
2014-09-08 13.04

<p>Hei,<br>
Olen ollut sairaslomalla kotona tänään ja jatkan parantelua vielä huomenna. Luokassa on oppilaille tuttu Riikka-ope. Valitettavasti joudun perumaan huomisaamun vanhempaintapaamiset. Koulupäivä on kuitenkin huomenna kaikilla 9.00-13.15, kuten aikaisemmin ilmoitin. Elämänkatsomustiedon tunti 8.15 pidetään.
</p>
<p>Alamme tehdä yhdessä 2A-luokan kanssa kansainväliseen Comenius-projektiin liittyvää Kalevala-projektia. Projektin lopputuotos, musiikkipainotteinen esitys, on tarkoitus esittää ennen joulua ainakin koulun muille oppilaille ja teille vanhemmille (mahdollisesti kutsumme paikalle myös isovanhempia ja käymme esiintymässä Kauklahden Asu ja elä -keskuksessa).</p>
<p>Esitys videoidaan ja lähetetään kopiona muistitikulla muihin samassa Comenius-projektissa mukana oleviin kouluihin (8 koulua Euroopassa). Esitystä ei tulle levittämään internettiin. Se näytetään vastaanottavissa kouluissa vain oppilaille. Projektin ajatuksena on tutustua oman ja muiden Euroopan maiden kulttuuriperintöön ja satuihin. Jos haluat, että oma lapsesi EI esiinny videoilla, ilmoitathan siitä minulle Wilma-viestillä tämän viikon aikana.</p>
<p>Kiitän vielä kaikkia mukavasta tunnelmasta viime keskiviikon vanhempainillassa!</p>
<p>Terveisin, Anni-ope</p>
<p></p>
