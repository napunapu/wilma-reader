Kysely etäopetuksesta oppilaille ja huoltajille - Distance learning survey for pupils and guardians
Kass Veronika (V.K)
Piilotettu
2020-05-08 13.58

<p><strong>Kysely etäopetuksesta oppilaille ja huoltajille </strong></p>
<p>Oppilaiden ja huoltajien kokemuksia etäopetuksesta Espoossa kartoitetaan lyhyellä, anonyymillä kyselyllä. Kysely on auki 15.5.2020 klo 23:59. asti.</p>
<p>Kysely oppilaille (luokat 1-10): <a href="https://webropol.com/s/etaopetuskysely-oppilaille-2020">https://webropol.com/s/etaopetuskysely-oppilaille-2020</a></p>
<p>Kysely huoltajille: <a href="https://webropol.com/s/etaopetuskysely-huoltajille-2020">https://webropol.com/s/etaopetuskysely-huoltajille-2020</a></p>
<p>Toivomme, että oppilaat vastaavat kyselyyn kotona. Syynä on se, että koulun tietotekniset välineet ovat osittain "karanteenissa" ja lisäksi yritämme välttää välineiden yhteiskäyttöä.</p>
<p>&nbsp;</p>
<p><strong>Distance learning survey for pupils and guardians</strong></p>
<p>We would like to ask pupils guardians about their experiences of exceptional teaching arrangements and distance learning in Espoo. The survey is short and anonymous. It will close on 15 May 2020 at 23.59.</p>
<p>Survey for pupils (grades 1-10): <a href="https://webropol.com/s/etaopetuskysely-oppilaille-2020">https://webropol.com/s/etaopetuskysely-oppilaille-2020</a></p>
<p>Survey for guardians: <a href="https://webropol.com/s/etaopetuskysely-huoltajille-2020">https://webropol.com/s/etaopetuskysely-huoltajille-2020</a></p>
<p>We hope that, the students respond to the survey at home. The reason is that the school’s IT tools are partly “quarantined” and in addition we try to avoid sharing the tools.</p>
