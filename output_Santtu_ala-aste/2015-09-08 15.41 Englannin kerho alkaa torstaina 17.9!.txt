Englannin kerho alkaa torstaina 17.9!
Lukkarinen Elina (ElinaL)
Piilotettu
2015-09-08 15.41

<p>Hei! 
</p>
<p>Englannin kerho alkaa<b> torstaina 17.9 klo 13.30-14.30.</b> Paikka ilmoitetaan oppilaille myöhemmin asian tarkennettua. Kerhoon tuli valtava määrä ilmoittautumisia ja tästä syystä olemme perustaneet kaksi kerhoryhmää, joiden ohjaajina toimivat Elina Lukkarinen (2A-luokan ope) ja Helka Haipus (2B-luokan ope. Mikäli lapsesi ei pääsekään osallistumaan kerhoon, ilmoitathan asiasta meille niin siirrämme paikan seuraavalle jonossa.</p>
<p>Kerhoterveisin,</p>
<p>Elina ja Helka</p>
<p></p>
