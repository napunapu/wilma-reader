Virkavapaa ma-ti
Lukkarinen Elina (ElinaL)
Piilotettu
2016-10-30 10.44

<p>Hei,&nbsp;</p>
<p>olen virkavapaalla maanantain ja tiistain. Sijaisenani on oppilaille&nbsp;tuttu Hintriika Hetemäe.&nbsp;Kiireellisissä asioissa yhteys näinä päivinä koulun kansliaan (09 81632095).&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin,&nbsp;</p>
<p>Elina</p>
