Santun koulupäivä.
Lukkarinen Elina (ElinaL)
Piilotettu
2016-01-15 14.31

<p>Hei!
</p>
<p>Tiedoksi teille myös kotiin tämän koulupäivän tapahtuma. Tänään koulussa Santtu oli heittänyt välitunnilla lumipallon. Keskustelimme välkän jälkeen, että lumipallojen heittäminen on vaarallista ja koulussa ehdottomasti kielletty ja Santulle tuli selvästi paha mieli tästä keskustelusta. Seuraavalla tunnilla puutuin pari kertaa Konstan ja Santun keskusteluun kirjoitustehtävän aikana. Kesken tunnin toinen oppilas tuli sanomaan minulle , että Santtu oli sanonut kirosanan. Heti kun näki luokkakaverinsa käyneen kertomassa opelle, Santtu pyysi anteeksi ja puhkesi itkuun. Santtua harmitti tämä tosi paljon ja hän alkoi hakata päätään pulpettiin ja löi itseään kynällä päähän. Myös itku oli todella voimakasta. Kehotin jättämään kirjoitustehtävän kesken ja menemään rauhoittumaan luokan perälle sohvalle. Menin itse perässä lohduttamaan Santtua ja hetken päästä hän rauhoittuikin ja ruokailussa oli jo oma ihana iloinen itsensä. Santtu oli vaan selvästi todella pahalla mielellä siitä, että oli tullut monta asiaa joista jouduin huomauttamaan lyhyen ajan sisällä. Asiasta juttelin Santun kanssa ja että on hyvä ymmärtää toimineensa vastoin sääntöjä, mutta itselleen ei saa olla liian ankara, eikä itseään saa satuttaa.</p>
<p>Tämä tapahtuma siis tiedoksi teille. Hyvien viikonlopun toivotusten kera! <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span> </p>
<p>Terveisin,<br>
Elina</p>
<p></p>
