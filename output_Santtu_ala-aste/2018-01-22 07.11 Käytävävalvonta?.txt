Käytävävalvonta?
Tarvainen Outi (Santtu Tarvainen, 6E/2020)
Korpela Panu (Santtu Tarvainen, 6E/2020), Lukkarinen Elina (ElinaL)
2018-01-22 07.11

<p>Hei, <br>
laitan viestin tätäkin kautta eli onko Santulla tänään käytävävalvontavuoro? Silloin ilmeisesti pitäisi tulla tuntia aiemmin kouluun, koska muuten Santulla on ysin aamu. Santtu oli perjantaina poissa eikä ole ihan varma asiasta.<br>
t. Outi</p>
