Terveisiä koululta.
Lukkarinen Elina (ElinaL)
Piilotettu
2015-10-27 16.44

<p>Hei vanhemmat!
</p>
<p>Muutama ilmoitusasia 2A:n syksyä koskien:</p>
<p>- Liikunnassa siirryimme syysloman jälkeen <b>sisäliikuntakaudelle</b>. Liikuntapäivänä(eli torstaisin) oppilailla tulee olla mukana joustavat sisäliikuntavaatteet. Kätevää on tuoda kouluun oma sisäliikuntavaatepussi, jota oppilas säilyttää koululla sisäliikuntakauden ajan. </p>
<p>- Viime viikon torstain(22.10) lukujärjestysmuutoksen vuoksi<b> A-ryhmällä alkaa koulu perjantaina 30.10. vasta kello 9.00!</b> Laitattehan tämän muutoksen muistiin. </p>
<p>- 2.luokalla alamme harjoitella <b>nokkahuilun</b> soittoa. Olen nimikoinut jokaiselle oppilaalle oman nokkahuilun, joka säilyy samalla oppilaalla 6. luokalle saakka. Välillä oppilaat joutuvat tuomaan nokkahuilun myös kotiin harjoitellakseen soittoläksyä. Koulun käytäntö on, että hävinneen nokkahuilun korvaavat huoltajat. Opastamme oppilaita pitämään soittimista hyvää huolta ja keskustelettehan asiasta myös kotona. Näin nokkikset pysyvät tallessa ja hyvässä kunnossa.  </p>
<p>- Olemme kirjoitustunneilla harjoitelleet kirjeen kirjoittamista ja jokainen oppilas on saanut kirjoittaa kirjeen valitsemalleen henkilölle ja kirjeet ovat jokainen kirjoittajansa näköisiä <span class="emoticon" data-file="smile" style="background-image: url(&quot;/shared/scripts/ckeditor/plugins/smiley/images/smile.png&quot;);"></span> Kirjeet tulevat oppilaiden mukana kotiin tällä viikolla ja toivon että autatte oppilaita postittamaan kirjeet oikeaan osoitteeseen. Pyydämme siis myös postimerkin verran panostusta teiltä. </p>
<p>- Olemme syksyn aikana taiteilleet jo koulussa melko monta kuvataidetyötä ja tämä on saattanut näkyä myös oppilaiden vaatteista. Taiteilemme koulussa säännöllisesti, sillä lapset tykkäävät tästä tosi kovasti, ja silloin vaatteet aina altistuvat myös sotkuille.Olisi hienoa jos kotoanne löytyy joku vanha pitkähihainen paita, jonka oppilas voisi tuoda kouluun itselleen taiteilijapaidaksi.</p>
<p>Oikein mukavaa alkanutta viikkoa!</p>
<p>Syysterveisin,<br>
Elina-ope</p>
<p></p>
