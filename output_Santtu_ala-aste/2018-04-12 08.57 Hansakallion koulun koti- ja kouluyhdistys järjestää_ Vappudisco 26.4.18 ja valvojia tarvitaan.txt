Hansakallion koulun koti- ja kouluyhdistys järjestää: Vappudisco 26.4.18 ja valvojia tarvitaan
Väyrynen Sirpa (SirpaV)
Piilotettu
2018-04-12 08.57

<p><span style="color:black;font-size:12pt;">Hansakallion koti- ja kouluyhdistys järjestää Vappu-discon koululla torstaina 26.4.2018 Disco on tarkoitettu vain Hansakallion koulun oppilaille.<br>
<br>
1-3. luokkalaisten disco klo 18.00-19.30<br>
<br>
4-6. luokkalaisten disco klo 19.00-20.30<br>
<br>
Sisäänpääsymaksu 1e. Discossa on buffet eli mukaan voi ottaa pienen summan rahaa<br>
<br>
Jotta disco voidaan toteuttaa, tarvitsemme vanhempia valvojiksi kahteen eri vuoroon 10 henkilöä / vuoro:<br>
<br>
- Valvojavuoro 1 klo 17.45 – 19.30 (Info valvojille klo 17.45-18.00)<br>
<br>
- Valvojavuoro 2 klo 18.45 - 21.00 (Info valvojille klo 18.45-19.00, klo 20.30 alkaen loppusiivous)<br>
<br>
Valvojien tehtäviä ovat järjestyksenpito, buffet, pääsymaksut ja loppusiivous. Valvojat tulkaa ajoissa infoon. Lapset saavat tulla sisälle vasta kun disco alkaa.<br>
<br>
Ilmoittaudu valvojaksi sunnuntaihin 22.4 mennessä <a href="#OnlyHTTPAndHTTPSAllowed"><u>hansakallionvanhemmat@gmail.com</u></a> ja ilmoita kumpaan vuoroon olet tulossa.<br>
<br>
Keskiviikkona 25.4 klo 18-19 ohjelmassa koristelua koululla. Tervetuloa auttamaan koristelussa. Koti- ja kouluyhdistys hankkii koristeet.<br>
<br>
Hansakallion koti- ja kouluyhdistys toimii kaikkien koulun oppilaiden hyväksi. Yhdistys kustantaa oppilaille opiskeluvälineitä, stipendejä ja tapahtumia sekä tukee luokkaretkiä. Tarvitsemme uusia vanhempia mukaan toimintaamme jotta toiminta voi jatkua myös tulevina vuosina. Tervetuloa mukaan yhdistyksen toimintaan. Toimintaan voi ilmoittautua mukaan järjestämissämme tapahtumissa tai sähköpostitse <a href="#OnlyHTTPAndHTTPSAllowed"><u>hansakallionvanhemmat@gmail.com</u></a>. Yhdistyksen toimintaa voi myös tukea maksamalla kannatusmaksu 10 euroa/vuosi. Tili Nordea&nbsp; FI10 2416 2000 0400 71, Hansakallion Koti- ja kouluyhdistys, Viite 4543<br>
<br>
Terveisin<br>
<br>
Hansakallion koti- ja kouluyhdistys</span></p>
<p><span style="color:black;font-size:12pt;">&nbsp;</span></p>
