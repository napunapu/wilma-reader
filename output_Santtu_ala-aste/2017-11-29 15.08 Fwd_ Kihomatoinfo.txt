Fwd: Kihomatoinfo
Auvinen Arja (A.A)
Piilotettu
2017-11-29 15.08

<p>Hei opettajat ja&nbsp;oppilaiden &nbsp;huoltajat!&nbsp;</p>
<p>Kihomatotartuntaa on havaittu&nbsp;4 -luokkalaisten keskuudessa.</p>
<p>Tartuntamahdollisuuden minimoimiseksi on tärkeää se, että koulussa ja kotona sekä kyläreissuilla kiinnitätte huomiota&nbsp;lasten &nbsp;käsienpesuun. Erityisesti ennen ruokailua. &nbsp;Huolellinen käsihygienia sekä siistit kynnet ja kynnenaluset ovat&nbsp;keskeinen osa ennaltaehkäisyä.</p>
<p>Terveisin,&nbsp; th Arja Auvinen</p>
