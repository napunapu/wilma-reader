Re: Joulutiedotettavaa vielä
Tarvainen Outi (Santtu Tarvainen, 6E/2020)
Korpela Panu (Santtu Tarvainen, 6E/2020), Lukkarinen Elina (ElinaL)
2016-12-20 13.23

<p>Hei,<br>
Santun vahvuuksia:</p>
<p>- ihana ja rakastava isoveli</p>
<p>- herkkä, avulias ja empaattinen nuori mies</p>
<p>- taitava trampoliinitemppuilija</p>
<p>- rohkea esiintyjä (esiintyi yli tuhannen katsojan edessä&nbsp;Sirkus Finlandian juhlavuosinäytöksessä)</p>
<p>- pohjimmiltaan tunnollinen vaikka toteutus jää välillä jalkoihin :)</p>
<p>- kova tekemään töitä isovanhempien kanssa pihalla ja puutarhassa</p>
<p>- kotona&nbsp;reipas apulainen, joka&nbsp;aina innoissaan tekemässä&nbsp;joulutorttuja&nbsp;tai Mac&amp;Cheesea,&nbsp;osaa myös käydä kaupassa hakemassa puuttuvia tarvikkeita kuten torttutaikinaa</p>
<p>- taitava päässälaskija, joka soveltaa osaamaansa myös käytännössä</p>
<p>Panu voi jatkaa tästä :)&nbsp;</p>
<p>Hauskaa joulua, toivottavasti Santtu ehtii vielä käväistä koulussa ennen joululomaa!</p>
<p>t. Outi</p>
