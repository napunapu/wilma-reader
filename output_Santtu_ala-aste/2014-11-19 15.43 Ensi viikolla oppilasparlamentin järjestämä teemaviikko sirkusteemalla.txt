Ensi viikolla oppilasparlamentin järjestämä teemaviikko sirkusteemalla
Mattinen Leena (LeenaM)
Piilotettu
2014-11-19 15.43

<p>Tervehdys kotiväki!
</p>
<p>Marraskuu alkaa olla jo pian takana ja päivät ovat entistä harmaampia ja pimeämpiä. Oppilasparlamentti on suunnitellut ja valmistellut loppuvuoden harmautta vastaan teemaviikon, jonka teemana on sirkus. Tarkoituksena on tuoda sirkusteemalla väriä ja iloista mieltä marraskuun viimeiselle viikolle.</p>
<p>Eli ensi viikolla vietämme sirkusviikkoa, jolloin jokaiselle päivälle on nimetty jokin erillinen teema sirkuksen merkeissä.</p>
<p>Maanantaina teemana on erilaiset sirkusasut,<br>
tiistaina taikurit,<br>
keskiviikkona pellet,<br>
torstaina hassut hiukset ja<br>
perjantaina sirkuseläimet.</p>
<p>Toivomme, että oppilaat ja opettajat pukeutuvat tai tuovat muulla tavoin esiin päivien teemoja koulussa.</p>
<p>T: Hansakallion koulun oppilasparlamentti</p>
<p></p>
