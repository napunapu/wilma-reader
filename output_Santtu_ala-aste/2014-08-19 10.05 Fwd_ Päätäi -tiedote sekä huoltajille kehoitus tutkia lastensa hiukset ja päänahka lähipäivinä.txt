Fwd: Päätäi -tiedote sekä huoltajille kehoitus tutkia lastensa hiukset ja päänahka lähipäivinä
Auvinen Arja (A.A)
Piilotettu
2014-08-19 10.05

<p>Hansakallion koulun 1-luokkien oppilailla on havaittu täitä. Tartunnat ovat yleisiä kaikkialla, missä lapsiryhmiä on, päiväkodeissa, kouluissa, harrastuksissa. Tartuntojen ehkäisemiseksi ryhmissä olevien lasten pitkät hiukset on hyvä pitää kiinni ja hiuksia on syytä tarkastaa säännöllisin väliajoin, varsinkin tartuntojen huippukausina syksyllä ja tammikuussa ja käyttää vain omia henkilökohtaisia päähineitä ja hiustarvikkeita.
</p>
<p>Päänahan kutiaminen voi olla merkki täitartunasta. Kaikilla tartunnan saaneista ei ole kutinaoireita. Kutina alkaa noin viikon kuluttua tartunnasta. Kutinaan liittyvä raapiminen saattaa rikkoa hiuspohjan ihon ja altistaa sen bakteeritulehduksille.</p>
<p>Tartunta todetaan tarkastelemalla hiuspohjaa ja hiuksia hyvässä valaistuksessa. Tiheäpiikkisellä täikammalla (saatavana apteekista) kammataan hiuksia suortuva kerrallaan juuresta latvaan. Kampaaminen voidaan tehdä kuiviin tai märkiin hiuksiin. Kammattavan eteen pöydälle asetetaan valkoinen paperi. Tutkitaan esim suurennuslasia apuna käyttäen, onko pöydällä olevan paperin päälle pudonnut täitä.<br>
Märkiä hiuksia kammattaessa seurataan kamman piikkejä, jokaisen vedon jälkeen, onko saalista saatu.</p>
<p>Tartunta on saatu, jos hiuksista löytyy yksikin elävä täi tai hiuksissa kiinni olevia päätäin munia.</p>
<p>Huolellinen häätöhoito apteekista saatavilla valmisteilla ja täikammalla kampaaminen ovat hoidon keskeiset toimenpiteet.</p>
<p>Tartunnasta ja hoidosta on tärkeää ilmoittaa lapsen kavereille, päivähoitoon ja kouluun.</p>
<p>- Yst.terv. Arja Auvinen, terveydenhoitaja</p>
<p></p>
