Vinkki verbien harjoitteluun
Yli-Kohtamäki Heta (HetaY)
Piilotettu
2019-10-01 16.09

<p>https://quizlet.com/subject/Ep%C3%A4s%C3%A4%C3%A4nn%C3%B6lliset-verbit-go-for-it/</p>
<p>&nbsp;</p>
<p>Yllä olevasta linkistä pääsee harjoittelemaan englannin epäsäännöllisiä verbejä kännykän avulla. Jos linkki ei toimi, voi mennä sivulle quizlet.com ja laittaa suurennuslasiin hakusanaksi: epäsäännölliset verbit Go For it.</p>
<p>&nbsp;</p>
<p>Olemme ottaneet läksyksi tähän mennessä 10 verbiä kerrallaan työkirjan sivulta 228 ja ensi viikolla on kirjallinen verbikoe saman sivun verbeistä.</p>
<p>&nbsp;</p>
<p>Yt, Heta</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
