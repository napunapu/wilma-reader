Re: tietoa Lego Mindstorms-robokerhosta
Korpela Panu (Santtu Tarvainen, 6E/2020)
Tarvainen Outi (Santtu Tarvainen, 6E/2020), Kukkonen Jarno (JarnoK)
2016-09-06 18.55

<p>Hei,</p>
<p>Santtu Tarvainen osallistuisi mielellään. Meillä on kotona varsin käyttämätön MindStorms (NXT 2.0, eli edellinen versio), jonka saa haluttaessa ottaa kerhoon mukaan.</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu</p>
