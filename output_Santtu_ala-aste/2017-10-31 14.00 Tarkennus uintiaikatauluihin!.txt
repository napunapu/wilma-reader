Tarkennus uintiaikatauluihin!
Lukkarinen Elina (ElinaL)
Piilotettu
2017-10-31 14.00

<p>Hei!&nbsp;</p>
<p>Tänään uintireissulla huomasimme, että uintireissulla menee noin 10 minuuttia suunniteltua pidempään. Seuraava uinti on perjantaina 3.11., jolloin koulu alkaa klo 9.00 ja loppuu heti uintien jälkeen, eli noin klo 13.40!</p>
<p>Muistutuksena, että torstaina koulu alkaa klo 11.15!&nbsp;</p>
<p>&nbsp;</p>
<p>Uintiterkuin,</p>
<p>Elina&nbsp;</p>
