Tietoa Kauklahden Nuokun kesätoiminnasta 3-6-luokkalaisille
Smolander Maritta (MarittaS)
Piilotettu
2020-05-13 14.56

<p>Hei,</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>Nuorisopalveluiden kesätoiminta toteutuu lähes suunnitelmien mukaan ja Kauklahden nuorisotilalla järjestetään päiväleirejä/kursseja seuraavasti:</p>
<p>&nbsp;</p>
<p>1.-5.6. Päiväleiri tytöille päivittäin klo 9-16</p>
<p>8.-12.6. Kauklahden päiväleiri päivittäin klo 9-16</p>
<p>8.-12.6. Kepparikurssi päivittäin klo 9-15</p>
<p>22.6-26.6. Animaatiopäiväleiri päivittäin klo 9-16.</p>
<p>&nbsp;</p>
<p>Leirit löytyvät Harrastushaku.fi:stä haulla NEKESÄ.&nbsp;</p>
<p>&nbsp;</p>
<p>Terveisin&nbsp;</p>
<p>Kauklahden nuorisotilan ohjaajat.</p>
