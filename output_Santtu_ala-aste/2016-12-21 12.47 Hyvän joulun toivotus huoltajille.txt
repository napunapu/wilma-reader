Hyvän joulun toivotus huoltajille
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2016-12-21 12.47

<p>Arvoisat huoltajat,</p>
<p>Syyslukukausi on pikkuhiljaa tehtynä. Vietimme lauantaina yhdessä mukavat joulujuhlat. Luokat ovat tällä viikolla vielä palanneet tunnelmaan juhlista otetun tallenteen &nbsp;kautta. Viimeisenä koulupäivänä torstaina käydään kirkossa. Niille, jotka eivät osallistu kirkossakäyntiin järjestetään koulussa Arvokas-ohjelmamme mukaista toimintaa. Kouluun tullaan klo 9 ja koulu loppuu klo 12.15. Taxikyydit on järjestelty koulupäivän pituuden mukaisesti. Kevätlukukausi alkaa maanantaina&nbsp;9.1.2017 lukujärjestyksen mukaan. Rauhallista joulunaikaa!</p>
<p>Jarmo Jääskeläinen</p>
<p>rehtori</p>
<p>Hansakallion koulu</p>
