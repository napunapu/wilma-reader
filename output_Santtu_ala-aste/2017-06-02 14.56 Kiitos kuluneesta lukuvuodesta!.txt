Kiitos kuluneesta lukuvuodesta!
Lukkarinen Elina (ElinaL)
Piilotettu
2017-06-02 14.56

<p>Hei kotiväki!
</p>
<p>3.luokka alkaa olla paketissa ja on aika kääntää katse kohti ansaittua kesälomaa! </p>
<p>Oppilailla on huomenna vielä kevätjuhlallisuudet ja todistusten jako koululla klo 9.00-10.30. Ensin ohjelmassa pihajuhla ja sen jälkeen todistusten jako luokissa. Olen siis huomenna juhlimassa nuorimman veljeni ylioppilasjuhlissa ja todistukset jakaa resurssiopettajamme Annu. Riikka-opelle tuli vielä viime metreillä palaveri, eikä hän pääsekään jakamaan todistuksia. Riikka on oppilaiden kanssa kuitenkin pihajuhlan ajan.</p>
<p>Suurkiitos muistamisista, lahjoista ja korteista lukuvuoden päätteeksi! <br>
Ihanaa ja aurinkoista kesälomaa koko perheelle ja lämmin kiitos hyvästä yhteistyöstä! Tästä on hyvä jatkaa elokuussa!</p>
<p>Lomaterveisin, </p>
<p>Elina</p>
<p></p>
