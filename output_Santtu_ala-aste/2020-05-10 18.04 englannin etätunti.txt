englannin etätunti
Yli-Kohtamäki Heta (HetaY)
Piilotettu
2020-05-10 18.04

<p>Tarkista, että olet tehnyt kertaustestin ja annetut etätehtävät aikaisemmilta viikoilta kappaleista 10-11.</p>
<p>Tarkista, että sinulla on torstain enkun tuntia varten mukana: työkirjan (tarkistan!), tekstikirja (kerään pois!) sekä omaan käyttöösi kynäpenaali, kynä, kumi ja teroitin.</p>
<p>Jos kaikki on hoidettu, saat palkinnoksi katsoa Yle Areenasta joko: Docventures: Stonger: The Battle Against COVID-19<br>
TAI Junamatka Amerikkaan (Jakso 1, 7 tai mikä vain jakso näyttää mielenkiintoiselta).</p>
