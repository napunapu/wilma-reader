Huoltajatiedote 12.8.2019
Jääskeläinen Jarmo (JarmoJ)
Piilotettu
2019-08-12 11.06

<p>Arvoisat huoltajat</p>
<p>Lukuvuosi 2019-20 on käynnistynyt. &nbsp;Meitä on noin 570 oppilasta, 40 opettajaa ja kahdeksan koulunkäyntiavustajaa. Lisäksi tulee vielä koulun tiloissa tapahtuva esiopetustoiminta ja muu henkilökunta. Ensimmäisen luokan on aloittanut noin 110 innokasta oppilasta. Kuudennen luokan oppilaat opiskelevat Espoon yhteislyseon tiloissa. He suorittavat osan opinnoistaan liikkumalla&nbsp;välillä Hansakallion pääkoululle ja paviljonkiin.&nbsp;Koulun alkaessa aamulla, keskellä koulupäivää&nbsp;ja sen loppuessa iltapäivällä kaikki tämä näkyy liikenteessä ja koulumme ahtailla parkkialueilla. Olkaamme kaikki varovaisia koululaisten liikkuessa koulun ympäristössä kävellen, pyörällä tai millä tahansa liikkumisvälineellä.</p>
<p>Hansakallion koulun vieressä Hansavalkaman puolella oleva työmaa on edelleen käynnissä. On tärkeää, että koulumme oppilaat liikkuvat rakennustyömaan ohi vain merkittyjä reittejä pitkin. Alueella on ajoittain vilkas työmaaliikenne.</p>
<p>Junaradan eteläpuolella Lasilaaksoon mentäessä tai sieltä kouluun tultaessa käytetty Åminnen silta on korjauksen kohteena. Siltatyömaa yritetään saada loppuviikosta siihen kuntoon, että siitä voidaan erottaa koululaisille oma kaista. Sitä ennen kaupunki on luvannut järjestää aamuisin ja iltapäivisin liikennevalo-ohjauksen Kauklahden väylän ylittävälle suojatielle.</p>
<p>Luokkien vanhempainillat järjestetään elo-syyskuun aikana. Ilmoitamme ajankohdat lähiaikoina. Koulukuvaus on 16. - 18.9, syyslomaviikko 14. - 18.10 ja joulujuhlalauantai on 14.12.2019. Lauantaita vastaava lomapäivä on keväällä 4.5.2020.&nbsp;</p>
<p>&nbsp;</p>
<p>Hyvää lukuvuotta kaikille toivottavat</p>
<p>&nbsp;</p>
<p>Jarmo Jääskeläinen ja Hansakallion koulun väki</p>
