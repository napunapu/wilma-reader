Hansakallion koti- ja kouluyhdistys: Vappu -discoon 25.4. tarvitaan valvojia
Väyrynen Sirpa (SirpaV)
Piilotettu
2019-04-05 12.39

<p><span style="color:#1d2129;font-family:&quot;Times New Roman&quot;,serif;font-size:10pt;">Hansakallion koti- ja kouluyhdistys järjestää Vappu-discon koululla <strong>torstaina 25.04.2019</strong>. Disco on tarkoitettu vain Hansakallion koulun oppilaille. </span></p>
<p><strong><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">1-3. luokkalaisten disco klo 18.00-19.30 </span></strong></p>
<p><strong><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">4-6. luokkalaisten disco klo 19.00-20.30 </span></strong></p>
<p><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">Sisäänpääsymaksu 1e. Discossa on buffet eli mukaan voi ottaa pienen summan rahaa. Mitään kilpailevaa myyntitoimintaa ei tilaisuudessa sallita. </span></p>
<p><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">Jotta disco voidaan toteuttaa, tarvitsemme vanhempia valvojiksi kahteen eri vuoroon 10 henkilöä / vuoro:</span></p>
<p><strong><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">- Valvojavuoro 1 klo 17.45 – 19.30 (Info valvojille klo 17.45-18.00) </span></strong></p>
<p><strong><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">- Valvojavuoro 2 klo 18.45 - 21.00 (Info valvojille klo 18.45-19.00, klo 20.30 alkaen loppusiivous)</span></strong></p>
<p><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;">Valvojien tehtävänä on järjestyksenpito, buffet, pääsymaksut ja loppusiivous. Valvojat tulkaa ajoissa infoon. Lapset saavat tulla sisälle vasta kun disco alkaa. </span></p>
<p><strong><span style="color:#1d2129;font-family:&quot;Times New Roman&quot;,serif;font-size:10pt;">Ilmoittaudu valvojaksi maanantai 22.4 mennessä <a href="#OnlyHTTPAndHTTPSAllowed"><u>hansakallionvanhemmat@gmail.com</u></a> ja ilmoita kumpaan vuoroon olet tulossa. Voit myös tulla molempiin vuoroihin. Yleensä jälkimmäiseen vuoroon on ollut hankalampi saada tarpeeksi valvojia. Disco voidaan järjestää vain jos valvojia saadaan riittävästi.</span></strong></p>
<p><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;font-size:10pt;">Terveisin</span></p>
<p><span style="color:black;font-family:&quot;Times New Roman&quot;,serif;font-size:10pt;">Hansakallion koti- ja kouluyhdistys</span></p>
