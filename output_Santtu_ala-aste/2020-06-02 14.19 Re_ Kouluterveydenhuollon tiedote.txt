Re: Kouluterveydenhuollon tiedote
Korpela Panu (Santtu Tarvainen, 6E/2020)
Tarvainen Outi (Santtu Tarvainen, 6E/2020), Auvinen Arja (A.A)
2020-06-02 14.19

<p>Hei!</p>
<p>Poikamme Santtu Tarvainen on kasvainhoitojen päättymisen jälkeenkin tiiviissä seurannassa Uudessa lastensairaalassa (esim. MRI, verikokeet ja lääkärintarkastus n. 3 kk välein). Osaisitko sanoa, korvaako seuranta terveystarkastuksen?</p>
<p>Terv,<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Panu Korpela</p>
